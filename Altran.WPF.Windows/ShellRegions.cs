﻿namespace Altran.WPF.Windows
{
    /// <summary>
    ///
    /// </summary>
    public static class ShellRegions
    {
        /// <summary>
        /// The mai n_ region
        /// </summary>
        public const string MAIN_REGION_NAME = "MainRegion";

        /// <summary>
        /// The titl e_ ba r_ region
        /// </summary>
        public const string Header_REGION = "HeaderRegion";

        /// <summary>
        /// The foote r_ regio n_ name
        /// </summary>
        public const string FOOTER_REGION_NAME = "FooterRegion";
    }
}