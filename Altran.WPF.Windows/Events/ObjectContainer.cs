﻿namespace Altran.WPF.Windows.Events
{
    public class ObjectContainer<T>
    {
        /// <summary>
        /// Gets or sets the object.
        /// </summary>
        /// <value>
        /// The object.
        /// </value>
        public T Object { get; set; }
    }
}