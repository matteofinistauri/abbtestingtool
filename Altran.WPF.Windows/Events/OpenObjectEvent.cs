﻿using Prism.Events;

namespace Altran.WPF.Windows.Events
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="Prism.Events.PubSubEvent{T}" />
    public class OpenObjectEvent<T> : PubSubEvent<T>
    {
    }
}