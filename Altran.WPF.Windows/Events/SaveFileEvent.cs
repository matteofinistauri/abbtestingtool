﻿using Prism.Events;

namespace Altran.WPF.Windows.Events
{
    public class SaveFileEvent : PubSubEvent<string>
    {
    }
}