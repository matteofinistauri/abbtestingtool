﻿using Prism.Events;

namespace Altran.WPF.Windows.Events
{
    public class NewFileEvent : PubSubEvent<object>
    {
    }
}