﻿using Prism.Events;

namespace Altran.WPF.Windows.Events
{
    public class GetOpenObjectEvent<T> : PubSubEvent<ObjectContainer<T>>
    {
    }
}