﻿using Prism.Events;

namespace Altran.WPF.Windows.Events
{
    public class OpenFileEvent : PubSubEvent<string>
    {
    }
}