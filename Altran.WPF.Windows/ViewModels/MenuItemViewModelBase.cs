﻿using Altran.WPF.MVVM;
using Prism.Commands;
using System.Windows.Input;

namespace Altran.WPF.Windows.ViewModels
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.MVVM.ViewModelBase" />
    public abstract class MenuItemViewModelBase : ViewModelBase
    {
        /// <summary>
        /// The is selected
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItemViewModelBase"/> class.
        /// </summary>
        public MenuItemViewModelBase()
        {
            this.SelectCommand = new DelegateCommand(this.Select);
        }

        /// <summary>
        /// Gets or sets the select command.
        /// </summary>
        /// <value>
        /// The select command.
        /// </value>
        public ICommand SelectCommand { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                if (this.isSelected != value)
                {
                    this.isSelected = value;
                    this.RaisePropertyChangedEvent(() => this.IsSelected);
                }
            }
        }

        /// <summary>
        /// Selects this instance.
        /// </summary>
        public abstract void Select();
    }
}