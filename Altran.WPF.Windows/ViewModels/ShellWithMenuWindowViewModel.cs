﻿using Altran.WPF.MVVM;
using Prism.Commands;
using System.Windows.Input;

namespace Altran.WPF.Windows.ViewModels
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.MVVM.ViewModelBase" />
    public class ShellWithMenuWindowViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShellWithMenuWindowViewModel"/> class.
        /// </summary>
        public ShellWithMenuWindowViewModel()
        {
            this.NewCommand = new DelegateCommand(this.New, this.CanNew);
            this.OpenCommand = new DelegateCommand(this.Open, this.CanOpen);
            this.SaveCommand = new DelegateCommand(() => this.Save(), this.CanSave);
            this.SaveAsCommand = new DelegateCommand(() => this.SaveAs(), this.CanSave);
            this.ExitCommand = new DelegateCommand(this.Exit, this.CanExit);
            this.FormClosingCommand = new DelegateCommand(this.FormClosing);
        }

        /// <summary>
        /// Gets the window title.
        /// </summary>
        /// <value>
        /// The window title.
        /// </value>
        public virtual string WindowTitle
        {
            get
            {
                return "Main Window";
            }
        }

        /// <summary>
        /// Gets the new command.
        /// </summary>
        /// <value>
        /// The new command.
        /// </value>
        public ICommand NewCommand { get; private set; }

        /// <summary>
        /// Gets the open command.
        /// </summary>
        /// <value>
        /// The open command.
        /// </value>
        public ICommand OpenCommand { get; private set; }

        /// <summary>
        /// Gets the save command.
        /// </summary>
        /// <value>
        /// The save command.
        /// </value>
        public ICommand SaveCommand { get; private set; }

        /// <summary>
        /// Gets the save as command.
        /// </summary>
        /// <value>
        /// The save as command.
        /// </value>
        public ICommand SaveAsCommand { get; private set; }

        /// <summary>
        /// Gets the exit command.
        /// </summary>
        /// <value>
        /// The exit command.
        /// </value>
        public ICommand ExitCommand { get; private set; }

        /// <summary>
        /// Gets the form closing command.
        /// </summary>
        /// <value>
        /// The form closing command.
        /// </value>
        public ICommand FormClosingCommand { get; private set; }

        /// <summary>
        /// Gets the name of the main region.
        /// </summary>
        /// <value>
        /// The name of the main region.
        /// </value>
        public string MainRegionName
        {
            get
            {
                return ShellRegions.MAIN_REGION_NAME;
            }
        }

        /// <summary>
        /// Gets the name of the header region.
        /// </summary>
        /// <value>
        /// The name of the header region.
        /// </value>
        public string HeaderRegionName
        {
            get
            {
                return ShellRegions.Header_REGION;
            }
        }

        /// <summary>
        /// Gets the name of the footer region.
        /// </summary>
        /// <value>
        /// The name of the footer region.
        /// </value>
        public string FooterRegionName
        {
            get
            {
                return ShellRegions.FOOTER_REGION_NAME;
            }
        }

        /// <summary>
        /// News this instance.
        /// </summary>
        protected virtual void New()
        {
        }

        /// <summary>
        /// Determines whether this instance can new.
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanNew()
        {
            return true;
        }

        /// <summary>
        /// Opens this instance.
        /// </summary>
        protected virtual void Open()
        {
        }

        /// <summary>
        /// Determines whether this instance can open.
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanOpen()
        {
            return true;
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        protected virtual bool Save()
        {
            return true;
        }

        /// <summary>
        /// Determines whether this instance can save.
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanSave()
        {
            return true;
        }

        /// <summary>
        /// Saves as.
        /// </summary>
        /// <returns></returns>
        protected virtual bool SaveAs()
        {
            return true;
        }

        /// <summary>
        /// Determines whether this instance [can save as].
        /// </summary>
        protected virtual void CanSaveAs()
        {
        }

        /// <summary>
        /// Exits this instance.
        /// </summary>
        /// <returns></returns>
        protected virtual void Exit()
        {
        }

        /// <summary>
        /// Determines whether this instance can exit.
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanExit()
        {
            return true;
        }

        /// <summary>
        /// Forms the closing.
        /// </summary>
        protected virtual void FormClosing()
        {
        }
    }
}