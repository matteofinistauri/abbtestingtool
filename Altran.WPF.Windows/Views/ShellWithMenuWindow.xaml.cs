﻿using MahApps.Metro.Controls;
using System.Windows;

namespace Altran.WPF.Windows
{
    /// <summary>
    /// Interaction logic for ShellWindow.xaml
    /// </summary>
    public partial class ShellWithMenuWindow : MetroWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShellWithMenuWindow"/> class.
        /// </summary>
        public ShellWithMenuWindow()
        {
            InitializeComponent();
        }
    }
}