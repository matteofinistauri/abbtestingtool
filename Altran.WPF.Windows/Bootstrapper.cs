﻿using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Unity;
using System.Windows;

namespace Altran.WPF.Windows
{
    public class FolderBootstrapper<TShell> : UnityBootstrapper
        where TShell : DependencyObject
    {
        /// <summary>
        /// The modules folder
        /// </summary>
        protected string modulesFolder;

        /// <summary>
        /// Initializes a new instance of the <see cref="FolderBootstrapper{TShell}"/> class.
        /// </summary>
        /// <param name="modulesFolder">The modules folder.</param>
        public FolderBootstrapper(string modulesFolder)
        {
            this.modulesFolder = modulesFolder;
        }

        /// <summary>
        /// Creates the shell or main window of the application.
        /// </summary>
        /// <returns>
        /// The shell of the application.
        /// </returns>
        /// <remarks>
        /// If the returned instance is a <see cref="T:System.Windows.DependencyObject" />, the
        /// <see cref="T:Prism.Bootstrapper" /> will attach the default <see cref="T:Prism.Regions.IRegionManager" /> of
        /// the application in its <see cref="F:Prism.Regions.RegionManager.RegionManagerProperty" /> attached property
        /// in order to be able to add regions by using the <see cref="F:Prism.Regions.RegionManager.RegionNameProperty" />
        /// attached property from XAML.
        /// </remarks>
        protected override DependencyObject CreateShell()
        {
            var shell = Container.Resolve<TShell>();
            this.ConfigureShell(shell);
            return shell;
        }

        /// <summary>
        /// Configures the shell.
        /// </summary>
        /// <param name="shell">The shell.</param>
        protected virtual void ConfigureShell(TShell shell)
        {
        }

        /// <summary>
        /// Initializes the shell.
        /// </summary>
        protected override void InitializeShell()
        {
            Application.Current.MainWindow.Show();
        }

        /// <summary>
        /// Configures the <see cref="T:Prism.Modularity.IModuleCatalog" /> used by Prism.
        /// </summary>
        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();
            this.ModuleCatalog = new DirectoryModuleCatalog() { ModulePath = modulesFolder };
        }
    }
}