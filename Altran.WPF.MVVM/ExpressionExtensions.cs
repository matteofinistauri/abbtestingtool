﻿using System;
using System.Linq.Expressions;

namespace Altran.WPF.MVVM
{
    /// <summary>
    /// An helper class with extension methods for Raise Property Changing/Changed.
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="projection">The projection.</param>
        /// <returns></returns>
        public static string GetPropertyName<TProperty>(this Expression<Func<TProperty>> projection)
        {
            var memberExpression = (MemberExpression)projection.Body;
            return memberExpression.Member.Name;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        private static object GetValue(this ViewModelBase instance, string propertyName)
        {
            return instance.GetType().GetProperty(propertyName).GetValue(instance, null);
        }
    }
}