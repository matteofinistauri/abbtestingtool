﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Common.WPF.MVVM.DelegateWatchCommand
{
    /// <summary>
    ///
    /// </summary>
    public static class PropertyWatchExtensions
    {
        /// <summary>
        /// Multis the watch.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="branches">The branches.</param>
        /// <returns></returns>
        public static object MultiWatch<T>(this T source, params Expression<Func<T, object>>[] branches) where T : class, INotifyPropertyChanged
        {
            throw new InvalidOperationException("This method is not intended to be called");
        }

        /// <summary>
        /// Collections the watch.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="branches">The branches.</param>
        /// <returns></returns>
        public static object CollectionWatch<T>(this ObservableCollection<T> source, params Expression<Func<T, object>>[] branches)
        {
            throw new InvalidOperationException("This method is not intended to be called");
        }
    }

    #region Interfaces

    /// <summary>
    ///
    /// </summary>
    public interface IPropertyWatch
    {
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        INotifyPropertyChanged Source { get; set; }

        /// <summary>
        /// Gets the tail.
        /// </summary>
        /// <returns></returns>
        IPropertyWatchTail GetTail();
    }

    /// <summary>
    ///
    /// </summary>
    public interface IPropertyWatchTail : IPropertyWatch
    {
        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        PropertyWatchChain Parent { get; set; }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        IPropertyWatchTail Clone();
    }

    #endregion Interfaces

    /// <summary>
    ///
    /// </summary>
    public class PropertyWatch : IPropertyWatch
    {
        /// <summary>
        ///
        /// </summary>
        private string propName;

        /// <summary>
        ///
        /// </summary>
        private Func<INotifyPropertyChanged, INotifyPropertyChanged> getter;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyWatch"/> class.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="getter">The getter.</param>
        public PropertyWatch(string propertyName, Func<INotifyPropertyChanged, INotifyPropertyChanged> getter)
        {
            if (string.IsNullOrWhiteSpace(propertyName))
                throw new ArgumentOutOfRangeException("propertyName must be not empty");
            if (getter == null)
                throw new ArgumentNullException("getter");

            propName = propertyName;
            this.getter = getter;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyWatch"/> class.
        /// </summary>
        /// <param name="getterExpr">The getter expr.</param>
        public PropertyWatch(Expression<Func<INotifyPropertyChanged, INotifyPropertyChanged>> getterExpr)
        {
            propName = PropertySelectorUtils.GetPropertyName(getterExpr);
            if (getterExpr == null)
                throw new ArgumentNullException("getterExpr");
            getter = getterExpr.Compile();
        }

        /// <summary>
        /// Called when [watched property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void OnWatchedPropertyChanged(string propertyName)
        {
            if (this.Next != null)
                if (propertyName == propName)
                    this.Next.Source = getter(this.Source);
        }

        /// <summary>
        /// Gets or sets the next.
        /// </summary>
        /// <value>
        /// The next.
        /// </value>
        public IPropertyWatch Next { get; set; }

        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        /// <value>
        /// The name of the property.
        /// </value>
        public string PropertyName
        {
            get { return propName; }
        }

        /// <summary>
        ///
        /// </summary>
        private INotifyPropertyChanged source;

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public INotifyPropertyChanged Source
        {
            get { return source; }
            set
            {
                if (source != value)
                {
                    if (source != null)
                        source.PropertyChanged -= new PropertyChangedEventHandler(source_PropertyChanged);
                    source = value;
                    if (source != null)
                        source.PropertyChanged += new PropertyChangedEventHandler(source_PropertyChanged);
                    this.OnWatchedPropertyChanged(propName);
                }
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event of the _source control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void source_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnWatchedPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Doesnt copy Next, doesnt copy Source.
        /// </summary>
        /// <returns></returns>
        public PropertyWatch Clone()
        {
            return new PropertyWatch(this.PropertyName, this.getter);
        }

        /// <summary>
        /// Gets the tail.
        /// </summary>
        /// <returns></returns>
        public IPropertyWatchTail GetTail()
        {
            return this.Next == null ? null : this.Next.GetTail();
        }
    }
}