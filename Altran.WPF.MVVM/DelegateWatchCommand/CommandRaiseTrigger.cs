﻿// -----------------------------------------------------------------------
// <copyright file="CommandRaiseTrigger.cs" company="Indesit">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Common.WPF.MVVM.DelegateWatchCommand
{
    using Altran.WPF.MVVM;
    using Prism.Commands;
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// It defines a dependency between a Can execute of the command to raise and a chain of properties to watch.
    /// When one of the properties in the chain is changed, a RaiseCanExecuteChanged is called on the command to raise.
    /// </summary>
    public class CommandRaiseTrigger : RaiseTrigger
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRaiseTrigger"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="propertiesToWatch">The properties to watch.</param>
        /// <param name="propertiesToRaise">The properties to raise.</param>
        public CommandRaiseTrigger(ViewModelBase source, Expression<Func<ViewModelBase, object>>[] propertiesToWatch, Expression<Func<object>>[] propertiesToRaise)
            : base(source, propertiesToWatch, propertiesToRaise)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRaiseTrigger"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="propertyToRaise">The property to raise.</param>
        /// <param name="propertiesToWatch">The properties to watch.</param>
        public CommandRaiseTrigger(ViewModelBase source, Expression<Func<object>> propertyToRaise, params Expression<Func<ViewModelBase, object>>[] propertiesToWatch)
            : this(source, propertiesToWatch, new Expression<Func<object>>[] { propertyToRaise })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRaiseTrigger"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="propertyToWatch">The property to watch.</param>
        /// <param name="propertiesToRaise">The properties to raise.</param>
        public CommandRaiseTrigger(ViewModelBase source, Expression<Func<ViewModelBase, object>> propertyToWatch, params Expression<Func<object>>[] propertiesToRaise)
            : this(source, new Expression<Func<ViewModelBase, object>>[] { propertyToWatch }, propertiesToRaise)
        {
        }

        /// <summary>
        /// Handles the WatchedPropertyChanged event of the ret control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        protected override void ret_WatchedPropertyChanged(object sender, EventArgs e)
        {
            foreach (var propertyToRaise in this.propertiesToRaise)
            {
                var unaryValue = propertyToRaise.Body as UnaryExpression;
                dynamic t = unaryValue != null ? unaryValue.Operand : propertyToRaise.Body;
                this.Source.RaisePropertyChangedEvent(t.Member.Name);
                if (t.Type == typeof(DelegateCommand))
                {
                    PropertyInfo pr = t.Member as PropertyInfo;
                    DelegateCommand dc = (DelegateCommand)pr.GetValue(this.Source, null);
                    if (dc != null)
                    {
                        dc.RaiseCanExecuteChanged();
                    }
                }
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommandRaiseTrigger<T> : RaiseTrigger<T>
        where T : ViewModelBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRaiseTrigger{T}"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="propertiesToWatch">The properties to watch.</param>
        /// <param name="propertiesToRaise">The properties to raise.</param>
        public CommandRaiseTrigger(T source, Expression<Func<T, object>>[] propertiesToWatch, Expression<Func<object>>[] propertiesToRaise)
            : base(source, propertiesToWatch, propertiesToRaise)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRaiseTrigger{T}"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="propertyToRaise">The property to raise.</param>
        /// <param name="propertiesToWatch">The properties to watch.</param>
        public CommandRaiseTrigger(T source, Expression<Func<object>> propertyToRaise, params Expression<Func<T, object>>[] propertiesToWatch)
            : this(source, propertiesToWatch, new Expression<Func<object>>[] { propertyToRaise })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRaiseTrigger{T}"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="propertyToWatch">The property to watch.</param>
        /// <param name="propertiesToRaise">The properties to raise.</param>
        public CommandRaiseTrigger(T source, Expression<Func<T, object>> propertyToWatch, params Expression<Func<object>>[] propertiesToRaise)
            : this(source, new Expression<Func<T, object>>[] { propertyToWatch }, propertiesToRaise)
        {
        }

        #endregion Constructors

        /// <summary>
        /// Handles the WatchedPropertyChanged event of the ret control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        protected override void ret_WatchedPropertyChanged(object sender, EventArgs e)
        {
            foreach (var propertyToRaise in this.propertiesToRaise)
            {
                var unaryValue = propertyToRaise.Body as UnaryExpression;
                dynamic t = unaryValue != null ? unaryValue.Operand : propertyToRaise.Body;
                this.Source.RaisePropertyChangedEvent(t.Member.Name);
                PropertyInfo pr = t.Member as PropertyInfo;
                if (pr != null)
                {
                    DelegateCommand dc = pr.GetValue(this.Source, null) as DelegateCommand;
                    if (dc != null)
                    {
                        dc.RaiseCanExecuteChanged();
                    }
                }
            }
        }
    }
}