﻿using System.ComponentModel;

namespace Common.WPF.MVVM.DelegateWatchCommand
{
    /// <summary>
    ///
    /// </summary>
    public class NotifyPropertyChangedBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propName">Name of the prop.</param>
        protected void OnPropertyChanged(string propName)
        {
            var lst = this.PropertyChanged;
            if (lst != null)
                lst(this, new PropertyChangedEventArgs(propName));
        }
    }
}