﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Altran.WPF.MVVM
{
    public class ModelBase : INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <returns></returns>
        private object GetValue(string propertyName)
        {
            return this.GetType().GetProperty(propertyName).GetValue(this, null);
        }

        /// <summary>
        /// Determines whether the specified property has property.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <returns>
        ///   <c>true</c> if the specified property has property; otherwise, <c>false</c>.
        /// </returns>
        private bool HasProperty(string propertyName)
        {
            return this.GetType().GetProperty(propertyName) != null;
        }

        /// <summary>
        /// Raises the PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The name of the changed property.</param>
        protected internal void RaisePropertyChangedEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                PropertyChanged(this, e);
            }
        }

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="projection">The projection.</param>
        protected internal virtual void RaisePropertyChangedEvent<TProperty>(Expression<Func<TProperty>> projection)
        {
            RaisePropertyChangedEvent(projection.GetPropertyName());
        }

        #endregion Methods
    }
}