﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using SeleniumLibUITest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Threading;
using System.Linq;

namespace CUITestWebAppTestSuite
{
    public class UIWebApp_CommonAction
    {
        public static UITestControl OpenApplication(String urlApplication)
        {
            var browser = (UITestControl) BrowserWindow.Launch(urlApplication);
            return browser;
        }

        public static void LoginAction(UITestControl parent,String user,String password)
        {
            NavigateHtmlControl.EnterText(parent, "username", user);
            NavigateHtmlControl.EnterText(parent, "password", password);
            NavigateHtmlControl.ClickHtmlControl(parent, HtmlControl.PropertyNames.ValueAttribute, "Login");
        }
        public static void LogoutAction(UITestControl parent, String userName)
        {
            UITestControl _controlUser = NavigateHtmlControl.FindElementClass(parent, "utilities", 0);
            NavigateHtmlControl.ClickSpan(_controlUser, userName);
            NavigateHtmlControl.ClickDiv(parent, "Logout");
        }
        public static void NavigateLeftMenu(UITestControl parent, String plantName, String action)
        {
            UITestControl menuLeft = NavigateHtmlControl.FindElementByProperty(parent, HtmlControl.PropertyNames.Class, "navmenu");
            UITestControl elementMonitor = NavigateHtmlControl.FindHtmlElementInCollection(menuLeft, HtmlControl.PropertyNames.Class, "item main", plantName);
            NavigateHtmlControl.ClickHtmlControl(elementMonitor, HtmlControl.PropertyNames.InnerText, plantName);

            UITestControl elementMenuMonitor = NavigateHtmlControl.FindHtmlElementInCollection(elementMonitor, HtmlControl.PropertyNames.Class, "item", action);
            NavigateHtmlControl.ClickHtmlControl(elementMenuMonitor, HtmlControl.PropertyNames.InnerText, action);

            //UITestControl leftMenu = FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "navmenu");
            //UITestControl plant = FindElementByProperty(leftMenu, HtmlControl.PropertyNames.InnerText, plantName);
            //Mouse.Click(plant);

            //UITestControl elementMenuMonitor = plant.GetParent().GetParent().GetParent();
            //UITestControl itemAction = FindElementByProperty(elementMenuMonitor, HtmlControl.PropertyNames.InnerText, action);
            //Mouse.Click(itemAction.GetParent());
        }

        public static void NavigateCenterTabMenu(UITestControl parent, Int32 indexTabMenu, String nameMenu)
        {
            UITestControl tabs = NavigateHtmlControl.FindElementClass(parent, "menu", 0);
            UITestControl tabsItem = NavigateHtmlControl.FindElementClass(tabs, "item", indexTabMenu);
            NavigateHtmlControl.ClickHtmlControl(tabsItem, HtmlControl.PropertyNames.InnerText, nameMenu);
        }

        public static void InviteUserToPlant(UITestControl parent,String userToInvite, String role)
        {
            NavigateHtmlControl.EnterText(parent, "invitedEmail", userToInvite);
            UITestControl chkRole = NavigateHtmlControl.FindElementByProperty(parent, "for", role);
            Mouse.Click(chkRole);
            NavigateHtmlControl.ClickHtmlControl(parent, HtmlControl.PropertyNames.Name, "Send");
        }

        public static void AcceptInvitationRole(UITestControl parent,String typeNotification)
        {
            UITestControl notification = NavigateHtmlControl.FindElementByProperty(parent, HtmlControl.PropertyNames.Class, "notifications");
            //trova invito owner request
            UITestControl itemNotification = NavigateHtmlControl.FindHtmlElementInCollection(notification, HtmlControl.PropertyNames.Class, "list-items", typeNotification);
            if (itemNotification == null)
                Assert.IsFalse(true, "Non exist link notification");
            NavigateHtmlControl.ClickHtmlControl(itemNotification, HtmlControl.PropertyNames.Class, "button");
            //trova popup conferma
            UITestControl popupConfirm = NavigateHtmlControl.FindElementByProperty(parent, HtmlControl.PropertyNames.Class, "dialog");
            NavigateHtmlControl.ClickHtmlControl(popupConfirm, HtmlControl.PropertyNames.Name, "confirm");
        }

        #region Data Context
        /// <summary>
        ///Ottiene o imposta il contesto del test che fornisce
        ///le informazioni e le funzionalità per l'esecuzione del test corrente.
        ///</summary>
        public static TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private static TestContext testContextInstance;

        #endregion


    }
}
