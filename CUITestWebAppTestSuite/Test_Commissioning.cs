﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using SeleniumLibUITest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Threading;
using System.Linq;
using UIWebAppTestSuite.Data;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.IO;
using WebApiTestSuite.TestRunner.Lib;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.VariablesEngine;
using Newtonsoft.Json.Linq;

namespace CUITestWebAppTestSuite
{
    [CodedUITest]
    public class Test_Commissioning
    {
        private UITestControl browser;
        private UIGlobalParam globalParam;
        private String plantName;

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        public void CT_TS_CO_29()
        {
            //richiesta token di accesso
            //WebAPICaller webApiCaller = new WebAPICaller();
            String urlRequest = "https://epbsdevwabiotwebapi.azurewebsites.net/oauth/token";
            String requestBody = String.Format("grant_type=password&username={0}&password={1}",globalParam.User,globalParam.Password);
            String responseAPI = WebAPICaller.CallApi(urlRequest, HttpVerb.POST, null, requestBody);

            JObject rss = JObject.Parse(responseAPI);
            String token = (String)rss["access_token"];

            urlRequest = "https://epbsdevwabiotwebapi.azurewebsites.net/api/plant/8d332576-de95-4724-b4f2-aef1a2c6fb44/device";
            String requestHeader = String.Format("Authorization: Bearer {0}",token);
            responseAPI = WebAPICaller.CallApi(urlRequest, HttpVerb.GET, requestHeader, null);
            responseAPI = "{'devices':" + responseAPI;
            responseAPI = responseAPI + "}";

            rss = JObject.Parse(responseAPI);

            var nameDevices = from p in rss["devices"]
                        select (string)p["name"];


            //accesso alla we application
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, "Totem S.P.E.", "Settings");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 2, "Devices");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            UITestControlCollection devices = NavigateHtmlControl.FindElementsByProperty(view, HtmlControl.PropertyNames.Class, "item main");
            foreach(UITestControl device in devices)
            {
                NavigateHtmlControl.ClickHtmlControl(device, HtmlControl.PropertyNames.Class, "button");
                UITestControl title = NavigateHtmlControl.FindElementByProperty(device, HtmlControl.PropertyNames.Class, "title");
                String nameDevice = NavigateHtmlControl.ReadElementPropertyValue(title, HtmlControl.PropertyNames.InnerText);

                String checkDevice = (from String item in nameDevices where item == nameDevice select item).FirstOrDefault();

                if (checkDevice == null)
                { 
                    Assert.IsFalse(true, String.Format("The Device {0} not exist in Devices web app."));
                    break;
                }
            }


        }


        #region Attributi di test aggiuntivi

        // È possibile utilizzare i seguenti attributi aggiuntivi per la scrittura dei test:

        ////Utilizza TestInitialize per eseguire codice prima dell'esecuzione di ogni test 
        [TestInitialize()]
        public void UITestInitialize()
        {
            globalParam = TestSerialize.Deserialize<UIGlobalParam>("UIGlobalParam.xml");
        }

        ////Utilizza TestCleanup per eseguire codice dopo l'esecuzione di ogni test
        [TestCleanup()]
        public void UITestCleanup()
        {
            // Per generare un codice per questo test selezionare "Genera codice per Test codificato dell'interfaccia utente" dal menu di scelta rapida e scegliere una delle voci di menu.
            //UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);
        }

        #endregion

        #region Data Context
        /// <summary>
        ///Ottiene o imposta il contesto del test che fornisce
        ///le informazioni e le funzionalità per l'esecuzione del test corrente.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;


        #endregion

    }


}
