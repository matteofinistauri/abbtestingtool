﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using SeleniumLibUITest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Threading;
using System.Linq;
using UIWebAppTestSuite.Data;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.IO;

namespace CUITestWebAppTestSuite
{
    [CodedUITest]
    public class Test_Assets
    {
        private UITestControl browser;
        private UIGlobalParam globalParam;
        private String plantName;

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        public void CT_TS_AS_1()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            //per ogni plant nel menu di sinistra verifica che esista la voce di menu Assets
            UITestControl menuLeft = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "navmenu");
            UITestControlCollection itemsMenuLeft = NavigateHtmlControl.FindElementsByProperty(menuLeft, HtmlControl.PropertyNames.Class, "list-items");
            UITestControlCollection plants = NavigateHtmlControl.FindElementsByProperty(itemsMenuLeft[0], HtmlControl.PropertyNames.Class, "item main");
            foreach (UITestControl plant in plants)
            {
                Mouse.Click(plant);
                UITestControl assets = NavigateHtmlControl.FindElementByProperty(plant, HtmlControl.PropertyNames.InnerText, "Assets");
                if (assets == null)
                    Assert.IsFalse(true, "The plant ");
            }
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_AS_2.csv", "CT_TS_AS_2#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\Assets\\CT_TS_AS_2.csv")]
        public void CT_TS_AS_2()
        {
            //-------------------------------------------------------------------------------
            // Utilizzare il file dati CT_TS_AS_2 per i valori:
            //     - plantname
            //     - assetname
            //-------------------------------------------------------------------------------

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname"].ToString(), "Assets");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 0, "Assets");

            //click su Add Asset
            UITestControl menuAddAsset = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "functions");
            UITestControlCollection menuItems = NavigateHtmlControl.FindElementsByProperty(menuAddAsset, HtmlControl.PropertyNames.Class, "item");
            NavigateHtmlControl.ClickHtmlControl(menuItems[0], HtmlControl.PropertyNames.Class, "button");

            UITestControl popupAddAsset = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
            NavigateHtmlControl.EnterText(popupAddAsset, "title", TestContext.DataRow["assetname"].ToString());
            NavigateHtmlControl.ClickHtmlControl(popupAddAsset, HtmlControl.PropertyNames.Name, "Save");

            //chiude popup upload image
            popupAddAsset = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
            NavigateHtmlControl.ClickHtmlControl(popupAddAsset, HtmlControl.PropertyNames.Name, "Cancel");

            //verifica che il nuovo asset sia in elenco
            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);
            UITestControl asset = NavigateHtmlControl.FindElementByProperty(view, HtmlControl.PropertyNames.InnerText, TestContext.DataRow["assetname"].ToString());

            if (asset == null)
                Assert.IsFalse(true, "New asset not exist in plant");

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DeploymentItem("index.jpg")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_AS_3.csv", "CT_TS_AS_3#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\Assets\\CT_TS_AS_3.csv")]
        public void CT_TS_AS_3()
        {
            //-------------------------------------------------------------------------------
            // Utilizzare il file dati CT_TS_AS_3 per i valori:
            //     - plantname
            //     - assetname
            //-------------------------------------------------------------------------------

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname"].ToString(), "Assets");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 0, "Assets");

            //click su Add Asset
            UITestControl menuAddAsset = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "functions");
            UITestControlCollection menuItems = NavigateHtmlControl.FindElementsByProperty(menuAddAsset, HtmlControl.PropertyNames.Class, "item");
            NavigateHtmlControl.ClickHtmlControl(menuItems[0], HtmlControl.PropertyNames.Class, "button");

            UITestControl popupAddAsset = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
            NavigateHtmlControl.EnterText(popupAddAsset, "title", TestContext.DataRow["assetname"].ToString());
            NavigateHtmlControl.ClickHtmlControl(popupAddAsset, HtmlControl.PropertyNames.Name, "Save");

            //chiude popup upload image
            popupAddAsset = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
            NavigateHtmlControl.ClickHtmlControl(popupAddAsset, HtmlControl.PropertyNames.Name, "Cancel");

            //verifica che il nuovo asset sia in elenco
            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);
            UITestControl asset = NavigateHtmlControl.FindElementByProperty(view, HtmlControl.PropertyNames.InnerText, TestContext.DataRow["assetname"].ToString());

            if (asset == null)
                Assert.IsFalse(true, "New asset not exist in plant");

            //apre l'area asset appena creata
            UITestControl itemAsset = NavigateHtmlControl.FindHtmlElementInCollection(view, HtmlControl.PropertyNames.Class, "item main", TestContext.DataRow["assetname"].ToString());
            NavigateHtmlControl.ClickHtmlControl(itemAsset, HtmlControl.PropertyNames.Class, "button");

            //seleziona funzione add image
            UITestControl item = NavigateHtmlControl.FindHtmlElementInCollection(itemAsset, HtmlControl.PropertyNames.Class, "item", "Add image");
            NavigateHtmlControl.ClickHtmlControl(item, HtmlControl.PropertyNames.Class, "button");

            UITestControl popupUpload = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
            UITestControl btnOpenDialog = NavigateHtmlControl.FindElementByProperty(popupUpload, "for", "fileUploader");
            Mouse.Click(btnOpenDialog);


            WinWindow ChooseFileDialog = new WinWindow();
            ChooseFileDialog.SearchProperties.Add(WinWindow.PropertyNames.ControlType, "Window");
            ChooseFileDialog.SearchProperties.Add(WinWindow.PropertyNames.ClassName, "#32770");
            ChooseFileDialog.TechnologyName = "MSAA";
            ChooseFileDialog.Find();

            WinEdit fileName = new WinEdit(ChooseFileDialog);
            fileName.SearchProperties.Add(WinEdit.PropertyNames.Name, "Nome File:");
            fileName.TechnologyName = "MSAA";

            String nameStr =  Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"index.jpg"); //"C:\\Users\\rzisa\\Desktop\\ABB\\index.jpg";

            fileName.Text = nameStr;


            WinButton openbutton = new WinButton(ChooseFileDialog);
            openbutton.SearchProperties.Add(WinSplitButton.PropertyNames.Name, "Apri");
            Mouse.Click(openbutton);

            NavigateHtmlControl.ClickHtmlControl(popupUpload, HtmlControl.PropertyNames.Name, "Upload");

            UITestControl popupMarker = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
            NavigateHtmlControl.ClickHtmlControl(browser, HtmlControl.PropertyNames.Class, "ngdialog-close");


        }

        #region Attributi di test aggiuntivi

        // È possibile utilizzare i seguenti attributi aggiuntivi per la scrittura dei test:

        ////Utilizza TestInitialize per eseguire codice prima dell'esecuzione di ogni test 
        [TestInitialize()]
        public void UITestInitialize()
        {
            globalParam = TestSerialize.Deserialize<UIGlobalParam>("UIGlobalParam.xml");
        }

        ////Utilizza TestCleanup per eseguire codice dopo l'esecuzione di ogni test
        [TestCleanup()]
        public void UITestCleanup()
        {
            // Per generare un codice per questo test selezionare "Genera codice per Test codificato dell'interfaccia utente" dal menu di scelta rapida e scegliere una delle voci di menu.
            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);
        }

        #endregion

        #region Data Context
        /// <summary>
        ///Ottiene o imposta il contesto del test che fornisce
        ///le informazioni e le funzionalità per l'esecuzione del test corrente.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;


        #endregion

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
