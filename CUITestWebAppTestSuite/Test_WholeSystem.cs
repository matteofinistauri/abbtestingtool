﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using SeleniumLibUITest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Threading;
using System.Linq;
using UIWebAppTestSuite.Data;

namespace CUITestWebAppTestSuite
{
    [CodedUITest]
    public class Test_WholeSystem
    {
        private UITestControl browser;
        private UIGlobalParam globalParam;
        private String plantName;

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WS_27.csv", "CT_TS_WS_27#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WholeSystem\\CT_TS_WS_27.csv")]
        public void CT_TS_WS_27()
        {
            //-------------------------------------------------------------------------------
            // Utilizzare il file dati CT_TS_WS_17 per i valori:
            //     - plantname
            //-------------------------------------------------------------------------------

            String msgFailed = String.Empty;
            //BrowserWindow.CurrentBrowser = "Chrome";
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname"].ToString(), "Assets");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Devices");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            List<String> itemFailed = new List<String>();

            UITestControlCollection devices = NavigateHtmlControl.FindElementsByProperty(view, HtmlControl.PropertyNames.Class, "item main");

            List<String> infoToSearch = new List<String> { "Maintenance", "Info", "Commissioning" };

            Boolean infoFound = true;
            foreach (UITestControl device in devices)
            {
                UITestControl controlDeviceName = NavigateHtmlControl.FindElementByProperty(device, HtmlControl.PropertyNames.Class, "title");
                String deviceName = ((HtmlControl)controlDeviceName).InnerText;

                NavigateHtmlControl.ClickHtmlControl(device, HtmlControl.PropertyNames.Class, "button");

                foreach (String info in infoToSearch)
                {
                    if (NavigateHtmlControl.FindElementByProperty(device, HtmlControl.PropertyNames.InnerText, info) == null)
                    {
                        infoFound = false;
                        msgFailed += String.Format("Devices {0} - not contain property Maintenance", deviceName);
                        msgFailed += "\r\n";
                        break;
                    }
                }
                if (!infoFound)
                    break;
            }

            if (!infoFound)
                Assert.IsFalse(true, msgFailed);

            //logout
            Mouse.MoveScrollWheel(50);
            Thread.Sleep(1000);
            Mouse.MoveScrollWheel(50);
            Thread.Sleep(1000);
            Mouse.MoveScrollWheel(50);

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WS_28.csv", "CT_TS_WS_28#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WholeSystem\\CT_TS_WS_28.csv")]
        public void CT_TS_WS_28()
        {
            //-------------------------------------------------------------------------------
            // Utilizzare il file dati CT_TS_WS_17 per i valori:
            //     - plantname
            //     - devicename
            //-------------------------------------------------------------------------------

            //valori da aggiornare e verificare:
            String name = TestContext.DataRow["devicename"].ToString();
            plantName = TestContext.DataRow["plantname"].ToString();
            String isMainValue = "";

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);
            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 2, "Devices");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);
            UITestControlCollection devices = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");
            NavigateHtmlControl.ClickHtmlControl(devices[0], HtmlControl.PropertyNames.Class, "button");

            UITestControl edit = NavigateHtmlControl.FindElementByProperty(devices[0], HtmlControl.PropertyNames.InnerText, "Edit device");
            NavigateHtmlControl.ClickHtmlControl(edit.GetParent(), HtmlControl.PropertyNames.Class, "button");

            UITestControl popupEdit = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "ngdialog-open");

            NavigateHtmlControl.EnterText(popupEdit, "name", name);

            //legge valore corrente di main
            UITestControl checkMain = NavigateHtmlControl.FindElementByProperty(popupEdit, HtmlControl.PropertyNames.Id, "isMain");
            isMainValue = NavigateHtmlControl.ReadElementPropertyValue(checkMain, HtmlControl.PropertyNames.ValueAttribute);

            NavigateHtmlControl.ClickHtmlControl(popupEdit, "for", "isMain");

            NavigateHtmlControl.ClickHtmlControl(popupEdit, HtmlControl.PropertyNames.ValueAttribute, "Save");

            devices = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");
            NavigateHtmlControl.ClickHtmlControl(devices[0], HtmlControl.PropertyNames.Class, "button");

            //check valori aggiornati
            UITestControl updateName = NavigateHtmlControl.FindElementByProperty(devices[0], HtmlControl.PropertyNames.InnerText, name);
            if (updateName == null)
                Assert.IsFalse(true, "Update failed");
            UITestControl updateIsMain = NavigateHtmlControl.FindElementByProperty(devices[0], HtmlControl.PropertyNames.InnerText, "Main");
            if (isMainValue == "true")
            {
                if (updateIsMain != null)
                    Assert.IsFalse(true, "Update failed");
            }
            if (isMainValue == "false")
            {
                if (updateIsMain == null)
                    Assert.IsFalse(true, "Update failed");
            }


            //NavigateHtmlControl.ClickHtmlControl(popupEdit, HtmlControl.PropertyNames.Class, "ngdialog-close");
        }


        #region Attributi di test aggiuntivi

        // È possibile utilizzare i seguenti attributi aggiuntivi per la scrittura dei test:

        ////Utilizza TestInitialize per eseguire codice prima dell'esecuzione di ogni test 
        [TestInitialize()]
        public void UITestInitialize()
        {
            globalParam = TestSerialize.Deserialize<UIGlobalParam>("UIGlobalParam.xml");
        }

        ////Utilizza TestCleanup per eseguire codice dopo l'esecuzione di ogni test
        [TestCleanup()]
        public void UITestCleanup()
        {
            // Per generare un codice per questo test selezionare "Genera codice per Test codificato dell'interfaccia utente" dal menu di scelta rapida e scegliere una delle voci di menu.
            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);
        }

        #endregion

        #region Data Context
        /// <summary>
        ///Ottiene o imposta il contesto del test che fornisce
        ///le informazioni e le funzionalità per l'esecuzione del test corrente.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        #endregion


    }



}
