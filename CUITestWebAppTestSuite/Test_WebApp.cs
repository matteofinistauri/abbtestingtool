﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using SeleniumLibUITest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Threading;
using System.Linq;
using UIWebAppTestSuite.Data;



namespace CUITestWebAppTestSuite
{
    /// <summary>
    /// Descrizione del riepilogo per Test_WebApp
    /// </summary>
    [CodedUITest]
    public class Test_WebApp
    {
        private UITestControl browser;
        private UIGlobalParam globalParam;
        private String user = String.Empty;
        private String password = String.Empty;
        private String userName = String.Empty;
        private String plantName = String.Empty;
        //private String urlApplication = "https://epbsdevwabiotwebapp.azurewebsites.net";

        public Test_WebApp()
        {
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WA_2.csv", "CT_TS_WA_2#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WebApp\\CT_TS_WA_2.csv")]
        public void CT_TS_WA_2()
        {
            //-------------------------------------------------------------------------------
            // Utilizzare il file dati CT_TS_WA_2 per i valori:
            //     - plantname1
            //     - plantname2
            //     - user_admin_owner
            //     - password_admin_owner
            //     - username_admin_owner
            //     - user_guest_admin
            //     - password_guest_admin
            //     - username_guest_admin
            //-------------------------------------------------------------------------------

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_admin_owner"].ToString(), TestContext.DataRow["password_admin_owner"].ToString());
            globalParam.UserName = TestContext.DataRow["username_admin_owner"].ToString();

            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname1"].ToString(), "Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            //verifica che esistano i check admin, staff, guest
            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);
            UITestControl chkAdmin = NavigateHtmlControl.FindElementByProperty(view, "for", "RoleAdmin");
            UITestControl chkStaff = NavigateHtmlControl.FindElementByProperty(view, "for", "RoleStaff");
            UITestControl chkGuest = NavigateHtmlControl.FindElementByProperty(view, "for", "RoleGuest");

            if (chkAdmin == null || chkStaff == null || chkGuest == null)
                Assert.IsFalse(true, "Non exist all option Admin, Staff or Guest to invite other user");

            //invia l'invito per role guest
            UIWebApp_CommonAction.InviteUserToPlant(view, TestContext.DataRow["user_guest"].ToString(), "RoleGuest");

            //logout current user e login con utente guest per conferma invito
            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);
            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_guest"].ToString(), TestContext.DataRow["password_guest"].ToString());
            globalParam.UserName = TestContext.DataRow["username_guest"].ToString();

            //accettazione invito
            UIWebApp_CommonAction.AcceptInvitationRole(browser, "Invitation");

            //logout user guest e login con user admin/owner
            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);
            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_admin_owner"].ToString(), TestContext.DataRow["password_admin_owner"].ToString());
            globalParam.UserName = TestContext.DataRow["username_admin_owner"].ToString();
            
            //entra in settings-profiles
            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname1"].ToString(), "Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            view = NavigateHtmlControl.FindElementClass(browser, "view", 0);
            //trova gli elementi user
            UITestControlCollection users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            Boolean existUserGuest = false;
            foreach(UITestControl user in users)
            {
                NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                UITestControl checkUserGuest = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, TestContext.DataRow["user_guest"].ToString());
                if(checkUserGuest!=null)
                {
                    existUserGuest = true;
                    UITestControl checkRoleGuest = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Guest");
                    if(checkUserGuest==null)
                    {
                        Assert.IsFalse(true, "User Guest not found");
                        break;
                    }
                }
            }
            if(!existUserGuest)
                Assert.IsFalse(true, "User Guest not found");
            
            //entra nei profiles del secondo plant
            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname2"].ToString(), "Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            //invia l'invito per role admin
            UIWebApp_CommonAction.InviteUserToPlant(view, TestContext.DataRow["user_guest"].ToString(), "RoleAdmin");

            //logout current user e login con utente guest per conferma invito
            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);
            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_guest"].ToString(), TestContext.DataRow["password_guest"].ToString());
            globalParam.UserName = TestContext.DataRow["username_guest"].ToString();

            //accettazione invito
            UIWebApp_CommonAction.AcceptInvitationRole(browser, "Invitation");

            //logout user guest e login con user admin/owner
            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);
            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_admin_owner"].ToString(), TestContext.DataRow["password_admin_owner"].ToString());
            globalParam.UserName = TestContext.DataRow["username_admin_owner"].ToString();

            //entra nei profiles del secondo plant
            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname2"].ToString(), "Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            //trova gli elementi user
            users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            Boolean existUserAdmin = false;
            foreach (UITestControl user in users)
            {
                NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                UITestControl checkUserAdmin = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, TestContext.DataRow["user_guest"].ToString());
                if (checkUserAdmin != null)
                {
                    existUserAdmin = true;
                    UITestControl checkRoleAdmin = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Admin");
                    if (checkRoleAdmin == null)
                    {
                        Assert.IsFalse(true, "User Admin not found");
                        break;
                    }
                }
            }
            if (!existUserAdmin)
                Assert.IsFalse(true, "User Admin not found");

            //entra nei profiles del primo plant
            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname1"].ToString(), "Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            //trova gli elementi user
            users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");
            foreach (UITestControl user in users)
            {
                NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                UITestControl checkUser = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, TestContext.DataRow["user_guest"].ToString());
                if (checkUser != null)
                {
                    //verifica che l'utente non abbia il profilo Admin
                    UITestControl checkRoleAdmin = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Admin");
                    if (checkRoleAdmin != null)
                    {
                        Assert.IsFalse(true, "User exist in plant with the same role");
                        break;
                    }
                }
            }
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]

        public void CT_TS_WA_4()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser,globalParam.User,globalParam.Password);

            plantName = "ABB SACE Lab -1";
            UIWebApp_CommonAction.NavigateLeftMenu(browser,plantName, "Settings");

            UITestControl tabs = NavigateHtmlControl.FindElementClass(browser, "menu", 0);
            UITestControl tabsItem = NavigateHtmlControl.FindElementClass(tabs, "item", 0);
            NavigateHtmlControl.ClickHtmlControl(tabsItem, HtmlControl.PropertyNames.InnerText, "Configuration");

            UITestControl formView = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "view");
            UITestControlCollection fields = NavigateHtmlControl.FindElementsByProperty(formView, HtmlControl.PropertyNames.Class, "control");

            checkInfoForm(formView,HtmlControl.PropertyNames.Id, new List<String> { "Name", "Company","Address","PostCode","Location","Zone","Country","Latitude","Longitude","Type","Description" });

            UITestControl fieldIsAlive = NavigateHtmlControl.FindElementByProperty(formView, HtmlControl.PropertyNames.InnerText, "Alive");
            if (fieldIsAlive == null)
                Assert.IsFalse(true, "Field IsAlive not found");

            Mouse.MoveScrollWheel(-50);
            Thread.Sleep(1000);

            this.UIMap.ClickTerms();

            UITestControl popupTerms = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
            UITestControl txtApproved = NavigateHtmlControl.FindElementByProperty(popupTerms, HtmlControl.PropertyNames.InnerText, "Approved by plant Owner");
            if (txtApproved == null)
                Assert.IsFalse(true, "Field IsOwnerTermsApproved not found");

            NavigateHtmlControl.ClickHtmlControl(browser, HtmlControl.PropertyNames.Class, "ngdialog-close");

            UITestControl textCompany = NavigateHtmlControl.FindElementByProperty(formView, HtmlControl.PropertyNames.Id, "company");

            NavigateHtmlControl.EnterText(formView, HtmlControl.PropertyNames.Id, "company", "Totem S.p.a.");

            Mouse.MoveScrollWheel(-50);
            Thread.Sleep(1000);

            NavigateHtmlControl.ClickHtmlControl(formView, HtmlControl.PropertyNames.Name, "save");

            Thread.Sleep(3000);

            Mouse.MoveScrollWheel(50);
            Thread.Sleep(1000);

            NavigateLeftMenu(plantName, "Settings");

            tabs = NavigateHtmlControl.FindElementClass(browser, "menu", 0);
            tabsItem = NavigateHtmlControl.FindElementClass(tabs, "item", 0);
            NavigateHtmlControl.ClickHtmlControl(tabsItem, HtmlControl.PropertyNames.InnerText, "Configuration");

            formView = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "view");
            textCompany = NavigateHtmlControl.FindElementByProperty(formView, HtmlControl.PropertyNames.Id, "company");
            String companyValue = textCompany.GetProperty("value").ToString();

            if (companyValue != "Totem S.p.a.")
                Assert.IsFalse(true, "value not refresh");

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]

        public void CT_TS_WA_6()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser,globalParam.User,globalParam.Password);

            plantName = "ABB SACE Lab -1";
            UIWebApp_CommonAction.NavigateLeftMenu(browser,plantName, "Settings");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser,2, "Devices");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            UITestControlCollection devices = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            List<String> listField = new List<String> { "Name", "Serial Number", "Main", "Device Type", "Generator", "Alive" };

            String msgFailed = String.Empty;
            Boolean flExistEdit = false;
            foreach (UITestControl device in devices)
            {
                HtmlDiv divButton = new HtmlDiv(device);
                divButton.SearchProperties.Add(HtmlDiv.PropertyNames.Class, "button");
                Mouse.Click(divButton);

                UITestControl title = NavigateHtmlControl.FindElementByProperty(device, HtmlControl.PropertyNames.Class, "title");
                String nameDevice = ((HtmlControl)title).InnerText;

                if (!flExistEdit)
                {
                    UITestControl editDevice = NavigateHtmlControl.FindElementByProperty(device, HtmlControl.PropertyNames.InnerText, "Edit device");
                    flExistEdit = (editDevice == null) ? false : true;
                }

                foreach (String field in listField)
                {
                    UITestControl htmlField = NavigateHtmlControl.FindElementByProperty(device, HtmlControl.PropertyNames.InnerText, field);
                    if (htmlField == null)
                    {
                        msgFailed = String.Format("The device {0} not contain field {1}", nameDevice, field);
                        //Assert.IsFalse(true, "Field " + field + "not found");
                        break;
                    }
                }
                if (!String.IsNullOrEmpty(msgFailed))
                    break;
            }

            if (!String.IsNullOrEmpty(msgFailed))
                Assert.IsFalse(true, msgFailed);
            else
            {
                if (flExistEdit == null)
                    Assert.IsFalse(true, "Not exists editable device");
            }

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WA_9.csv", "CT_TS_WA_9#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WebApp\\CT_TS_WA_9.csv")]
        public void CT_TS_WA_9()
        {
            //-------------------------------------------------------------------------------
            // Compilare nel file CT_TS_WA_9 l'utente admin o owner che assegna il plant
            //
            // Utilizzare il file dati CT_TS_WA_9 per i valori:
            //     - plantname
            //     - user_admin_owner
            //     - password_admin_owner
            //     - username_admin_owner
            //-------------------------------------------------------------------------------

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            //cerca il plant menu e verifica che non esiste
            UITestControl menuLeft = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "navmenu");
            UITestControl elementMonitor = NavigateHtmlControl.FindHtmlElementInCollection(menuLeft, HtmlControl.PropertyNames.Class, "item main", TestContext.DataRow["plantname"].ToString());
            if(elementMonitor==null)
            {
                
                UIWebApp_CommonAction.LogoutAction(browser,globalParam.UserName);
                UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_admin_owner"].ToString(), TestContext.DataRow["password_admin_owner"].ToString());

                UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname"].ToString(), "Settings");

                UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");
                UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

                //esegue l'invito per l'utente a cui assegnare il plant
                NavigateHtmlControl.EnterText(view, "invitedEmail", globalParam.User);
                //globalParam.UserName = TestContext.DataRow["username_admin_owner"].ToString();

                NavigateHtmlControl.ClickHtmlControl(view, "for", "RoleAdmin");
                NavigateHtmlControl.ClickHtmlControl(view, HtmlControl.PropertyNames.Name, "Send");

                UIWebApp_CommonAction.LogoutAction(browser, TestContext.DataRow["username_admin_owner"].ToString());
                UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

                //trova area notification 
                UITestControl notification = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "notifications");
                //trova invito owner request
                UITestControl itemNotification = NavigateHtmlControl.FindHtmlElementInCollection(notification, HtmlControl.PropertyNames.Class, "list-items", "Invitation");
                NavigateHtmlControl.ClickHtmlControl(itemNotification, HtmlControl.PropertyNames.Class, "button");

                //trova popup conferma invitation
                UITestControl popupConfirm = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
                NavigateHtmlControl.ClickHtmlControl(popupConfirm, HtmlControl.PropertyNames.Name, "confirm");

                menuLeft = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "navmenu");
                elementMonitor = NavigateHtmlControl.FindHtmlElementInCollection(menuLeft, HtmlControl.PropertyNames.Class, "item main", TestContext.DataRow["plantname"].ToString());

                if (elementMonitor == null)
                    Assert.IsFalse(true, "The plant is not assigned to first user");
            }
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WA_11.csv", "CT_TS_WA_11#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WebApp\\CT_TS_WA_11.csv")]
        public void CT_TS_WA_11()
        {
            //-------------------------------------------------------------------------------
            // L'utente definito nei parametri globali deve essere un profilo Owner
            // sul plant definito nel file dati CT_TS_WA_11
            //
            // Utilizzare il file dati CT_TS_WA_11 per i valori:
            //     - plantname
            //     - user_new_owner
            //     - password_new_owner
            //     - username_new_owner
            //-------------------------------------------------------------------------------

            plantName = TestContext.DataRow["plantname"].ToString();

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Settings");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            //trova gli elementi user
            UITestControlCollection users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            //verifica la presenza di un solo owner
            Int32 countOwner = 0;
            Int32 indexUserNoOwner = -1;
            Int32 countUser = 0;
            foreach (UITestControl user in users)
            {
                UITestControl owner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Owner");
                if (owner != null)
                {
                    countOwner += 1;
                }
                else
                {
                    NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                    UITestControl newOwner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, TestContext.DataRow["user_new_owner"].ToString());
                    if(newOwner!=null)
                        indexUserNoOwner = countUser;
                    NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                }
                countUser += 1;
            }

            if (countOwner > 1)
                Assert.IsFalse(true, "User is not only owner");

            //assegna il role owner ad altro utente
            NavigateHtmlControl.ClickHtmlControl(users[indexUserNoOwner], HtmlControl.PropertyNames.Class, "button");
            UITestControl setOwner = NavigateHtmlControl.FindHtmlElementInCollection(users[indexUserNoOwner], HtmlControl.PropertyNames.Class, "item", "Set as Owner");
            NavigateHtmlControl.ClickHtmlControl(setOwner, HtmlControl.PropertyNames.Class, "button");

            //trova popup set owner
            UITestControl popupAssignOwner = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "ngdialog-content");
            //invia l'assegnazione owner
            NavigateHtmlControl.ClickHtmlControl(popupAssignOwner, HtmlControl.PropertyNames.InnerText, "Send Owner request");

            //Verifica che esista ancora un solo owner
            countOwner = 0;
            foreach (UITestControl user in users)
            {
                UITestControl owner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Owner");
                if (owner != null)
                    countOwner += 1;
            }

            if (countOwner > 1)
                Assert.IsFalse(true, "User is not only owner");

            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);

            //Accesso all'applicazione con l'utente che ha ricevuto l'invito owner
            userName = TestContext.DataRow["username_new_owner"].ToString();
            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_new_owner"].ToString(), TestContext.DataRow["password_new_owner"].ToString());

            //trova area notification
            UITestControl notification = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "notifications");
            //trova invito owner request
            UITestControl itemNotification = NavigateHtmlControl.FindHtmlElementInCollection(notification, HtmlControl.PropertyNames.Class, "list-items", "Owner request");
            NavigateHtmlControl.ClickHtmlControl(itemNotification, HtmlControl.PropertyNames.Class, "button");

            //trova popup conferma owner
            UITestControl popupConfirm = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");

            //accetta agreement e confirma la request
            NavigateHtmlControl.ClickHtmlControl(popupConfirm, "for", "AgreementPart1");
            NavigateHtmlControl.ClickHtmlControl(popupConfirm, "for", "AgreementPart2");
            NavigateHtmlControl.ClickHtmlControl(popupConfirm, HtmlControl.PropertyNames.Name, "confirm");

            //torna nell'area profile
            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            //trova gli elementi user
            users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            //Verifica che esista ancora un solo owner
            countOwner = 0;
            foreach (UITestControl user in users)
            {
                UITestControl owner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Owner");
                if (owner != null)
                    countOwner += 1;
            }

            if (countOwner > 1)
                Assert.IsFalse(true, "User is not only owner");

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WA_9.csv", "CT_TS_WA_9#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WebApp\\CT_TS_WA_9.csv")]
        public void CT_Test()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, "matteo.finistauri@altran.com", "Matteo82");
            UIWebApp_CommonAction.NavigateLeftMenu(browser, "Test Matteo","Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            String myPath = AppDomain.CurrentDomain.BaseDirectory;

            globalParam.UserName = "Matteo Finistauri";
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WA_12.csv", "CT_TS_WA_12#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WebApp\\CT_TS_WA_12.csv")]
        public void CT_TS_WA_12()
        {
            //-------------------------------------------------------------------------------
            // L'utente definito nei parametri globali deve essere un profilo Admin
            // sul plant definito nel file dati CT_TS_WA_12
            //
            // Utilizzare il file dati CT_TS_WA_12 per i valori:
            //     - plantname
            //     - user owner
            //     - password owner
            //     - username owner
            //-------------------------------------------------------------------------------

            plantName = TestContext.DataRow["plantname"].ToString();

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Settings");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            //trova gli elementi user
            UITestControlCollection users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            foreach (UITestControl user in users)
            {
                NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                //cerca link Remove profile
                UITestControl removeProfile = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Remove profile");
                //cerca profilo owner
                UITestControl owner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Owner");
                if (owner != null)
                {
                    if (removeProfile != null)
                        Assert.IsFalse(true, "The owner keep Remove Profile function");
                }
                else
                {
                    //elimina profile utente per tutti gli user presenti escluso il connesso
                    UITestControl currentUser = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, globalParam.User);
                    if (currentUser == null)
                    {
                        UITestControl item = NavigateHtmlControl.FindHtmlElementInCollection(user, HtmlControl.PropertyNames.Class, "item", "Remove profile");
                        NavigateHtmlControl.ClickHtmlControl(item, HtmlControl.PropertyNames.Class, "button");
                        //trova l'area popup delete
                        UITestControl popupRemove = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
                        NavigateHtmlControl.ClickHtmlControl(popupRemove, HtmlControl.PropertyNames.InnerText, "Remove");
                    }
                }
            }

            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);

            //Accesso all'applicazione con l'utente owner
            userName = TestContext.DataRow["username_owner"].ToString();
            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_owner"].ToString(), TestContext.DataRow["password_owner"].ToString());

            globalParam.UserName = TestContext.DataRow["username_owner"].ToString();

            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Settings");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            //trova gli elementi user
            users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            foreach (UITestControl user in users)
            {
                NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                //cerca link Remove profile
                UITestControl removeProfile = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Remove profile");
                //cerca profilo owner
                UITestControl owner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Owner");
                if (owner != null)
                {
                    if (removeProfile != null)
                        Assert.IsFalse(true, "The owner keep Remove Profile function");
                }
                else
                {
                    //elimina profile user per tutti gli user presenti
                    UITestControl item = NavigateHtmlControl.FindHtmlElementInCollection(user, HtmlControl.PropertyNames.Class, "item", "Remove profile");
                    NavigateHtmlControl.ClickHtmlControl(item, HtmlControl.PropertyNames.Class, "button");
                    //trova l'area popup remove
                    UITestControl popupRemove = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");
                    NavigateHtmlControl.ClickHtmlControl(browser, HtmlControl.PropertyNames.Class, "ngdialog-close");
                }
            }

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WA_13.csv", "CT_TS_WA_13#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WebApp\\CT_TS_WA_13.csv")]
        public void CT_TS_WA_13()
        {
            //-------------------------------------------------------------------------------
            // L'utente definito nei parametri globali deve essere un profilo Owner
            // sul plant definito nel file dati CT_TS_WA_13
            //
            // Utilizzare il file dati CT_TS_WA_13 per i valori:
            //     - plantname
            //     - user_new_owner
            //     - password_new_owner
            //     - username_new_owner
            //-------------------------------------------------------------------------------

            plantName = TestContext.DataRow["plantname"].ToString();

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Settings");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            //trova gli elementi user
            UITestControlCollection users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            //cerca user a cui assegnare owner
            Int32 countUser = 0;
            Int32 indexNewOwner = -1;
            foreach (UITestControl user in users)
            {
                NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                UITestControl newOwner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, TestContext.DataRow["user_new_owner"].ToString());
                if (newOwner != null)
                    indexNewOwner = countUser;
                NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                countUser += 1;
            }

            //assegna il role owner ad altro utente
            NavigateHtmlControl.ClickHtmlControl(users[indexNewOwner], HtmlControl.PropertyNames.Class, "button");
            UITestControl setOwner = NavigateHtmlControl.FindHtmlElementInCollection(users[indexNewOwner], HtmlControl.PropertyNames.Class, "item", "Set as Owner");
            NavigateHtmlControl.ClickHtmlControl(setOwner, HtmlControl.PropertyNames.Class, "button");

            //trova popup set owner
            UITestControl popupAssignOwner = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "ngdialog-content");
            //invia l'assegnazione owner
            NavigateHtmlControl.ClickHtmlControl(popupAssignOwner, HtmlControl.PropertyNames.InnerText, "Send Owner request");

            UIWebApp_CommonAction.LogoutAction(browser, globalParam.UserName);

            //Accesso all'applicazione con l'utente che ha ricevuto l'invito owner
            userName = TestContext.DataRow["username_new_owner"].ToString();
            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user_new_owner"].ToString(), TestContext.DataRow["password_new_owner"].ToString());

            //trova area notification 
            UITestControl notification = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "notifications");
            //trova invito owner request
            UITestControl itemNotification = NavigateHtmlControl.FindHtmlElementInCollection(notification, HtmlControl.PropertyNames.Class, "list-items", "Owner request");
            NavigateHtmlControl.ClickHtmlControl(itemNotification, HtmlControl.PropertyNames.Class, "button");

            //trova popup conferma owner
            UITestControl popupConfirm = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "dialog");

            //accetta agreement e confirma la request
            NavigateHtmlControl.ClickHtmlControl(popupConfirm, "for", "AgreementPart1");
            NavigateHtmlControl.ClickHtmlControl(popupConfirm, "for", "AgreementPart2");
            NavigateHtmlControl.ClickHtmlControl(popupConfirm, HtmlControl.PropertyNames.Name, "confirm");

            //torna nell'area profile
            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Settings");
            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            //trova gli elementi user
            users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");

            //Verifica che esista ancora un solo owner
            Int32 countOwner = 0;
            foreach (UITestControl user in users)
            {
                UITestControl owner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Owner");
                if (owner != null)
                    countOwner += 1;
            }

            if (countOwner > 1)
                Assert.IsFalse(true, "User is not only owner");

            //verifica che l'utente che ha assegnato l'owner ora è admin
            foreach (UITestControl user in users)
            {
                NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                UITestControl oldOwner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, globalParam.User);
                if (oldOwner != null)
                {
                    //verifica che il profile sia Admin
                    UITestControl admin = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Admin");
                    if (admin == null)
                        Assert.IsFalse(true, "The old owner hasn't Admin profile");
                }
            }

            globalParam.UserName = TestContext.DataRow["username_new_owner"].ToString();

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WA_14.csv", "CT_TS_WA_14#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WebApp\\CT_TS_WA_14.csv")]

        public void CT_TS_WA_14()
        {
            //-------------------------------------------------------------------------------
            // Compilare nel file CT_TS_WA_14 l'elenco degli user con il profilo da testare
            //
            // Utilizzare il file dati CT_TS_WA_14 per i valori:
            //     - plantname
            //     - profile (owner,admin,guest,staff)
            //     - user
            //     - password
            //     - username
            //-------------------------------------------------------------------------------

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            globalParam.UserName = TestContext.DataRow["username"].ToString();

            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user"].ToString(), TestContext.DataRow["password"].ToString());

            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname"].ToString(), "Settings");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            UITestControl invitedEmail = NavigateHtmlControl.FindElementByProperty(view, HtmlControl.PropertyNames.Id, "invitedEmail");
            switch (TestContext.DataRow["profile"].ToString())
            {
                case "owner":
                    if (invitedEmail == null)
                        Assert.IsFalse(true, "User width profile " + TestContext.DataRow["profile"].ToString() + " keep not Invite other user");
                    break;
                case "admin":
                    if (invitedEmail == null)
                        Assert.IsFalse(true, "User width profile " + TestContext.DataRow["profile"].ToString() + " keep not Invite other user");
                    break;
                case "guest":
                    if (invitedEmail != null)
                        Assert.IsFalse(true, "User width profile " + TestContext.DataRow["profile"].ToString() + " keep Invite other user");
                    break;
                case "staff":
                    if (invitedEmail != null)
                        Assert.IsFalse(true, "User width profile " + TestContext.DataRow["profile"].ToString() + " keep Invite other user");
                    break;
            }
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "CT_TS_WA_15.csv", "CT_TS_WA_15#csv",
            DataAccessMethod.Sequential), DeploymentItem("FileConfig\\WebApp\\CT_TS_WA_15.csv")]

        public void CT_TS_WS_15()
        {
            //-------------------------------------------------------------------------------
            // Compilare nel file CT_TS_WA_15 l'elenco degli user con il profilo da testare
            //
            // Utilizzare il file dati CT_TS_WA_15 per i valori:
            //     - plantname
            //     - profile (owner,admin,guest,staff)
            //     - user
            //     - password
            //     - username
            //-------------------------------------------------------------------------------

            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            globalParam.UserName = TestContext.DataRow["username"].ToString();

            UIWebApp_CommonAction.LoginAction(browser, TestContext.DataRow["user"].ToString(), TestContext.DataRow["password"].ToString());

            UIWebApp_CommonAction.NavigateLeftMenu(browser, TestContext.DataRow["plantname"].ToString(), "Settings");

            UIWebApp_CommonAction.NavigateCenterTabMenu(browser, 1, "Profiles");

            UITestControl view = NavigateHtmlControl.FindElementClass(browser, "view", 0);

            switch (TestContext.DataRow["profile"].ToString())
            {
                case "owner":
                    Int32 countChangeRole = 0;
                    UITestControlCollection users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");
                    if(users.Count==1)
                        Assert.IsFalse(true, "Profile owner: not exist other user");
                    foreach (UITestControl user in users)
                    {
                        UITestControl owner = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Owner");
                        if (owner == null)
                        { 
                            NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                            UITestControl changeRole = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Change role");
                            if (changeRole != null)
                            {
                                countChangeRole +=1;
                            }
                        }
                    }
                    if(countChangeRole==0)
                        Assert.IsFalse(true, "Profile owner: no other user keep Change Role function");

                    break;

                case "admin": case"guest":
                case "staff": 
                    countChangeRole = 0;
                    users = NavigateHtmlControl.FindElementsByProperty(view, HtmlProgressBar.PropertyNames.Class, "item main");
                    if (users.Count == 1)
                        Assert.IsFalse(true, "Profile " + TestContext.DataRow["profile"].ToString() + ": not exist other user");
                    foreach (UITestControl user in users)
                    {
                        NavigateHtmlControl.ClickHtmlControl(user, HtmlControl.PropertyNames.Class, "button");
                        UITestControl currentUser = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, TestContext.DataRow["user"].ToString());
                        if (currentUser == null)
                        {
                            UITestControl changeRole = NavigateHtmlControl.FindElementByProperty(user, HtmlControl.PropertyNames.InnerText, "Change role");
                            if (changeRole != null)
                            {
                                countChangeRole += 1;
                            }
                        }
                    }
                    switch(TestContext.DataRow["profile"].ToString())
                    {
                        case "admin":
                            if (countChangeRole == 0)
                                Assert.IsFalse(true, "Profile " + TestContext.DataRow["profile"].ToString() + ": No other user keep Change Role function");
                            break;
                        case "guest": case "staff":
                            if (countChangeRole > 0)
                                Assert.IsFalse(true, "Profile " + TestContext.DataRow["profile"].ToString() + ": other user keep Change Role function");
                            break;
                    }


                    break;                 
            }

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]

        public void CT_TS_WA_16()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);

            UIWebApp_CommonAction.LoginAction(browser,globalParam.User,globalParam.Password);
            UITestControl _controlUser = NavigateHtmlControl.FindElementClass(browser, "utilities", 0);
            NavigateHtmlControl.ClickHtmlControl(_controlUser,HtmlControl.PropertyNames.InnerText, "Roberto Zisa");
            UITestControl userMenu = NavigateHtmlControl.FindElementClass(browser, "menu fl-l", 0);
            UITestControl editProfile = NavigateHtmlControl.FindElementClass(userMenu, "no-uline", 0);
            Mouse.Click(editProfile);

            //Thread.Sleep(5000);

            UITestControl formView = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "view");
            UITestControlCollection fields = NavigateHtmlControl.FindElementsByProperty(formView, HtmlControl.PropertyNames.Class, "control");

            checkInfoForm(formView,HtmlControl.PropertyNames.Name, new List<String> { "Company", "Name", "Surname", "Email", "Phone", "Job", "Language", "ID" });

            Thread.Sleep(2000);
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]

        public void CT_TS_WA_18()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UITestControl areaplants = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "plants");
            if (areaplants == null)
                Assert.IsFalse(true, "plants not found");
            else
            {
                Boolean infoOk;
                UITestControlCollection plants = NavigateHtmlControl.FindElementsByProperty(areaplants, HtmlControl.PropertyNames.Class, "item");
                if(plants.Count==0)
                    Assert.IsFalse(true, "plants not found");
                else
                    foreach(UITestControl plant in plants)
                    {
                        Dictionary<String, Boolean> returnCheckInfo = new Dictionary<string, Boolean>();
                        returnCheckInfo.Add("title", false);
                        returnCheckInfo.Add("address", false);
                        returnCheckInfo.Add("role", false);
                        returnCheckInfo.Add("devices", false);
                        returnCheckInfo.Add("powers", false);
                        returnCheckInfo.Add("numberAlerts", false);

                        //title
                        UITestControl title = NavigateHtmlControl.FindElementByProperty(plant, HtmlControl.PropertyNames.Class, "title");
                        infoOk = (title == null) ? false : true;
                        returnCheckInfo["title"] = infoOk;

                        //address 
                        UITestControl address = NavigateHtmlControl.FindElementByProperty(plant, HtmlControl.PropertyNames.Class, "address");
                        infoOk = (address == null) ? false : true;
                        returnCheckInfo["address"] = infoOk;
                        //active powers
                        returnCheckInfo["powers"] = false;
                        //................
                        //actual weather
                        //................
                        //role plant
                        UITestControl profile = NavigateHtmlControl.FindElementByProperty(plant, HtmlControl.PropertyNames.Class, "profile");
                        if (profile!= null)
                        {
                            UITestControl roleValue = NavigateHtmlControl.FindElementByProperty(profile, HtmlControl.PropertyNames.Class, "value");
                            infoOk = (roleValue == null) ? false : true;
                            returnCheckInfo["role"]=infoOk;
                        }
                        UITestControlCollection devices = NavigateHtmlControl.FindElementsByProperty(plant, HtmlControl.PropertyNames.Class, "devices");
                        if(devices.Count>0)
                            foreach(UITestControl device in devices)
                            {
                                UITestControl labelDevice = NavigateHtmlControl.FindElementByProperty(device, HtmlControl.PropertyNames.Class, "label");
                                //number of connected device
                                if(labelDevice!=null && ((HtmlControl) labelDevice).InnerText=="Devices:")
                                    returnCheckInfo["devices"]=true;

                                //alert in places
                                //................
                            }

                        //check result
                        var KeyFalse = returnCheckInfo.FirstOrDefault(x => x.Value == false).Key;
                        if (KeyFalse != null)
                            Assert.IsFalse(true, "Informations plant are incompleted ");

                    }
            }

        }

        [DeploymentItem("UIGlobalParam.xml")]

        public void CT_TS_WA_20()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UITestControl areamap = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Id, "GoogleMap");
            UITestControlCollection imgFlags = NavigateHtmlControl.FindImageBySrc(browser, "/images/marker.svg");
            if (imgFlags.Count > 0)
            {
                String a = "1";
            }
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]

        public void CT_TS_WA_21()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser,plantName, "Dashboard");

            //click su Add Widget
            UITestControl menuDashboard = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "menu");
            UITestControlCollection menuItems = NavigateHtmlControl.FindElementsByProperty(menuDashboard, HtmlControl.PropertyNames.Class, "item");
            NavigateHtmlControl.ClickHtmlControl(menuItems[1], HtmlControl.PropertyNames.Class, "button");

            //cerca l'item energy consumption e aggiunge il widget
            UITestControl popupContent = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "ngdialog-content");
            UITestControlCollection itemsPopup = NavigateHtmlControl.FindElementsByProperty(popupContent, HtmlControl.PropertyNames.Class, "item");
            UITestControl energyConsumption = NavigateHtmlControl.FindHtmlElementInCollection(popupContent, HtmlControl.PropertyNames.Class, "item", "Energy Consumption");
            NavigateHtmlControl.ClickHtmlControl(energyConsumption, HtmlControl.PropertyNames.ValueAttribute, "Add widget");

            //UITestControl popupClose = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "ngdialog-close");
            //Mouse.Click(popupClose);

            //click su Edit Dashboard
            menuDashboard = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "menu");
            menuItems = NavigateHtmlControl.FindElementsByProperty(menuDashboard, HtmlControl.PropertyNames.Class, "item");
            NavigateHtmlControl.ClickHtmlControl(menuItems[0], HtmlControl.PropertyNames.Class, "button");

            //cerca widget Energy Consumption
            UITestControl view = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "view");
            UITestControl updateWidget = NavigateHtmlControl.FindHtmlElementInCollection(view, HtmlControl.PropertyNames.Class, "item gridster-item", "Energy Consumption");
            //cerca l'oggetto size 3x1
            UITestControl sizeWidgetMax = NavigateHtmlControl.FindElementByProperty(updateWidget, HtmlControl.PropertyNames.InnerText, "Size 3x1");

            NavigateHtmlControl.ClickHtmlControl(view, HtmlControl.PropertyNames.Class, "close");

            if (sizeWidgetMax == null)
                Assert.IsFalse(true, "The Widget haven't max size");

            //click su edit dashboard
            //cerca widget Energy Consumption
            //click su edit size
            //save

            //click su edit dashboard
            //cerca widget Energy Consumption
            //cerca sizewidgetmax e verifica che non esista perchè ha dimensione small


            Thread.Sleep(3000);


        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        public void CT_TS_WA_24()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser,plantName, "Dashboard");

            //UITestControl menuDashboard = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "menu");
            //UITestControlCollection menuItems = NavigateHtmlControl.FindElementsByProperty(menuDashboard, HtmlControl.PropertyNames.Class, "item");
            //NavigateHtmlControl.ClickHtmlControl(menuItems[1], HtmlControl.PropertyNames.Class, "button");

            //UITestControl popupContent = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "ngdialog-content");
            //UITestControlCollection itemsPopup = NavigateHtmlControl.FindElementsByProperty(popupContent, HtmlControl.PropertyNames.Class, "item");

            //UITestControl powerDemand = NavigateHtmlControl.FindHtmlElementInCollection(popupContent, HtmlControl.PropertyNames.Class, "item", "Energy Consumption");
            //NavigateHtmlControl.ClickHtmlControl(powerDemand, HtmlControl.PropertyNames.ValueAttribute, "Add widget");

            //UITestControl popupClose = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "ngdialog-close");
            //Mouse.Click(popupClose);

            UITestControl gridWidget = NavigateHtmlControl.FindElementByProperty(browser, HtmlAreaHyperlink.PropertyNames.Class, "view");
            HtmlComboBox period = NavigateHtmlControl.FindComboBox(gridWidget, HtmlControl.PropertyNames.Name, "period");

            Mouse.MoveScrollWheel(-50);
            Thread.Sleep(1000);
            Mouse.MoveScrollWheel(-50);
            Thread.Sleep(1000);

            period.SelectedIndex=1;

            List<String> valuesPeriod = new List<String> { "Today","Current week","Current month","Current semester","Current year","Custom" };
            String retMessage = String.Empty;
            if (!CheckValueInComboBox(period, valuesPeriod, ref retMessage))
                Assert.IsFalse(true, String.Format("The list of 'Period' not contains {0}",retMessage));

            //UITestControl graph = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "energyconsumption");
            //UITestControl chart = NavigateHtmlControl.FindElementByProperty(graph, HtmlControl.PropertyNames.Class, "chart");
            //UITestControl chartLabel = NavigateHtmlControl.FindElementByProperty(chart, HtmlControl.PropertyNames.Class, "nv-axislabel");
            //String label = ((HtmlControl)chartLabel).InnerText;

            foreach (String item in valuesPeriod)
            {
                period.SelectedItem = item;
            }

            //verificare il requisito per capire cosa testare sul grafico

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        public void CT_TS_WA_25()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser,plantName, "Dashboard");

            UITestControl gridWidget = NavigateHtmlControl.FindElementByProperty(browser, HtmlAreaHyperlink.PropertyNames.Class, "view");
            HtmlComboBox graphType = NavigateHtmlControl.FindComboBox(gridWidget, HtmlControl.PropertyNames.Name, "graphType");

            Mouse.MoveScrollWheel(-50);
            Thread.Sleep(1000);
            Mouse.MoveScrollWheel(-50);
            Thread.Sleep(1000);
            Mouse.MoveScrollWheel(-50);
            Thread.Sleep(5000);

            graphType.SelectedIndex = 1;

            List<String> valuesGraphType = new List<String> { "Plant demand", "Energy source", "Grid energy", "Generate energy" };
            String retMessage = String.Empty;
            if (!CheckValueInComboBox(graphType, valuesGraphType, ref retMessage))
                Assert.IsFalse(true, String.Format("The list of 'Graph Type' not contains {0}", retMessage));

            foreach(String item in valuesGraphType)
            {
                graphType.SelectedItem = item;
            }

        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        public void CT_TS_WA_26()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Dashboard");

            //cerca widget Energy Consumption
            UITestControl view = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "view");
            //UITestControl energyConsumption = NavigateHtmlControl.FindHtmlElementInCollection(view, HtmlControl.PropertyNames.Class, "item gridster-item", "Energy Consumption");

            //cerca combo Graph Type e seleziona un elemento
            HtmlComboBox graphType = NavigateHtmlControl.FindComboBox(view, HtmlComboBox.PropertyNames.Name, "graphType");
            graphType.SelectedIndex = 1;

            //cerca combo Period e seleziona un elemento
            HtmlComboBox period = NavigateHtmlControl.FindComboBox(view, HtmlComboBox.PropertyNames.Name, "period");
            period.SelectedIndex = 2;

            //cerca widget energyconsumption
            UITestControl enegyconsumption = NavigateHtmlControl.FindElementByProperty(view, HtmlControl.PropertyNames.Class, "energyconsumption");
            //cerca il chart
            UITestControl chart = NavigateHtmlControl.FindElementByProperty(enegyconsumption, HtmlControl.PropertyNames.Class, "chart");
            //cerca uno shape del chart
            UITestControlCollection shape = NavigateHtmlControl.FindElementsByProperty(chart, HtmlControl.PropertyNames.Class, "nv-bar positive");

            Mouse.MoveScrollWheel(-50);
            Thread.Sleep(1000);


            Thread.Sleep(5000);
        }

        [TestMethod]
        [DeploymentItem("UIGlobalParam.xml")]
        public void CT_TS_WA_29()
        {
            browser = UIWebApp_CommonAction.OpenApplication(globalParam.UrlSite);
            UIWebApp_CommonAction.LoginAction(browser, globalParam.User, globalParam.Password);

            UIWebApp_CommonAction.NavigateLeftMenu(browser, plantName, "Dashboard");

            //click su Add Widget
            UITestControl menuDashboard = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "menu");
            UITestControlCollection menuItems = NavigateHtmlControl.FindElementsByProperty(menuDashboard, HtmlControl.PropertyNames.Class, "item");
            NavigateHtmlControl.ClickHtmlControl(menuItems[1], HtmlControl.PropertyNames.Class, "button");

            //cerca l'item energy consumption e aggiunge il widget
            UITestControl popupContent = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "ngdialog-content");
            UITestControlCollection itemsPopup = NavigateHtmlControl.FindElementsByProperty(popupContent, HtmlControl.PropertyNames.Class, "item");
            UITestControl energyConsumption = NavigateHtmlControl.FindHtmlElementInCollection(popupContent, HtmlControl.PropertyNames.Class, "item", "Energy Consumption");
            NavigateHtmlControl.ClickHtmlControl(energyConsumption, HtmlControl.PropertyNames.ValueAttribute, "Add widget");

            //click su Edit Dashboard
            menuDashboard = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "menu");
            menuItems = NavigateHtmlControl.FindElementsByProperty(menuDashboard, HtmlControl.PropertyNames.Class, "item");
            NavigateHtmlControl.ClickHtmlControl(menuItems[0], HtmlControl.PropertyNames.Class, "button");

            //cerca widget Energy Consumption
            UITestControl view = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "view");
            UITestControl updateWidget = NavigateHtmlControl.FindHtmlElementInCollection(view, HtmlControl.PropertyNames.Class, "item gridster-item", "Energy Consumption");

            //modifica size widget

            //verifica valori del combo
        }

        #region Common Action Test

        private void LogoutAction(UITestControl parent)
        {
            UITestControl _controlUser = NavigateHtmlControl.FindElementClass(parent, "utilities", 0);
            NavigateHtmlControl.ClickSpan(_controlUser, userName);
            NavigateHtmlControl.ClickDiv(parent, "Logout");
        }

        private void NavigateLeftMenu(String plantName, String action)
        {
            UITestControl menuLeft = NavigateHtmlControl.FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "navmenu");
            UITestControl elementMonitor = NavigateHtmlControl.FindHtmlElementInCollection(menuLeft, HtmlControl.PropertyNames.Class, "item main", plantName);
            NavigateHtmlControl.ClickHtmlControl(elementMonitor, HtmlControl.PropertyNames.InnerText, plantName);

            UITestControl elementMenuMonitor = NavigateHtmlControl.FindHtmlElementInCollection(elementMonitor, HtmlControl.PropertyNames.Class, "item", action);
            NavigateHtmlControl.ClickHtmlControl(elementMenuMonitor, HtmlControl.PropertyNames.InnerText, action);

            //UITestControl leftMenu = FindElementByProperty(browser, HtmlControl.PropertyNames.Class, "navmenu");
            //UITestControl plant = FindElementByProperty(leftMenu, HtmlControl.PropertyNames.InnerText, plantName);
            //Mouse.Click(plant);

            //UITestControl elementMenuMonitor = plant.GetParent().GetParent().GetParent();
            //UITestControl itemAction = FindElementByProperty(elementMenuMonitor, HtmlControl.PropertyNames.InnerText, action);
            //Mouse.Click(itemAction.GetParent());
        }

        private void NavigateTest()
        {

            this.UIMap.NavigateMenu();

        }

        private void NavigateCenterTabMenu(Int32 indexTabMenu,String nameMenu)
        {
            UITestControl tabs = NavigateHtmlControl.FindElementClass(browser, "menu", 0);
            UITestControl tabsItem = NavigateHtmlControl.FindElementClass(tabs, "item", indexTabMenu);
            NavigateHtmlControl.ClickHtmlControl(tabsItem, HtmlControl.PropertyNames.InnerText, nameMenu);
        }


        private void checkInfoForm(UITestControl form,String propertyName, List<String> data)
        {
            foreach (String itemData in data)
            {
                UITestControl field = NavigateHtmlControl.FindElementByProperty(form, propertyName, itemData);

                if (field==null)
                {
                    Assert.IsFalse(true, "Field " + itemData + " not found.");
                    break;
                }
            }
        }

        private void checkInfoFormOLD(UITestControlCollection fields, List<String> data)
        {
            foreach (String itemData in data)
            {
                Boolean fieldOK = false;
                foreach (UITestControl field in fields)
                {
                    UITestControl labelNameField = NavigateHtmlControl.FindElementByProperty(field, HtmlControl.PropertyNames.InnerText, itemData);
                    if (labelNameField != null)
                    {
                        fieldOK = true;
                        break;
                    }
                }
                if (!fieldOK)
                {
                    Assert.IsFalse(true, "Field " + itemData + " not found.");
                    break;
                }
            }
        }

        private Boolean CheckDevice(UITestControl parent, ref String msgFailed)
        {
            List<String> itemFailed = new List<String>();

            UITestControlCollection devices = NavigateHtmlControl.FindElementsByProperty(parent, HtmlControl.PropertyNames.Class, "item main");

            Int32 intCount = 0;
            foreach (UITestControl device in devices)
            {
                NavigateHtmlControl.ClickHtmlControl(device, HtmlControl.PropertyNames.Class, "button");

                if (NavigateHtmlControl.FindElementInnerText(device, "Maintenance") == null)
                {
                    itemFailed.Add(String.Format("Devices {0} - {1} not contain property Maintenance", intCount.ToString(), ((HtmlControl)device).InnerText));
                    msgFailed += String.Format("Devices {0} - {1} not contain property Maintenance", intCount.ToString(), "");
                    msgFailed += "\r\n";
                }
                intCount++;
            }

            if (itemFailed.Count > 0)
                return false;
            else
                return true;
        }

        private Boolean CheckValueInComboBox(HtmlComboBox combobox,List<String> values, ref String retMessage)
        {
            Boolean check = true;
            List<String> valuesInComboBox = new List<String>();
            foreach (HtmlControl item in combobox.Items)
            {
                valuesInComboBox.Add(item.InnerText);
            }
            foreach (String item in values)
            {
                String match = (from String current in valuesInComboBox where current == item select current).FirstOrDefault();
                if (match == null)
                { 
                    retMessage =item;
                    check = false;
                }
            }
            return check;
        }

        #endregion

        #region Attributi di test aggiuntivi

        // È possibile utilizzare i seguenti attributi aggiuntivi per la scrittura dei test:

        ////Utilizza TestInitialize per eseguire codice prima dell'esecuzione di ogni test 
        [TestInitialize()]
        public void UITestInitialize()
        {
            globalParam = TestSerialize.Deserialize<UIGlobalParam>("UIGlobalParam.xml");
        }

        ////Utilizza TestCleanup per eseguire codice dopo l'esecuzione di ogni test
        [TestCleanup()]
        public void UITestCleanup()
        {
            // Per generare un codice per questo test selezionare "Genera codice per Test codificato dell'interfaccia utente" dal menu di scelta rapida e scegliere una delle voci di menu.
            UIWebApp_CommonAction.LogoutAction(browser,globalParam.UserName);
        }

        #endregion

        #region Data Context
        /// <summary>
        ///Ottiene o imposta il contesto del test che fornisce
        ///le informazioni e le funzionalità per l'esecuzione del test corrente.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        #endregion

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
