﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using SeleniumLibUITest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Threading;
using System.Linq;

namespace CUITestWebAppTestSuite
{
    public class NavigateHtmlControl
    {
        public static void ClickHtmlControl(UITestControl parent, String propertyName, String innerText)
        {
            var element = new HtmlControl(parent);
            element.SearchProperties.Add(propertyName, innerText);
            Mouse.Click(element);
        }
        public static void EnterText(UITestControl browser, String name, String value)
        {
            var edit = new HtmlEdit(browser);
            edit.SearchProperties.Add(HtmlEdit.PropertyNames.Name, name);
            edit.Text = value;
        }
        public static void EnterText(UITestControl browser, String propertyName, String propertyValue, String textValue)
        {
            var edit = new HtmlEdit(browser);
            edit.SearchProperties.Add(propertyName, propertyValue);
            edit.Text = textValue;
        }

        public static UITestControl FindElementInnerText(UITestControl parent, String innerText)
        {
            try
            {
                HtmlControl control = new HtmlControl(parent);
                control.SearchProperties.Add(HtmlControl.PropertyNames.InnerText, innerText);
                control.Find();
                UITestControlCollection collection = control.FindMatchingControls();
                return collection[0];
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static UITestControl FindHtmlElementInCollection(UITestControl parent, String propertyName, String propertyValue, String innerTextItem)
        {
            UITestControl retElement = null;

            HtmlControl element = new HtmlControl(parent);
            element.SearchProperties.Add(propertyName, propertyValue);
            UITestControlCollection collection = element.FindMatchingControls();

            foreach (HtmlControl item in collection)
            {
                retElement = FindElementInnerText(item, innerTextItem);
                if (retElement != null)
                {
                    retElement = item;
                    break;
                }
            }

            return retElement;
        }

        public static void ReadTextHtmlElement(UITestControl parent, String idElement, String text)
        {
            var htmlElement = new HtmlDiv(parent);
            htmlElement.SearchProperties.Add(HtmlDiv.PropertyNames.Id, idElement);
            String sText = htmlElement.GetProperty("style").ToString();
        }

        public static String ReadElementPropertyValue(UITestControl element,String propertyName)
        {
            return element.GetProperty(propertyName).ToString();
        }

        public static void CheckMessage(UITestControl browser, String idElement, String message, String textMesError)
        {
            var htmlElement = new HtmlSpan(browser);
            htmlElement.SearchProperties.Add(HtmlSpan.PropertyNames.Id, idElement);
            String propertyValue = htmlElement.InnerText;

            if (propertyValue != message)
                Assert.IsTrue(true, "TEST Passed");
            else
                Assert.IsFalse(true, "TEST NOT PASSED - '" + (message == propertyValue) + "' - '" + propertyValue + "'");
        }

        public static void ClickLink(UITestControl browser, String innerText)
        {
            var link = new HtmlHyperlink(browser);
            link.SearchProperties.Add(HtmlHyperlink.PropertyNames.InnerText, innerText);
            Mouse.Click(link);
        }
        public static void ClickDiv(UITestControl browser, String innerText)
        {
            var link = new HtmlDiv(browser);
            link.SearchProperties.Add(HtmlDiv.PropertyNames.InnerText, innerText);
            Mouse.Click(link);
        }

        public static void ClickButton(UITestControl browser, String innerText)
        {
            var button = new HtmlButton(browser);
            button.SearchProperties.Add(HtmlButton.PropertyNames.InnerText, innerText);
            Mouse.Click(button);
        }
        public static void ClickInputButton(UITestControl browser, String innerText)
        {
            var button = new HtmlInputButton(browser);
            button.SearchProperties.Add(HtmlInputButton.PropertyNames.ValueAttribute, innerText);
            Mouse.Click(button);
        }
        public static void ClickSpan(UITestControl browser, String innerText)
        {
            var button = new HtmlSpan(browser);
            button.SearchProperties.Add(HtmlSpan.PropertyNames.InnerText, innerText);
            //ScrollAndClick(button);
            Mouse.Click(button);
        }

        public static UITestControl FindElementClass(UITestControl parent, String className, Int32 indexElement)
        {
            HtmlControl control = new HtmlControl(parent);
            control.SearchProperties.Add(HtmlControl.PropertyNames.Class, className);
            UITestControlCollection collection = control.FindMatchingControls();

            return collection[indexElement];
        }

        public static UITestControl FindElementByPropertyAndCollection(UITestControl parent, String propertyName, String propertyValue)
        {
            HtmlControl element = new HtmlControl(parent);
            element.SearchProperties.Add(propertyName, propertyValue);
            element.Find();
            UITestControlCollection collection = element.FindMatchingControls();
            return collection[0];
        }

        public static UITestControl FindElementByProperty(UITestControl parent, String propertyName, String propertyValue)
        {
            HtmlControl element = new HtmlControl(parent);
            element.SearchProperties.Add(propertyName, propertyValue);
            try
            {
                element.Find();
            }
            catch (Exception ex)
            {
                element = null;
            }
            return element;
        }

        public static UITestControlCollection FindElementsByProperty(UITestControl parent, String propertyName, String propertyValue)
        {
            HtmlControl element = new HtmlControl(parent);
            element.SearchProperties.Add(propertyName, propertyValue);
            UITestControlCollection collection = element.FindMatchingControls();

            return collection;
        }

        public static HtmlComboBox FindComboBox(UITestControl parent,String propertyName,String propertyValue)
        {
            HtmlComboBox element = new HtmlComboBox(parent);
            element.SearchProperties.Add(propertyName, propertyValue);
            try
            {
                element.Find();
            }
            catch (Exception ex)
            {
                element = null;
            }
            return element;
        }

        public static HtmlRadioButton FindRadioButton(UITestControl parent, String propertyName, String propertyValue)
        {
            HtmlRadioButton element = new HtmlRadioButton(parent);
            element.SearchProperties.Add(propertyName, propertyValue);
            try
            {
                element.Find();
            }
            catch (Exception ex)
            {
                element = null;
            }
            return element;
        }

        public static UITestControlCollection FindImageBySrc(UITestControl parent, String src)
        {
            HtmlImage element = new HtmlImage(parent);
            element.SearchProperties.Add(HtmlImage.PropertyNames.Src, src);
            UITestControlCollection collection = element.FindMatchingControls();

            return collection;
        }

        public static void ScrollAndClick(HtmlControl Control)
        {
            bool isClickable = false;
            if (Control.TryFind())
            {
                while (!isClickable)
                {
                    try
                    {
                        Control.EnsureClickable();
                        Mouse.Click(Control);
                        isClickable = true;
                    }
                    catch (FailedToPerformActionOnHiddenControlException ex)
                    {
                        Mouse.MoveScrollWheel(-1);
                        //throw;
                    }

                }
            }
            else
            {
                throw new AssertInconclusiveException("Control Not Found");
            }

        }

    }
}
