﻿using Altran.WPF.Prism;
using Altran.WPF.Windows;
using Altran.WPF.Windows.Events;
using Prism.Events;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Serialization;
using WebApiTestSuite.TestRunner.ViewModels;
using WebApiTestSuite.TestRunner.Views;

namespace WebApiTestSuite.TestRunner
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Prism.Modularity.IModule" />
    public class TestRunnerModule : ModuleBase
    {
        /// <summary>
        /// The event aggregator
        /// </summary>
        private IEventAggregator eventAggregator;

        private TestRunnerMenuItemViewModel testRunnerViewModel;

        public TestRunnerModule(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }

        /// <summary>
        /// Notifies the module that it has be initialized.
        /// </summary>
        public override void Initialize()
        {
            PrismHelper.NavigateToView<TestListViewModel, TestListView>(ShellRegions.MAIN_REGION_NAME, new TestPlan());
            var view = ViewHelper.CreateView<TestRunnerMenuItemViewModel, TestRunnerMenuItemView>(x => x.IsSelected = true, eventAggregator);
            this.testRunnerViewModel = (TestRunnerMenuItemViewModel)view.DataContext;
            PrismHelper.RegisterViewWithRegion(ShellRegions.Header_REGION, () => view);
            PrismHelper.RegisterViewWithRegion(ShellRegions.Header_REGION, () => ViewHelper.CreateView<LoadTestRunnerMenuItemViewModel, LoadTestRunnerMenuItemView>(eventAggregator));
            PrismHelper.RegisterViewWithRegion(ShellRegions.Header_REGION, () => ViewHelper.CreateView<VariablesMenuItemViewModel, VariablesMenuItemView>(eventAggregator));
            PrismHelper.RegisterViewWithRegion(ShellRegions.Header_REGION, () => ViewHelper.CreateView<MacrosMenuItemViewModel, MacrosMenuItemView>(eventAggregator));
            eventAggregator.GetEvent<OpenObjectEvent<TestPlan>>().Subscribe(this.OpenTestPlan, ThreadOption.PublisherThread, true);
        }

        /// <summary>
        /// Opens the test plan.
        /// </summary>
        /// <param name="testPlan">The test plan.</param>
        private void OpenTestPlan(TestPlan testPlan)
        {
            testRunnerViewModel.IsSelected = true;
            testRunnerViewModel.Select();
        }
    }
}