﻿using Altran.WPF.Prism;
using Altran.WPF.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTestSuite.Data.Models;
using WebApiTestSuite.TestRunner.Models;
using WebApiTestSuite.TestRunner.Views;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    internal class LoadTestDetailViewModel : TestStepListViewModelBase<LoadTest>
    {
        private TestCaseRunner runner = new TestCaseRunner();

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadTestDetailViewModel"/> class.
        /// </summary>
        /// <param name="loadTest">The load test.</param>
        public LoadTestDetailViewModel(LoadTest loadTest)
            : base(loadTest)
        {
        }

        /// <summary>
        /// Backs this instance.
        /// </summary>
        protected override void Back()
        {
            PrismHelper.NavigateToView<LoadTestRunnerViewModel, LoadTestRunnerView>(ShellRegions.MAIN_REGION_NAME, this.TestStepParent.Parent);
        }

        /// <summary>
        /// Determines whether this instance can back.
        /// </summary>
        /// <returns></returns>
        protected override bool CanBack()
        {
            return true;
        }

        /// <summary>
        /// Runs the test case.
        /// </summary>
        protected override void Run()
        {
            this.runner.RunLoadTests(this.TestStepParent.Parent.Variables, this.TestStepParent);
        }
    }
}