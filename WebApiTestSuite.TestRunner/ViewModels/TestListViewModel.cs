﻿using Altran.WPF.MVVM;
using Altran.WPF.Prism;
using Altran.WPF.Windows;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using WebApiTestSuite.Data;
using WebApiTestSuite.TestRunner.Models;
using WebApiTestSuite.TestRunner.Views;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    internal class TestListViewModel : ListViewModelBase<TestCase>
    {
        #region Fields

        /// <summary>
        /// The test plan
        /// </summary>
        private TestPlan testPlan;

        /// <summary>
        /// The runner
        /// </summary>
        private TestCaseRunner runner = new TestCaseRunner();

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TestListViewModel" /> class.
        ///
        /// </summary>
        /// <param name="testPlan">The test plan.</param>

        public TestListViewModel(TestPlan testPlan)
            : base(testPlan.TestCases)
        {
            this.TestPlan = testPlan;
            this.runner.TestsExecutionCompleted += this.Runner_TestsExecutionCompleted;
            this.runner.TestsStarted += this.Runner_TestsStarted;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the ts plan repository.
        /// </summary>
        /// <value>
        /// The ts plan repository.
        /// </value>
        public TestPlan TestPlan
        {
            get
            {
                return this.testPlan;
            }

            set
            {
                if (this.TestPlan != value)
                {
                    this.testPlan = value;
                    this.RaisePropertyChangedEvent(() => this.TestPlan);
                }
            }
        }

        /// <summary>
        /// Gets the group field.
        /// </summary>
        /// <value>
        /// The group field.
        /// </value>
        public override string GroupField
        {
            get
            {
                return "Group";
            }
        }

        #endregion Properties

        #region Methods

        #region Events management

        /// <summary>
        /// Handles the TestsStarted event of the Runner control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Runner_TestsStarted(object sender, EventArgs e)
        {
            this.ItemsCollection.Refresh();
        }

        /// <summary>
        /// Handles the TestsExecutionCompleted event of the Runner control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Runner_TestsExecutionCompleted(object sender, EventArgs e)
        {
            this.ItemsCollection.Refresh();
        }

        #endregion Events management

        /// <summary>
        /// Edits the specified test case.
        /// </summary>
        /// <param name="testCase">The test case.</param>
        protected override void Edit(TestCase testCase)
        {
            PrismHelper.NavigateToView<TestDetailViewModel, TestDetailView>(ShellRegions.MAIN_REGION_NAME, testCase);
        }

        /// <summary>
        /// Gets the ordered results.
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<TestCase> GetOrderedResults()
        {
            return from t in this.TestPlan.TestCases orderby t.Group select t;
        }

        /// <summary>
        /// Creates the new item.
        /// </summary>
        /// <returns></returns>
        protected override TestCase CreateNewItem()
        {
            return new TestCase() { Parent = this.TestPlan };
        }

        /// <summary>
        /// Runs all.
        /// </summary>
        protected override void RunAll()
        {
            this.runner.RunTestCases(this.TestPlan.Variables, this.TestPlan.TestCases);
        }

        /// <summary>
        /// Runs the test case.
        /// </summary>
        /// <param name="item"></param>
        protected override void Run(TestCase item)
        {
            this.runner.RunTestCases(item.Parent.Variables, item);
        }

        #endregion Methods
    }
}