﻿using Altran.WPF.MVVM;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    public abstract class ListViewModelBase<T> : ViewModelBase
        where T : class
    {
        #region Fields

        /// <summary>
        /// The parameters
        /// </summary>
        private T testStepParent;

        /// <summary>
        /// The items collection
        /// </summary>
        private ICollectionView itemsCollection;

        /// <summary>
        /// The container
        /// </summary>
        private ObservableCollection<T> container;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TestListViewModel" /> class.
        /// </summary>
        /// <param name="testStepParent">The test step parent.</param>

        public ListViewModelBase(ObservableCollection<T> container)
        {
            this.container = container;
            this.EditCommand = new DelegateCommand<T>(this.Edit);
            this.DeleteCommand = new DelegateCommand<T>(this.Delete);
            this.AddCommand = new DelegateCommand(this.Add);
            this.RunCommand = new DelegateCommand<T>(this.Run);
            this.RunAllCommand = new DelegateCommand(this.RunAll);
            this.CleanCommand = new DelegateCommand(this.Clean);
            this.MoveUpCommand = new DelegateCommand<T>(this.MoveUp, this.CanMoveUp);
            this.MoveDownCommand = new DelegateCommand<T>(this.MoveDown, this.CanMoveDown);
        }

        #endregion Constructor

        #region Properties

        #region Commands

        /// <summary>
        /// Gets the edit command.
        /// </summary>
        /// <value>
        /// The edit command.
        /// </value>
        public ICommand EditCommand { get; private set; }

        /// <summary>
        /// Gets the run test case command.
        /// </summary>
        /// <value>
        /// The run test case command.
        /// </value>
        public ICommand RunCommand { get; private set; }

        /// <summary>
        /// Gets the run all test cases command.
        /// </summary>
        /// <value>
        /// The run all test cases command.
        /// </value>
        public ICommand RunAllCommand { get; private set; }

        /// <summary>
        /// Gets the clean test command.
        /// </summary>
        /// <value>
        /// The clean test command.
        /// </value>
        public ICommand CleanCommand { get; private set; }

        /// <summary>
        /// Gets the delete command.
        /// </summary>
        /// <value>
        /// The delete command.
        /// </value>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the add command.
        /// </summary>
        /// <value>
        /// The add command.
        /// </value>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the move up command.
        /// </summary>
        /// <value>
        /// The move up command.
        /// </value>
        public ICommand MoveUpCommand { get; private set; }

        /// <summary>
        /// Gets the move down command.
        /// </summary>
        /// <value>
        /// The move down command.
        /// </value>
        public ICommand MoveDownCommand { get; private set; }

        #endregion Commands

        /// <summary>
        /// Gets the test cases collection.
        /// </summary>
        /// <value>
        /// The test cases collection.
        /// </value>
        public ICollectionView ItemsCollection
        {
            get
            {
                if (itemsCollection == null)
                {
                    var result = GetOrderedResults();
                    itemsCollection = CollectionViewSource.GetDefaultView(result);
                    string groupField = this.GroupField;
                    if (groupField != null)
                    {
                        itemsCollection.GroupDescriptions.Add(new PropertyGroupDescription(groupField));
                    }
                }

                return itemsCollection;
            }
        }

        /// <summary>
        /// Gets the group field.
        /// </summary>
        /// <value>
        /// The group field.
        /// </value>
        public virtual string GroupField { get; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Edits the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        protected abstract void Edit(T item);

        /// <summary>
        /// Gets the ordered results.
        /// </summary>
        /// <returns></returns>
        protected abstract IEnumerable<T> GetOrderedResults();

        /// <summary>
        /// Deletes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        private void Delete(T item)
        {
            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Are you sure you want to remove the item?", "Remove item", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                this.container.Remove(item);
                this.ItemsCollection.Refresh();
            }
        }

        /// <summary>
        /// Adds this instance.
        /// </summary>
        private void Add()
        {
            T item = CreateNewItem();
            this.container.Add(item);
            this.Edit(item);
        }

        /// <summary>
        /// Creates the new item.
        /// </summary>
        /// <returns></returns>
        protected abstract T CreateNewItem();

        /// <summary>
        /// Runs all.
        /// </summary>
        protected abstract void RunAll();

        /// <summary>
        /// Runs the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        protected abstract void Run(T item);

        /// <summary>
        /// Cleans this instance.
        /// </summary>
        private void Clean()
        {
            this.container.Clear();
            this.ItemsCollection.Refresh();
        }

        /// <summary>
        /// Moves down.
        /// </summary>
        /// <param name="item">The item.</param>
        private void MoveDown(T item)
        {
            var index = this.container.IndexOf(item);
            if (index != this.container.Count - 1)
            {
                this.container.RemoveAt(index);
                this.container.Insert(index + 1, item);
                this.ItemsCollection.Refresh();
            }
        }

        /// <summary>
        /// Determines whether this instance [can move down] the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private bool CanMoveDown(T item)
        {
            return true;//return this.container.IndexOf(item) != this.container.Count - 1;
        }

        /// <summary>
        /// Moves up.
        /// </summary>
        /// <param name="item">The item.</param>
        private void MoveUp(T item)
        {
            var index = this.container.IndexOf(item);
            if (index != 0)
            {
                this.container.RemoveAt(index);
                this.container.Insert(index - 1, item);
                this.ItemsCollection.Refresh();
            }
        }

        /// <summary>
        /// Determines whether this instance [can move up] the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private bool CanMoveUp(T item)
        {
            return true;//return this.container.IndexOf(item) != 0;
        }

        #endregion Methods
    }
}