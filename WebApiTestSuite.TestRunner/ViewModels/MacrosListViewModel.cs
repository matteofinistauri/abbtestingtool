﻿using Altran.WPF.MVVM;
using Prism.Commands;
using System;
using System.Windows.Input;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    internal class MacrosListViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MacrosListViewModel"/> class.
        /// </summary>
        /// <param name="testPlan">The test plan.</param>
        public MacrosListViewModel(TestPlan testPlan)
        {
            this.TestPlan = testPlan;
            this.AddMacroCommand = new DelegateCommand(this.AddMacro);
        }

        /// <summary>
        /// Gets the add macro command.
        /// </summary>
        /// <value>
        /// The add macro command.
        /// </value>
        public ICommand AddMacroCommand { get; private set; }

        /// <summary>
        /// Gets or sets the test plan.
        /// </summary>
        /// <value>
        /// The test plan.
        /// </value>
        public TestPlan TestPlan { get; private set; }

        /// <summary>
        /// Gets the HTTP verbs.
        /// </summary>
        /// <value>
        /// The HTTP verbs.
        /// </value>
        public HttpVerb[] HttpVerbs
        {
            get
            {
                return (HttpVerb[])Enum.GetValues(typeof(HttpVerb));
            }
        }

        /// <summary>
        /// Adds the macro.
        /// </summary>
        private void AddMacro()
        {
            this.TestPlan.Macros.Add(new TestStepDefinition()); // The parent is not set because it's not related to a test plan.
        }
    }
}