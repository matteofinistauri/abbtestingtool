﻿using Altran.WPF.MVVM;
using Altran.WPF.Prism;
using Altran.WPF.Windows;
using Common.WPF.MVVM.DelegateWatchCommand;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using System;
using System.Windows.Input;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;
using WebApiTestSuite.TestRunner.Models;
using WebApiTestSuite.TestRunner.Views;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    internal class TestDetailViewModel : TestStepListViewModelBase<TestCase>
    {
        /// <summary>
        /// The runner
        /// </summary>
        private TestCaseRunner runner = new TestCaseRunner();

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDetailViewModel" /> class.
        /// </summary>
        /// <param name="testCase">The test case.</param>
        public TestDetailViewModel(TestCase testCase)
            : base(testCase)
        {
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
            get
            {
                return this.TestStepParent.Description;
            }

            set
            {
                if (this.TestStepParent.Description != value)
                {
                    this.TestStepParent.Description = value;
                    this.RaisePropertyChangedEvent(() => this.Description);
                }
            }
        }

        /// <summary>
        /// Creates the back command.
        /// </summary>
        /// <returns></returns>
        protected override ICommand CreateBackCommand()
        {
            return new DelegateWatchCommand<TestDetailViewModel>(this.Back, this.CanBack, this, x => x.Description);
        }

        /// <summary>
        /// Determines whether this instance [can run test case].
        /// </summary>
        /// <returns></returns>
        protected override bool CanRun()
        {
            return true;
        }

        /// <summary>
        /// Runs the test case.
        /// </summary>
        protected override void Run()
        {
            this.runner.RunTestCases(this.TestStepParent.Parent.Variables, this.TestStepParent);
        }

        /// <summary>
        /// Determines whether this instance can back.
        /// </summary>
        /// <returns></returns>
        protected override bool CanBack()
        {
            return !string.IsNullOrEmpty(this.Description);
        }

        /// <summary>
        /// Backs this instance.
        /// </summary>
        protected override void Back()
        {
            PrismHelper.NavigateToView<TestListViewModel, TestListView>(ShellRegions.MAIN_REGION_NAME, this.TestStepParent.Parent);
        }
    }
}