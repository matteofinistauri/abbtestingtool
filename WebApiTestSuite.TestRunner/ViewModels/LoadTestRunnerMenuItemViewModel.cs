﻿using Altran.WPF.Prism;
using Altran.WPF.Windows;
using Altran.WPF.Windows.Events;
using Altran.WPF.Windows.ViewModels;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTestSuite.Data;
using WebApiTestSuite.TestRunner.Views;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.Windows.ViewModels.MenuItemViewModelBase" />
    public class LoadTestRunnerMenuItemViewModel : MenuItemViewModelBase
    {
        /// <summary>
        /// The event aggregator
        /// </summary>
        private IEventAggregator eventAggregator;

        public LoadTestRunnerMenuItemViewModel(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }

        /// <summary>
        /// Selects this instance.
        /// </summary>
        public override void Select()
        {
            var ev = eventAggregator.GetEvent<GetOpenObjectEvent<TestPlan>>();
            var objectContainer = new ObjectContainer<TestPlan>();
            ev.Publish(objectContainer);
            PrismHelper.NavigateToView<LoadTestRunnerViewModel, LoadTestRunnerView>(ShellRegions.MAIN_REGION_NAME, objectContainer.Object);
        }
    }
}