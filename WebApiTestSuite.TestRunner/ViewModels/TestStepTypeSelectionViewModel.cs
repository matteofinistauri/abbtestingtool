﻿using Altran.WPF.MVVM;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using System;
using System.Windows.Input;
using WebApiTestSuite.Data;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    internal class TestStepTypeSelectionViewModel : ModelBase, IInteractionRequestAware
    {
        /// <summary>
        /// The notification
        /// </summary>
        private TestStepTypeNotification notification;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestStepTypeSelectionViewModel" /> class.
        /// </summary>
        public TestStepTypeSelectionViewModel()
        {
            this.OKCommand = new DelegateCommand(this.OK);
            this.CancelCommand = new DelegateCommand(this.Cancel);
        }

        /// <summary>
        /// An <see cref="T:System.Action" /> that can be invoked to finish the interaction.
        /// </summary>
        public Action FinishInteraction { get; set; }

        /// <summary>
        /// The <see cref="T:Prism.Interactivity.InteractionRequest.INotification" /> passed when the interaction request was raised.
        /// </summary>
        public INotification Notification
        {
            get
            {
                return this.notification;
            }
            set
            {
                if (value is TestStepTypeNotification)
                {
                    // To keep the code simple, this is the only property where we are raising the PropertyChanged event,
                    // as it's required to update the bindings when this property is populated.
                    // Usually you would want to raise this event for other properties too.
                    this.notification = value as TestStepTypeNotification;
                    this.RaisePropertyChangedEvent(() => this.Notification);
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the test step.
        /// </summary>
        /// <value>
        /// The type of the test step.
        /// </value>
        public TestStepType TestStepType { get; set; }

        /// <summary>
        /// Gets the ok command.
        /// </summary>
        /// <value>
        /// The ok command.
        /// </value>
        public ICommand OKCommand { get; private set; }

        /// <summary>
        /// Gets the cancel command.
        /// </summary>
        /// <value>
        /// The cancel command.
        /// </value>
        public ICommand CancelCommand { get; private set; }

        /// <summary>
        /// Oks this instance.
        /// </summary>
        public void OK()
        {
            if (this.notification != null)
            {
                this.notification.SelectedItem = this.TestStepType;
                this.notification.Confirmed = true;
            }

            this.FinishInteraction();
        }

        /// <summary>
        /// Cancels this instance.
        /// </summary>
        public void Cancel()
        {
            if (this.notification != null)
            {
                this.notification.Confirmed = false;
            }

            this.FinishInteraction();
        }
    }
}