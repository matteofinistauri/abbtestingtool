﻿using Altran.WPF.MVVM;
using Altran.WPF.Prism;
using Altran.WPF.Windows;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;
using WebApiTestSuite.TestRunner.Models;
using WebApiTestSuite.TestRunner.Views;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.MVVM.ViewModelBase" />
    public class LoadTestRunnerViewModel : ListViewModelBase<LoadTest>
    {
        /// <summary>
        /// The test plan
        /// </summary>
        private TestPlan testPlan;

        /// <summary>
        /// The runner
        /// </summary>
        private TestCaseRunner runner = new TestCaseRunner();

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadTestRunnerViewModel"/> class.
        /// </summary>
        /// <param name="testPlan">The test plan.</param>
        /// <param name="container">The container.</param>
        public LoadTestRunnerViewModel(TestPlan testPlan)
            : base(testPlan.LoadTests)
        {
            this.testPlan = testPlan;
        }

        /// <summary>
        /// Creates the new item.
        /// </summary>
        /// <returns></returns>
        protected override LoadTest CreateNewItem()
        {
            return new LoadTest() { Parent = testPlan };
        }

        /// <summary>
        /// Edits the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        protected override void Edit(LoadTest item)
        {
            PrismHelper.NavigateToView<LoadTestDetailViewModel, LoadTestDetailView>(ShellRegions.MAIN_REGION_NAME, item);
        }

        /// <summary>
        /// Gets the ordered results.
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<LoadTest> GetOrderedResults()
        {
            return this.testPlan.LoadTests;
        }

        /// <summary>
        /// Runs the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        protected override void Run(LoadTest item)
        {
            this.runner.RunLoadTests(this.testPlan.Variables, item);
        }

        /// <summary>
        /// Runs all.
        /// </summary>
        protected override void RunAll()
        {
            this.runner.RunLoadTests(this.testPlan.Variables, this.testPlan.LoadTests);
        }
    }
}