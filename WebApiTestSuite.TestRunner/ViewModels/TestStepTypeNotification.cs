﻿using Prism.Interactivity.InteractionRequest;
using WebApiTestSuite.Data;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    internal class TestStepTypeNotification : Confirmation
    {
        public TestStepType SelectedItem { get; internal set; }
    }
}