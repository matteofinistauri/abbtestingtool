﻿using Altran.WPF.MVVM;
using Altran.WPF.Prism;
using Common.WPF.MVVM.DelegateWatchCommand;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.MVVM.ViewModelBase" />
    internal abstract class TestStepListViewModelBase<T> : ViewModelBase
        where T : TestStepParent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestStepListViewModelBase"/> class.
        /// </summary>
        /// <param name="testStepParent">The test step parent.</param>
        public TestStepListViewModelBase(T testStepParent)
        {
            this.TestStepParent = testStepParent;

            this.RunCommand = this.CreateRunCommand();
            this.BackCommand = this.CreateBackCommand();
            this.AddCommand = new DelegateCommand(this.Add);
            this.RemoveCommand = new DelegateCommand<TestStep>(this.Remove);
            this.ItemSelectionRequest = new InteractionRequest<TestStepTypeNotification>();
        }

        /// <summary>
        /// Gets the ok command.
        /// </summary>
        /// <value>
        /// The ok command.
        /// </value>
        public ICommand RunCommand { get; private set; }

        /// <summary>
        /// Gets or sets the back command.
        /// </summary>
        /// <value>
        /// The back command.
        /// </value>
        public ICommand BackCommand { get; private set; }

        /// <summary>
        /// Gets or sets the add step command.
        /// </summary>
        /// <value>
        /// The add step command.
        /// </value>
        public ICommand AddStepCommand { get; private set; }

        /// <summary>
        /// Gets the test case.
        /// </summary>
        /// <value>
        /// The test case.
        /// </value>
        public T TestStepParent { get; private set; }

        /// <summary>
        /// Gets the add command.
        /// </summary>
        /// <value>
        /// The add command.
        /// </value>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the remove command.
        /// </summary>
        /// <value>
        /// The remove command.
        /// </value>
        public ICommand RemoveCommand { get; private set; }

        /// <summary>
        /// Gets the item selection request.
        /// </summary>
        /// <value>
        /// The item selection request.
        /// </value>
        public InteractionRequest<TestStepTypeNotification> ItemSelectionRequest { get; private set; }

        /// <summary>
        /// Gets the HTTP verbs.
        /// </summary>
        /// <value>
        /// The HTTP verbs.
        /// </value>
        public HttpVerb[] HttpVerbs
        {
            get
            {
                return (HttpVerb[])Enum.GetValues(typeof(HttpVerb));
            }
        }

        /// <summary>
        /// Creates the run command.
        /// </summary>
        /// <returns></returns>
        protected virtual ICommand CreateRunCommand()
        {
            return new DelegateWatchCommand<TestDetailViewModel>(this.Run, this.CanRun);
        }

        /// <summary>
        /// Creates the back command.
        /// </summary>
        /// <returns></returns>
        protected virtual ICommand CreateBackCommand()
        {
            return new DelegateWatchCommand<TestDetailViewModel>(this.Back, this.CanBack);
        }

        /// <summary>
        /// Determines whether this instance [can run test case].
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanRun()
        {
            return true;
        }

        /// <summary>
        /// Runs the test case.
        /// </summary>
        protected abstract void Run();

        /// <summary>
        /// Determines whether this instance can back.
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanBack()
        {
            return true;
        }

        /// <summary>
        /// Backs this instance.
        /// </summary>
        protected abstract void Back();

        /// <summary>
        /// Removes the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        private void Remove(TestStep obj)
        {
            this.TestStepParent.Steps.Remove(obj);
        }

        /// <summary>
        /// Adds this instance.
        /// </summary>
        private void Add()
        {
            TestStepTypeNotification notification = new TestStepTypeNotification();
            notification.Title = "Add Item";
            this.ItemSelectionRequest.Raise(notification,
                    returned =>
                    {
                        if (returned.Confirmed)
                        {
                            TestStep testStep = null;
                            switch (returned.SelectedItem)
                            {
                                case TestStepType.Custom:
                                    testStep = new CustomStep();
                                    break;

                                case TestStepType.Macro:
                                    testStep = new MacroStep();
                                    break;

                                default:
                                    throw new Exception("Test type not expected.");
                            }

                            testStep.Parent = this.TestStepParent;
                            this.TestStepParent.Steps.Add(testStep);
                        }
                    });
        }
    }
}