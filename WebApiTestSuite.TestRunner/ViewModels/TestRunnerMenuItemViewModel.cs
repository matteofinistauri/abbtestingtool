﻿using Altran.WPF.Prism;
using Altran.WPF.Windows;
using Altran.WPF.Windows.Events;
using Altran.WPF.Windows.ViewModels;
using Prism.Events;
using WebApiTestSuite.Data;
using WebApiTestSuite.TestRunner.Views;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    public class TestRunnerMenuItemViewModel : MenuItemViewModelBase
    {
        /// <summary>
        /// The event aggregator
        /// </summary>
        private IEventAggregator eventAggregator;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestRunnerMenuItemViewModel"/> class.
        /// </summary>
        /// <param name="eventAggregator">The event aggregator.</param>
        public TestRunnerMenuItemViewModel(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }

        /// <summary>
        /// Selects this instance.
        /// </summary>
        public override void Select()
        {
            var ev = eventAggregator.GetEvent<GetOpenObjectEvent<TestPlan>>();
            var objectContainer = new ObjectContainer<TestPlan>();
            ev.Publish(objectContainer);
            PrismHelper.NavigateToView<TestListViewModel, TestListView>(ShellRegions.MAIN_REGION_NAME, objectContainer.Object);
        }
    }
}