﻿using Altran.WPF.MVVM;
using Common.WPF.MVVM.DelegateWatchCommand;
using Prism.Commands;
using System;
using System.Windows.Input;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.TestRunner.ViewModels
{
    internal class VariablesViewModel : ViewModelBase
    {
        private string name;

        private string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="VariablesViewModel"/> class.
        /// </summary>
        /// <param name="testPlan">The test plan.</param>
        public VariablesViewModel(TestPlan testPlan)
        {
            this.TestPlan = testPlan;
            this.AddCommand = new DelegateWatchCommand<VariablesViewModel>(this.Add, this.CanAdd, this, x => x.Name, x => x.Value);
            this.DeleteCommand = new DelegateCommand<Variable>(this.Delete);
        }

        /// <summary>
        /// Gets the add command.
        /// </summary>
        /// <value>
        /// The add command.
        /// </value>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the delete command.
        /// </summary>
        /// <value>
        /// The delete command.
        /// </value>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the test plan.
        /// </summary>
        /// <value>
        /// The test plan.
        /// </value>
        public TestPlan TestPlan { get; private set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.RaisePropertyChangedEvent(() => this.Name);
                }
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value
        {
            get
            {
                return this.value;
            }

            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    this.RaisePropertyChangedEvent(() => this.Value);
                }
            }
        }

        /// <summary>
        /// Determines whether this instance can add.
        /// </summary>
        /// <returns></returns>
        private bool CanAdd()
        {
            return !string.IsNullOrEmpty(this.Name) && !string.IsNullOrEmpty(this.Value);
        }

        /// <summary>
        /// Adds this instance.
        /// </summary>
        private void Add()
        {
            this.TestPlan.Variables.Add(new Variable(this.Name, this.Value));
            this.Name = string.Empty;
            this.Value = string.Empty;
        }

        /// <summary>
        /// Deletes the specified variable.
        /// </summary>
        /// <param name="variable">The variable.</param>
        private void Delete(Variable variable)
        {
            this.TestPlan.Variables.Remove(variable);
        }
    }
}