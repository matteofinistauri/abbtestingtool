﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;
using WebApiTestSuite.Data.VariablesEngine;
using WebApiTestSuite.TestRunner.Lib;

namespace WebApiTestSuite.TestRunner.Models
{
    /// <summary>
    ///
    /// </summary>
    public class TestCaseRunner
    {
        #region Events

        /// <summary>
        /// Occurs when [test case execution completed].
        /// </summary>
        public event EventHandler TestCaseExecutionCompleted;

        /// <summary>
        /// Occurs when [tests execution completed].
        /// </summary>
        public event EventHandler TestsExecutionCompleted;

        /// <summary>
        /// Occurs when [tests started].
        /// </summary>
        public event EventHandler TestsStarted;

        #endregion Events

        /// <summary>
        /// Runs the test cases.
        /// </summary>
        /// <param name="variables">The variables.</param>
        /// <param name="testCases">The test cases.</param>
        public void RunTestCases(IEnumerable<Variable> variables, IEnumerable<TestCase> testCases)
        {
            this.RunTestCases(variables, testCases.ToArray());
        }

        /// <summary>
        /// Runs the test case.
        /// </summary>
        /// <param name="variables">The variables.</param>
        /// <param name="testCases">The test cases.</param>
        public void RunTestCases(IEnumerable<Variable> variables, params TestCase[] testCases)
        {
            foreach (var testCase in testCases)
            {
                VariablesRepository variablesRepository = new VariablesRepository(Application.Current.Dispatcher);
                variablesRepository.AddGlobalVariables(variables.ToArray());
                var testCaseExecution = new TestCaseExecution(testCase, variablesRepository);
                testCase.LastExecution = testCaseExecution;
                testCaseExecution.ExecutionStatus = ExecutionStatus.Running;
            }

            TestsStarted?.Invoke(this, EventArgs.Empty);

            new Thread(() =>
            {
                TestCaseExecution testCaseExecution = null;
                try
                {
                    foreach (var testCase in testCases)
                    {
                        testCaseExecution = testCase.LastExecution;
                        testCaseExecution.ExecutionDate = DateTime.Now;
                        //var testCaseExecution = testCase.LastExecution;
                        foreach (TestStep testStep in testCase.Steps)
                        {
                            TestStepDefinition testStepDefinition = GetTestStepDefinition(testStep);
                            ExecuteStep(testCaseExecution.VariablesRepository, testCaseExecution, testStep, testStepDefinition);
                        }

                        testCaseExecution.ExecutionStatus = ExecutionStatus.Completed;
                        testCaseExecution.ExecutionDate = DateTime.Now;
                    }
                }
                catch (StepExecutionException ex)
                {
                    this.ManageFault(testCaseExecution, ex);
                }
                finally
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => TestsExecutionCompleted?.Invoke(this, EventArgs.Empty)));
                }
            }).Start();
        }

        /// <summary>
        /// Runs the load tests.
        /// </summary>
        /// <param name="variables">The variables.</param>
        /// <param name="loadTests">The load tests.</param>
        public void RunLoadTests(IEnumerable<Variable> variables, IEnumerable<LoadTest> loadTests)
        {
            this.RunLoadTests(variables, loadTests.ToArray());
        }

        /// <summary>
        /// Runs the load tests.
        /// </summary>
        /// <param name="variables">The variables.</param>
        /// <param name="loadTests">The load tests.</param>
        public void RunLoadTests(IEnumerable<Variable> variables, params LoadTest[] loadTests)
        {
            foreach (var loadTest in loadTests)
            {
                LoadTestExecution loadTestExecution = new LoadTestExecution(loadTest);
                loadTest.LastExecution = loadTestExecution;
                loadTestExecution.ExecutionStatus = ExecutionStatus.Running;
                loadTestExecution.ExecutionDate = DateTime.Now;
            }

            TestsStarted?.Invoke(this, EventArgs.Empty);

            foreach (var loadTest in loadTests)
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                var loadTestExecution = loadTest.LastExecution;
                Task.Run(() =>
                {
                    var task = new Task(() =>
                    {
                        for (int i = 0; i < loadTest.NumberOfConcurrentThreads; i++)
                        {
                            VariablesRepository variablesRepository = new VariablesRepository(Application.Current.Dispatcher);
                            variablesRepository.AddGlobalVariables(variables.ToArray());
                            Task childTask = new Task(x =>
                            {
                                CreateActionForSteps(loadTest.Steps, variablesRepository)?.Invoke();
                            }, i, TaskCreationOptions.AttachedToParent);
                            childTask.Start();
                            if (loadTest.TimeAmongRequests != 0)
                            {
                                Task.Delay(loadTest.TimeAmongRequests).Wait();
                            }
                        }
                    });
                    stopWatch.Start();
                    task.Start();
                    try
                    {
                        task.Wait();
                        if (task.IsCompleted)
                        {
                            Console.WriteLine("Parent task is completed successfully");
                            loadTestExecution.ExecutionStatus = ExecutionStatus.Completed;
                            loadTestExecution.ExecutionDate = DateTime.Now;
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => TestCaseExecutionCompleted?.Invoke(this, EventArgs.Empty)));
                        }
                    }
                    catch (AggregateException ex)
                    {
                        var internalAggregateException = ((AggregateException)ex.InnerExceptions[0]).InnerExceptions[0];
                        this.ManageFault(loadTestExecution, (StepExecutionException)internalAggregateException);
                    }
                    finally
                    {
                        stopWatch.Stop();
                        loadTestExecution.ExecutionTime = stopWatch.Elapsed;
                    }
                });
            }

            Application.Current.Dispatcher.BeginInvoke(new Action(() => TestsExecutionCompleted?.Invoke(this, EventArgs.Empty)));
        }

        /// <summary>
        /// Manages the fault.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="stepsExecution">The steps execution.</param>
        private void ManageFault(Task task, StepsExecution stepsExecution)
        {
            var exception = (StepExecutionException)task.Exception.InnerExceptions[0];
            this.ManageFault(stepsExecution, exception);
        }

        /// <summary>
        /// Manages the fault.
        /// </summary>
        /// <param name="stepsExecution">The steps execution.</param>
        /// <param name="exception">The exception.</param>
        private void ManageFault(StepsExecution stepsExecution, StepExecutionException exception)
        {
            CreateError(stepsExecution, exception.Step, exception.Message);
            Application.Current.Dispatcher.BeginInvoke(new Action(() => TestCaseExecutionCompleted?.Invoke(this, EventArgs.Empty)));
            //Console.WriteLine("Exception... ID = " + stepsExecution.ErrorMessage);
        }

        #region Static Methods

        /// <summary>
        /// Gets the test step definition.
        /// </summary>
        /// <param name="testStep">The test step.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Test step not expected.</exception>
        private static TestStepDefinition GetTestStepDefinition(TestStep testStep)
        {
            var customStep = testStep as CustomStep;
            if (customStep != null)
            {
                return customStep.StepDefinition;
            }

            var macroStep = testStep as MacroStep;
            if (macroStep != null)
            {
                return macroStep.Macro;
            }

            throw new Exception("Test step type not expected.");
        }

        /// <summary>
        /// Creates the action for steps.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="variablesRepository">The variables repository.</param>
        /// <returns></returns>
        private static Action CreateActionForSteps(IEnumerable<TestStep> steps, VariablesRepository variablesRepository)
        {
            Action action = null;
            foreach (TestStep testStep in steps)
            {
                TestStepDefinition testStepDefinition = GetTestStepDefinition(testStep);
                action = AddStepExecutionToAction(action, variablesRepository, testStep, testStepDefinition);
            }

            return action;
        }

        /// <summary>
        /// Adds the step execution to task.
        /// </summary>
        /// <param name="previousAction">The previous successful task.</param>
        /// <param name="variablesRepository">The variables repository.</param>
        /// <param name="testStep">The test step.</param>
        /// <param name="testStepDefinition">The test step definition.</param>
        /// <returns></returns>
        private static Action AddStepExecutionToAction(Action previousAction, VariablesRepository variablesRepository, TestStep testStep, TestStepDefinition testStepDefinition)
        {
            return () =>
            {
                previousAction?.Invoke();
                PerformStep(variablesRepository, testStep, testStepDefinition);
            };
        }

        /// <summary>
        /// Adds the step execution to task.
        /// </summary>
        /// <param name="variablesRepository">The variables repository.</param>
        /// <param name="stepsExecution">The steps execution.</param>
        /// <param name="testStep">The test step.</param>
        /// <param name="testStepDefinition">The test step definition.</param>
        /// <param name="options">The options.</param>
        /// <exception cref="StepExecutionException">Actual result not matching the expected result.</exception>
        private void ExecuteStep(VariablesRepository variablesRepository, StepsExecution stepsExecution, TestStep testStep, TestStepDefinition testStepDefinition)
        {
            try
            {
                PerformStep(variablesRepository, testStep, testStepDefinition);
            }
            catch (StepExecutionException ex)
            {
                this.ManageFault(stepsExecution, ex);
                throw;
            }
        }

        private static void PerformStep(VariablesRepository variablesRepository, TestStep testStep, TestStepDefinition testStepDefinition)
        {
            var requestHeader = testStepDefinition.RequestHeader != null ? variablesRepository.ReplaceVariables(testStepDefinition.RequestHeader) : null;
            var requestBody = testStepDefinition.RequestBody != null ? variablesRepository.ReplaceVariables(testStepDefinition.RequestBody) : null;
            var url = variablesRepository.ReplaceVariables(testStepDefinition.Url);
            //Console.WriteLine("API: " + url + " ID = " + testStepDefinition.Description);
            string result;
            try
            {
                result = WebAPICaller.CallApi(url, testStepDefinition.HttpVerb, requestHeader, requestBody);
            }
            catch (Exception ex)
            {
                throw new StepExecutionException(testStep, ex);
            }

            //Console.WriteLine("Writing variables... ID = " + testStepDefinition.Description);
            if (testStepDefinition.ExpectedResult != null)
            {
                var regexToMatch = VariablesRepository.CreateRegExFromVariables(testStepDefinition.ExpectedResult);
                if (Regex.IsMatch(result, regexToMatch))
                {
                    variablesRepository.StoreVariablesValues(testStepDefinition.ExpectedResult, result);
                }
                else
                {
                    throw new StepExecutionException(testStep, "Actual result not matching the expected result.");
                }
            }
        }

        /// <summary>
        /// Creates the error.
        /// </summary>
        /// <param name="stepsExecution">The steps execution.</param>
        /// <param name="testStep">The test step.</param>
        /// <param name="errorMessage">The error message.</param>
        private static void CreateError(StepsExecution stepsExecution, TestStep testStep, string errorMessage)
        {
            stepsExecution.ExecutionStatus = ExecutionStatus.Completed;
            stepsExecution.Result = TestResult.KO;
            stepsExecution.FailingStep = testStep;
            stepsExecution.ErrorMessage = errorMessage;
        }

        #endregion Static Methods
    }
}