﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTestSuite.TestRunner.Models
{
    public class TestStepParam
    {
        private String name;
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        private String sValue;
        public String Value
        {
            get { return sValue; }
            set { sValue = value; }
        }

        private String type;
        public String Type
        {
            get { return type; }
            set { type = value; }
        }

        private String typeValue;
        public String TypeValue
        {
            get { return typeValue; }
            set { typeValue = value; }
        }

    }
}
