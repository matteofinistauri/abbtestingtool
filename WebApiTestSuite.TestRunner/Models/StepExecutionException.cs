﻿using System;
using WebApiTestSuite.Data;

namespace WebApiTestSuite.TestRunner.Models
{
    public class StepExecutionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StepExecutionException"/> class.
        /// </summary>
        /// <param name="step">The step.</param>
        /// <param name="innerException">The inner exception.</param>
        public StepExecutionException(TestStep step, Exception innerException)
            : base(innerException.Message)
        {
            this.Step = step;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StepExecutionException"/> class.
        /// </summary>
        /// <param name="step">The step.</param>
        /// <param name="message">The message.</param>
        public StepExecutionException(TestStep step, string message)
            : base(message)
        {
            this.Step = step;
        }

        /// <summary>
        /// Gets the step.
        /// </summary>
        /// <value>
        /// The step.
        /// </value>
        public TestStep Step { get; private set; }
    }
}