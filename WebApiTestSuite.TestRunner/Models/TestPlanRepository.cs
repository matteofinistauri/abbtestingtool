﻿using Altran.WPF.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTestSuite.TestRunner.Lib;
using System.Configuration;

namespace WebApiTestSuite.TestRunner.Models
{
    internal class TestPlanRepository: ModelBase
    {
        public TestPlanRepository()
        {

            PlanRepository planRepository = new PlanRepository();
            try
            {
                List<String> listElementRepository = planRepository.getListFilePlan(Utility.getPathRepository());

                this.Plans = new ObservableCollection<TestPlan>();

                foreach (String item in listElementRepository)
                {
                    String[] valElementRepository = item.Split('|');
                    this.Plans.Add(new TestPlan() { PlanName = valElementRepository[0], PlanDate = DateTime.Parse(valElementRepository[1]) });
                }
            }
            catch (Exception ex)
            {
                this.Plans = new ObservableCollection<TestPlan>();
            }

        }


        private static TestPlanRepository instance;

        public static TestPlanRepository Instance
        {
            get
            {
                return instance;
            }
        }

        public ObservableCollection<TestPlan> Plans { get;  set; }
    }
}
