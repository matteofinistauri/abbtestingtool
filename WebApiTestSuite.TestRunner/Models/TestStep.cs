﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace WebApiTestSuite.TestRunner.Models
{
    /// <summary>
    ///
    /// </summary>
    internal class TestStep
    {
        public TestStep()
        {
            //this.TestStepParam = new ObservableCollection<TestStepParam>();
        }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public string Url { get; set; }

        public string UrlType { get; set; }

        //public ObservableCollection<TestStepParam> TestStepParam { get; set; }
        private List<TestStepParam> _testStepParams;
        public ObservableCollection<TestStepParam> TestStepParam
        {
            get
            {
                return new ObservableCollection<TestStepParam>(_testStepParams);
            }
            set { _testStepParams = value.ToList(); }
        }

        public ObservableCollection<TestStepParam> TestStepParamUser
        {
            get
            {
                List<TestStepParam> listParam = (from TestStepParam item in this._testStepParams where item.Type == "user" select item).ToList();
                return new ObservableCollection<TestStepParam>(listParam);
            }
        }

        public String ID { get; set; }

        public string Description { get; set; }

        public String StepType { get; set; }

        public String RequestBody { get; set; }

        public String RequestHeader { get; set; }

        public String State { get; set; }

        public String Result { get; set; }

        public String MsgResult { get; set; }

        public Boolean IsResult
        {
            get
            {
                if (!String.IsNullOrEmpty(this.Result))
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Gets or sets the expected result.
        /// </summary>
        /// <value>
        /// The expected result.
        /// </value>
        public string ExpectedResultType { get; set; }

        public string ExpectedResultValue { get; set; }

        public Boolean IsResultVariable { get; set; }

        public String ResultVariableName { get; set; }
    }
}