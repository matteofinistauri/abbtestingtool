﻿using Altran.WPF.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTestSuite.TestRunner.Models
{
    /// <summary>
    ///
    /// </summary>
    internal class TestCase : ModelBase
    {
        /// <summary>
        /// The description
        /// </summary>
        private string description;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestCase"/> class.
        /// </summary>
        public TestCase()
        {
            //this.Steps = new List<TestStep>();
            this.Steps = new ObservableCollection<TestStep>();
        }

        private String id;
        public String ID
        {
            get { return id; }
            set { id = value; }
        }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                if (this.description != value)
                {
                    this.description = value;
                    this.RaisePropertyChangedEvent(() => this.Description);
                }
            }
        }

        /// <summary>
        /// Gets or sets the steps.
        /// </summary>
        /// <value>
        /// The steps.
        /// </value>
        //public List<TestStep> Steps { get; set; }
        public ObservableCollection<TestStep> Steps { get; set; }

        /// <summary>
        /// Gets the steps count.
        /// </summary>
        /// <value>
        /// The steps count.
        /// </value>
        public int StepsCount { get { return this.Steps.Count; } }
    }
}