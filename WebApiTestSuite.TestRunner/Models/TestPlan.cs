﻿using Altran.WPF.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTestSuite.TestRunner.Models
{
    internal class TestPlan : ModelBase
    {

        private string planName;
        private DateTime planDate;

        public String PlanName
        {
            get { return planName; }
            set { planName = value; }
        }

        public DateTime PlanDate
        {
            get { return planDate; }
            set { planDate = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestPlan"/> class.
        /// </summary>
        public TestPlan()
        {
            this.TestCases = new ObservableCollection<TestCase>();
        }

        /// <summary>
        /// Gets the test cases.
        /// </summary>
        /// <value>
        /// The test cases.
        /// </value>
        public ObservableCollection<TestCase> TestCases { get; set; }
    }
}