﻿using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for TestStepTypeSelectionView.xaml
    /// </summary>
    public partial class TestStepTypeSelectionView : UserControl
    {
        public TestStepTypeSelectionView()
        {
            InitializeComponent();
        }
    }
}