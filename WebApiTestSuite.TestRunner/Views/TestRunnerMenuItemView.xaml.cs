﻿using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for TestRunnerMenuItemView.xaml
    /// </summary>
    public partial class TestRunnerMenuItemView : UserControl
    {
        public TestRunnerMenuItemView()
        {
            InitializeComponent();
        }
    }
}