﻿using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for VariablesView.xaml
    /// </summary>
    public partial class VariablesView : UserControl
    {
        public VariablesView()
        {
            InitializeComponent();
        }
    }
}