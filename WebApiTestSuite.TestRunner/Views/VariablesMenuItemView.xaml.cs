﻿using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for VariablesMenuItemView.xaml
    /// </summary>
    public partial class VariablesMenuItemView : UserControl
    {
        public VariablesMenuItemView()
        {
            InitializeComponent();
        }
    }
}