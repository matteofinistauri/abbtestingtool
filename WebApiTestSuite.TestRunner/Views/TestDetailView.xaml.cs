﻿using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for TestDetailView.xaml
    /// </summary>
    public partial class TestDetailView : TestStepListViewBase
    {
        public TestDetailView()
        {
            InitializeComponent();
        }
    }
}