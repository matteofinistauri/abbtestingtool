﻿using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for TestStepControl.xaml
    /// </summary>
    public partial class TestStepControl : UserControl
    {
        public TestStepControl()
        {
            InitializeComponent();
        }
    }
}