﻿using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for TestCaseExecutionDetailsControl.xaml
    /// </summary>
    public partial class TestCaseExecutionDetailsControl : UserControl
    {
        public TestCaseExecutionDetailsControl()
        {
            InitializeComponent();
        }
    }
}