﻿using System.Windows;
using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for TestListView.xaml
    /// </summary>
    public partial class TestListView : ListViewBase
    {
        public TestListView()
        {
            InitializeComponent();
        }
    }
}