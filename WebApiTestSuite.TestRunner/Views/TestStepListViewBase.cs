﻿using System.Windows;
using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="System.Windows.Controls.UserControl" />
    [TemplatePart(Name = "PART_DetailsContentPresenter", Type = typeof(ContentPresenter))]
    public class TestStepListViewBase : UserControl
    {
        /// <summary>
        /// The details content.
        /// </summary>
        public static readonly DependencyProperty BottomPageContentProperty =
                DependencyProperty.Register("BottomPageContent", typeof(object), typeof(TestStepListViewBase), new UIPropertyMetadata(null));

        /// <summary>
        /// Initializes the <see cref="TestSuiteListViewBase"/> class.
        /// </summary>
        static TestStepListViewBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TestStepListViewBase), new FrameworkPropertyMetadata(typeof(TestStepListViewBase)));
        }

        /// <summary>
        /// Gets or sets the content of the details.
        /// </summary>
        /// <value>
        /// The content of the details.
        /// </value>
        public object BottomPageContent
        {
            get { return (object)GetValue(BottomPageContentProperty); }
            set { SetValue(BottomPageContentProperty, value); }
        }
    }
}