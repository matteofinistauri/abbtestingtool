﻿using System;
using System.Windows;
using System.Windows.Controls;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="System.Windows.Controls.DataTemplateSelector" />
    internal class TestTypeDataTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate" /> based on custom logic.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        /// Returns a <see cref="T:System.Windows.DataTemplate" /> or null. The default value is null.
        /// </returns>
        /// <exception cref="System.Exception">Data template for type  + testType.GetType().Name +  not found.</exception>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;
            TestStep testType = (TestStep)item;
            if (testType is CustomStep)
            {
                return element.FindResource("CustomStepDataTemplate") as DataTemplate;
            }
            else if (testType is MacroStep)
            {
                return element.FindResource("MacroStepDataTemplate") as DataTemplate;
            }
            else
            {
                throw new Exception("Data template for type '" + testType.GetType().Name + "' not found.");
            }
        }
    }
}