﻿using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    /// Interaction logic for MacrosMenuItemView.xaml
    /// </summary>
    public partial class MacrosMenuItemView : UserControl
    {
        public MacrosMenuItemView()
        {
            InitializeComponent();
        }
    }
}