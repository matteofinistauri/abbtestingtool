﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WebApiTestSuite.TestRunner.Views
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="System.Windows.Controls.UserControl" />
    [TemplatePart(Name = "PART_DetailsContentPresenter", Type = typeof(ContentPresenter))]
    public class ListViewBase : UserControl
    {
        /// <summary>
        /// The details content.
        /// </summary>
        public static readonly DependencyProperty BottomPageContentProperty =
                DependencyProperty.Register("BottomPageContent", typeof(object), typeof(ListViewBase), new UIPropertyMetadata(null));

        public static readonly DependencyProperty ItemDataTemplateProperty =
                DependencyProperty.Register("ItemDataTemplate", typeof(DataTemplate), typeof(ListViewBase), new UIPropertyMetadata(null));

        /// <summary>
        /// Initializes the <see cref="TestSuiteListViewBase"/> class.
        /// </summary>
        static ListViewBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ListViewBase), new FrameworkPropertyMetadata(typeof(ListViewBase)));
        }

        /// <summary>
        /// Gets or sets the content of the details.
        /// </summary>
        /// <value>
        /// The content of the details.
        /// </value>
        public object BottomPageContent
        {
            get { return (object)GetValue(BottomPageContentProperty); }
            set { SetValue(BottomPageContentProperty, value); }
        }

        /// <summary>
        /// Gets or sets the item data template.
        /// </summary>
        /// <value>
        /// The item data template.
        /// </value>
        public DataTemplate ItemDataTemplate
        {
            get { return (DataTemplate)GetValue(ItemDataTemplateProperty); }
            set { SetValue(ItemDataTemplateProperty, value); }
        }
    }
}