﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.TestRunner.Converters
{
    internal class TestCaseExecutionToExecutionStatusIconConverter : IMultiValueConverter
    {
        //        <Rectangle Style = "{StaticResource MyRectangle}" >
        //    < Rectangle.Fill >
        //        < VisualBrush Stretch="Fill" Visual="{DynamicResource appbar_arrow_down}" />
        //    </Rectangle.Fill>
        //</Rectangle>

        /// <summary>
        /// The frame elem
        ///
        /// </summary>
        private FrameworkElement frameElem = new FrameworkElement();

        private const string ICONS_PATH = "/WebApiTestSuite.Common;Component/Icons/";

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(values[0] is ExecutionStatus && values[1] is TestResult))
            {
                return null;
            }

            Rectangle rectangle = new Rectangle();

            ExecutionStatus status = (ExecutionStatus)values[0];
            TestResult result = (TestResult)values[1];
            switch (status)
            {
                case ExecutionStatus.NotExecuted:
                    return new BitmapImage(new Uri(ICONS_PATH + "Waiting.png", UriKind.Relative));

                case ExecutionStatus.Running:
                    return new BitmapImage(new Uri(ICONS_PATH + "Run.png", UriKind.Relative));

                case ExecutionStatus.Interrupted:
                    return new BitmapImage(new Uri(ICONS_PATH + "Interrupted.png", UriKind.Relative));

                case ExecutionStatus.Completed:
                    if (result == TestResult.OK)
                    {
                        return new BitmapImage(new Uri(ICONS_PATH + "OK.png", UriKind.Relative));
                    }
                    else
                    {
                        return new BitmapImage(new Uri(ICONS_PATH + "KO.png", UriKind.Relative));
                    }

                default:
                    throw new Exception("Execution status not expected.");
            }
        }

        /// <summary>
        /// Converts a binding target value to the source binding values.
        /// </summary>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// An array of values that have been converted from the target value back to the source values.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}