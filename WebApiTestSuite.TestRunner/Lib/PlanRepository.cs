﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTestSuite.TestRunner.Models;
using System.IO;

namespace WebApiTestSuite.TestRunner.Lib
{
    public class PlanRepository
    {

        public PlanRepository()
        {

        }

        public List<String> getListFilePlan(String pathRepository)
        {
            List<String> list = new List<String>();

            try
            {
                DirectoryInfo di = new DirectoryInfo(pathRepository);
                // Create an array representing the files in the current directory.
                FileInfo[] fi = di.GetFiles();

                foreach (FileInfo item in fi)
                {
                    String currentPlan = item.Name + "|" + item.LastWriteTime.ToString();

                    list.Add(currentPlan);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return list;
        }
    }
}
