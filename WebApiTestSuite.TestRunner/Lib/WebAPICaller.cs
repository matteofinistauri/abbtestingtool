﻿using System;
using System.IO;
using System.Net;
using System.Text;
using WebApiTestSuite.Data;

namespace WebApiTestSuite.TestRunner.Lib
{
    public static class WebAPICaller
    {
        /// <summary>
        /// Calls the API asynchronous.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="httpVerb">The HTTP verb.</param>
        /// <param name="header">The header.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        public static string CallApi(string url, HttpVerb httpVerb, string header, string body)
        {
            //CredentialCache cache = new CredentialCache();
            //NetworkCredential nc = new NetworkCredential();
            //cache.Add(new Uri(uri), "Basic", nc);

            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //request.Timeout = 30000;
            //request.ReadWriteTimeout = 30000;
            //request.PreAuthenticate = true;
            //request.UseDefaultCredentials = true;
            //request.KeepAlive = false;
            //request.ProtocolVersion = HttpVersion.Version10;
            //request.Method = type.ToString().ToUpper();
            //request.ContentType = "application/json";

            //switch (type)
            //{
            //    case HttpVerb.POST:
            //        if (!string.IsNullOrEmpty(body))
            //        {
            //            byte[] postBytes = Encoding.ASCII.GetBytes(body);
            //            request.ContentLength = postBytes.Length;
            //            Stream requestStream = request.GetRequestStream();
            //            requestStream.Write(postBytes, 0, postBytes.Length);
            //            requestStream.Close();
            //        }

            //        break;
            //}

            //if (!string.IsNullOrEmpty(header))
            //{
            //    request.Headers.Add(header);
            //}

            //HttpWebResponse response = (HttpWebResponse)(request.GetResponse());
            //return new StreamReader(response.GetResponseStream()).ReadToEnd();
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                if (header != null)
                {
                    client.Headers.Add(header);
                }

                switch (httpVerb)
                {
                    case HttpVerb.GET:
                        return client.DownloadString(url);

                    case HttpVerb.POST:
                    case HttpVerb.DELETE:
                    case HttpVerb.PUT:
                        return client.UploadString(url, httpVerb.ToString(), body);

                    default:
                        throw new Exception("Http Verb not expected.");
                }
            }
        }
    }
}