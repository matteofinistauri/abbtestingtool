﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace WebApiTestSuite.TestRunner.Lib
{
    public static class Utility
    {
        public const String STEP_STATE_WAIT = "WAIT";
        public const String STEP_STATE_EXECUTED = "EXECUTED";
        public const String STEP_STATE_RUNNING = "RUNNING";
        public const String STEP_TYPE_REQ_URL = "REQ_URL";
        public const String STEP_TYPE_REQ_TOKEN = "REQ_TOKEN";

        public const String CLR_LOG = "------------------------------------------------------------------------------------------------------------";

        public static String getPathRepository()
        {
            return ConfigurationManager.AppSettings["pathRepositoryPlan"].ToString();
        }

        public static String getHostWebAPI()
        {
            return ConfigurationManager.AppSettings["HostWebAPI"].ToString();
        }

        public static String getPathFileLog()
        {
            return ConfigurationManager.AppSettings["PathFileLog"].ToString();
        }

        public static String getFileLogName()
        {
            return ConfigurationManager.AppSettings["FileLog"].ToString();
        }

        public static void ScriviFileLog(string contentFile)
        {
            if (!Directory.Exists(getPathFileLog()))
                Directory.CreateDirectory(getPathFileLog());
            if (!File.Exists(getPathFileLog() + "/" + getFileLogName()))
                File.Create(getPathFileLog() + "/" + getFileLogName());

            using (StreamWriter w = File.AppendText(getPathFileLog() + "/" + getFileLogName()))
            {
                w.WriteLine(contentFile);
            }
        }
    }
}
