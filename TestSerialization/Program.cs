﻿using System;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Models;
using WebApiTestSuite.Data.Serialization;

namespace TestSerialization
{
    internal class Program
    {
        /// <summary>
        /// Mains the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void Main(string[] args)
        {
            TestPlan testPlan = new TestPlan() { Name = "My Test Plan", Date = DateTime.Now };
            testPlan.Macros = new System.Collections.ObjectModel.ObservableCollection<TestStepDefinition>();
            testPlan.Macros.Add(new TestStepDefinition()
            {
                Description = "Token request to the system",
                HttpVerb = HttpVerb.POST,
                Url = "{{environment}}/oauth/token",
                RequestBody = "grant_type=password&username={{user}}&password={{password}}",
                ExpectedResult = "\"access_token\":\"{{token}}\",\"token_type\":\"bearer\",\"expires_in\":{{expiresIn}},\"userName\":\"{{user}}\",\".issued\":\"{{issuedDate}}\",\".expires\":\"{{expires}}\""
            });
            for (int i = 0; i < 30; i++)
            {
                AddTestCase(testPlan, i);
            }

            testPlan.Variables.Add(new Variable() { Name = "user", Value = "matteo.finistauri@altran.com" });
            testPlan.Variables.Add(new Variable() { Name = "password", Value = "Matteo82" });
            testPlan.Variables.Add(new Variable() { Name = "environment", Value = "https://epbsdevwabiotwebapi.azurewebsites.net" });

            {
                var loadTest = new LoadTest();
                loadTest.Description = "My load test";
                loadTest.NumberOfConcurrentThreads = 10;
                {
                    TestStep testStep = new MacroStep()
                    {
                        MacroDescription = testPlan.Macros[0].Description
                    };

                    loadTest.Steps.Add(testStep);
                }
                testPlan.LoadTests.Add(loadTest);
            }

            TestPlanSerializer.Serialize(testPlan, @"Test.xml");
        }

        /// <summary>
        /// Adds the test case.
        /// </summary>
        /// <param name="testPlan">The test plan.</param>
        /// <param name="i">The i.</param>
        private static void AddTestCase(TestPlan testPlan, int i)
        {
            {
                TestCase testCase = new TestCase() { Description = "Update a plant without the authorization token", ID = "TC.CO." + i + ".1" };
                {
                    TestStep testStep = new MacroStep()
                    {
                        MacroDescription = testPlan.Macros[0].Description
                    };
                    testCase.Steps.Add(testStep);
                }
                {
                    TestStep testStep = new CustomStep()
                    {
                        StepDefinition = new TestStepDefinition()
                        {
                            Description = "Create a request to /api/plant/5 using the PUT web method and a valid (existing) value for the plantId parameter",
                            Url = "{{environment}}/api/plant",
                            RequestHeader = "Authorization: Bearer {{token}}"
                        }
                    };
                    testCase.Steps.Add(testStep);
                }

                testPlan.TestCases.Add(testCase);
            }

            {
                TestCase testCase = new TestCase() { Description = "Request a token", ID = "TC.CO." + i + ".1" };
                {
                    TestStep testStep = new MacroStep()
                    {
                        MacroDescription = testPlan.Macros[0].Description
                    };
                    testCase.Steps.Add(testStep);
                }
                testPlan.TestCases.Add(testCase);
            }
        }
    }
}