﻿using Altran.WPF.MVVM;
using System;
using WebApiTestSuite.Data.VariablesEngine;

namespace WebApiTestSuite.Data.Models
{
    /// <summary>
    ///
    /// </summary>
    public class TestCaseExecution : StepsExecution
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TestCaseExecution" /> class.
        /// </summary>
        /// <param name="testCase">The test case.</param>
        /// <param name="variablesRepository">The variables repository.</param>
        public TestCaseExecution(TestCase testCase, VariablesRepository variablesRepository)
        {
            this.TestCase = testCase;
            this.VariablesRepository = variablesRepository;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the test case.
        /// </summary>
        /// <value>
        /// The test case.
        /// </value>
        public TestCase TestCase { get; private set; }

        /// <summary>
        /// Gets the index of the failing step.
        /// </summary>
        /// <value>
        /// The index of the failing step.
        /// </value>
        public override int FailingStepIndex
        {
            get
            {
                return this.TestCase.Steps.IndexOf(this.FailingStep);
            }
        }

        /// <summary>
        /// Gets or sets the variables repository.
        /// </summary>
        /// <value>
        /// The variables repository.
        /// </value>
        public VariablesRepository VariablesRepository { get; private set; }

        #endregion Properties
    }

    /// <summary>
    ///
    /// </summary>
    public enum TestResult
    {
        OK,
        KO,
    }

    /// <summary>
    ///
    /// </summary>
    public enum ExecutionStatus
    {
        NotExecuted,
        Running,
        Interrupted,
        Completed
    }
}