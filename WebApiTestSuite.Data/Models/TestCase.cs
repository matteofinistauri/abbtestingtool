﻿using Altran.WPF.MVVM;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.Data
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.MVVM.ModelBase" />
    /// <seealso cref="System.Collections.Generic.IEnumerable{WebApiTestSuite.Data.TestStep}" />
    public class TestCase : TestStepParent
    {
        #region Fields

        /// <summary>
        /// The identifier
        /// </summary>
        private string id;

        /// <summary>
        /// The description
        /// </summary>
        private string description;

        /// <summary>
        /// The last execution
        /// </summary>
        private TestCaseExecution lastExecution = null;

        #endregion Fields

        #region Properties

        #region Serializable Properties

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [XmlAttribute]
        public string ID
        {
            get
            {
                return id;
            }

            set
            {
                if (this.id != value)
                {
                    this.id = value;
                    this.RaisePropertyChangedEvent(() => this.ID);
                }
            }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [XmlAttribute]
        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                if (this.description != value)
                {
                    this.description = value;
                    this.RaisePropertyChangedEvent(() => this.Description);
                }
            }
        }

        #endregion Serializable Properties

        #region Not Serialized Properties

        /// <summary>
        /// Gets a value indicating whether this instance is last execution present.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is last execution present; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool IsLastExecutionPresent
        {
            get
            {
                return this.LastExecution != null;
            }
        }

        /// <summary>
        /// Gets the group.
        /// </summary>
        /// <value>
        /// The group.
        /// </value>
        [XmlIgnore]
        public string Group
        {
            get
            {
                if (LastExecution != null)
                {
                    if (LastExecution.ExecutionStatus == ExecutionStatus.Completed)
                    {
                        return LastExecution.Result.ToString();
                    }
                    else
                    {
                        return LastExecution.ExecutionStatus.ToString();
                    }
                }

                return "Not executed";
            }
        }

        /// <summary>
        /// Gets or sets the last execution.
        /// </summary>
        /// <value>
        /// The last execution.
        /// </value>
        [XmlIgnore]
        public TestCaseExecution LastExecution
        {
            get
            {
                return this.lastExecution;
            }

            set
            {
                if (this.lastExecution != value)
                {
                    this.lastExecution = value;
                    this.RaisePropertyChangedEvent(() => this.LastExecution);
                    this.RaisePropertyChangedEvent(() => this.IsLastExecutionPresent);
                    this.RaisePropertyChangedEvent(() => this.Group);
                }
            }
        }

        #endregion Not Serialized Properties

        #endregion Properties
    }
}