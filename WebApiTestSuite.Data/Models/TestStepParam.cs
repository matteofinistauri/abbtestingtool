﻿namespace WebApiTestSuite.Data
{
    /// <summary>
    ///
    /// </summary>
    public class TestStepParam
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the type value.
        /// </summary>
        /// <value>
        /// The type value.
        /// </value>
        public string TypeValue { get; set; }
    }
}