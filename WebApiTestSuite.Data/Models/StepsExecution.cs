﻿using Altran.WPF.MVVM;
using System;
using WebApiTestSuite.Data.VariablesEngine;

namespace WebApiTestSuite.Data.Models
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.MVVM.ModelBase" />
    public abstract class StepsExecution : ModelBase
    {
        #region Fields

        /// <summary>
        /// The test result
        /// </summary>
        private TestResult result;

        /// <summary>
        /// The execution status
        /// </summary>
        private ExecutionStatus executionStatus;

        /// <summary>
        /// The execution date
        /// </summary>
        private DateTime executionDate;

        /// <summary>
        /// The failing step
        /// </summary>
        private TestStep failingStep;

        /// <summary>
        /// The error message
        /// </summary>
        private string errorMessage;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the execution status.
        /// </summary>
        /// <value>
        /// The execution status.
        /// </value>
        public ExecutionStatus ExecutionStatus
        {
            get
            {
                return this.executionStatus;
            }

            set
            {
                if (this.executionStatus != value)
                {
                    this.executionStatus = value;
                    this.RaisePropertyChangedEvent(() => this.ExecutionStatus);
                    this.RaisePropertyChangedEvent(() => this.IsExecutionSucceeded);
                    this.RaisePropertyChangedEvent(() => this.IsExecutionFailed);
                    this.RaisePropertyChangedEvent(() => this.IsExecutionCompleted);
                    this.RaisePropertyChangedEvent(() => this.IsRunning);
                }
            }
        }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public TestResult Result
        {
            get
            {
                return this.result;
            }

            set
            {
                if (this.result != value)
                {
                    this.result = value;
                    this.RaisePropertyChangedEvent(() => this.Result);
                    this.RaisePropertyChangedEvent(() => this.IsExecutionSucceeded);
                    this.RaisePropertyChangedEvent(() => this.IsExecutionFailed);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether [execution succeeded].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [execution succeeded]; otherwise, <c>false</c>.
        /// </value>
        public bool IsExecutionSucceeded
        {
            get
            {
                return this.Result == TestResult.OK && this.executionStatus == ExecutionStatus.Completed;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [execution failed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [execution failed]; otherwise, <c>false</c>.
        /// </value>
        public bool IsExecutionFailed
        {
            get
            {
                return this.Result == TestResult.KO && this.executionStatus == ExecutionStatus.Completed;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is execution completed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is execution completed; otherwise, <c>false</c>.
        /// </value>
        public bool IsExecutionCompleted
        {
            get
            {
                return this.executionStatus == ExecutionStatus.Completed;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is running.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is running; otherwise, <c>false</c>.
        /// </value>
        public bool IsRunning
        {
            get
            {
                return this.executionStatus == ExecutionStatus.Running;
            }
        }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                if (this.errorMessage != value)
                {
                    this.errorMessage = value;
                    this.RaisePropertyChangedEvent(() => this.ErrorMessage);
                }
            }
        }

        /// <summary>
        /// Gets or sets the failing step.
        /// </summary>
        /// <value>
        /// The failing step.
        /// </value>
        public TestStep FailingStep
        {
            get
            {
                return this.failingStep;
            }

            set
            {
                if (this.failingStep != value)
                {
                    this.failingStep = value;
                    this.RaisePropertyChangedEvent(() => this.FailingStep);
                    this.RaisePropertyChangedEvent(() => this.FailingStepIndex);
                }
            }
        }

        /// <summary>
        /// Gets the index of the failing step.
        /// </summary>
        /// <value>
        /// The index of the failing step.
        /// </value>
        public abstract int FailingStepIndex { get; }

        /// <summary>
        /// Gets or sets the execution date.
        /// </summary>
        /// <value>
        /// The execution date.
        /// </value>
        public DateTime ExecutionDate
        {
            get
            {
                return this.executionDate;
            }

            set
            {
                if (this.executionDate != value)
                {
                    this.executionDate = value;
                    this.RaisePropertyChangedEvent(() => this.ExecutionDate);
                }
            }
        }

        #endregion Properties
    }
}