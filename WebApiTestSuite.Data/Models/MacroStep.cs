﻿using System.Xml.Serialization;

namespace WebApiTestSuite.Data.Models
{
    public class MacroStep : TestStep
    {
        /// <summary>
        /// The test step definition
        /// </summary>
        private TestStepDefinition macro;

        /// <summary>
        /// Gets or sets the selected macro.
        /// </summary>
        /// <value>
        /// The selected macro.
        /// </value>
        [XmlIgnore]
        public TestStepDefinition Macro
        {
            get
            {
                return this.macro;
            }

            set
            {
                this.macro = value;
                this.MacroDescription = this.macro?.Description;
                this.RaisePropertyChangedEvent(() => this.StepDescription);
            }
        }

        /// <summary>
        /// Gets or sets the selected macro description.
        /// </summary>
        /// <value>
        /// The selected macro description.
        /// </value>
        public string MacroDescription { get; set; }

        /// <summary>
        /// Gets the step description.
        /// </summary>
        /// <value>
        /// The step description.
        /// </value>
        [XmlIgnore]
        public override string StepDescription
        {
            get
            {
                return MacroDescription;
            }
        }
    }
}