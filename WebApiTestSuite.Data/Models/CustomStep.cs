﻿using System.Xml.Serialization;

namespace WebApiTestSuite.Data.Models
{
    public class CustomStep : TestStep
    {
        public CustomStep()
        {
            StepDefinition = new TestStepDefinition();
        }

        /// <summary>
        /// Gets or sets the step definition.
        /// </summary>
        /// <value>
        /// The step definition.
        /// </value>
        public TestStepDefinition StepDefinition { get; set; }

        /// <summary>
        /// Gets the step description.
        /// </summary>
        /// <value>
        /// The step description.
        /// </value>
        [XmlIgnore]
        public override string StepDescription
        {
            get
            {
                return this.StepDefinition.Description;
            }
        }
    }
}