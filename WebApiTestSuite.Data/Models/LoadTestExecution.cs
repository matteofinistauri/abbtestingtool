﻿using Altran.WPF.MVVM;
using System;
using WebApiTestSuite.Data.Models;
using WebApiTestSuite.Data.VariablesEngine;

namespace WebApiTestSuite.Data.Models
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.MVVM.ModelBase" />
    public class LoadTestExecution : StepsExecution
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestCaseExecution"/> class.
        /// </summary>
        /// <param name="loadTest">The load test.</param>
        /// <param name="variablesRepository">The variables repository.</param>
        public LoadTestExecution(LoadTest loadTest)
        {
            this.LoadTest = loadTest;
        }

        /// <summary>
        /// Gets or sets the load test.
        /// </summary>
        /// <value>
        /// The load test.
        /// </value>
        public LoadTest LoadTest { get; private set; }

        /// <summary>
        /// Gets the index of the failing step.
        /// </summary>
        /// <value>
        /// The index of the failing step.
        /// </value>
        public override int FailingStepIndex
        {
            get
            {
                return this.LoadTest.Steps.IndexOf(this.FailingStep);
            }
        }

        private TimeSpan executionTime;

        /// <summary>
        /// Gets or sets the execution time.
        /// </summary>
        /// <value>
        /// The execution time.
        /// </value>
        public TimeSpan ExecutionTime
        {
            get
            {
                return this.executionTime;
            }

            set
            {
                if (this.executionTime != value)
                {
                    this.executionTime = value;
                    this.RaisePropertyChangedEvent(() => this.ExecutionTime);
                }
            }
        }
    }
}