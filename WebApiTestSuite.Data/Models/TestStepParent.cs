﻿using Altran.WPF.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WebApiTestSuite.Data.Models
{
    /// <summary>
    ///
    /// </summary>
    public class TestStepParent : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestStepParent"/> class.
        /// </summary>
        public TestStepParent()
        {
            this.Steps = new ObservableCollection<TestStep>();
        }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        [XmlIgnore]
        public TestPlan Parent { get; set; }

        /// <summary>
        /// Gets or sets the steps.
        /// </summary>
        /// <value>
        /// The steps.
        /// </value>
        public ObservableCollection<TestStep> Steps { get; set; }

        /// <summary>
        /// Gets the steps count.
        /// </summary>
        /// <value>
        /// The steps count.
        /// </value>
        [XmlIgnore]
        public int StepsCount { get { return this.Steps.Count; } }
    }
}