﻿using Altran.WPF.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WebApiTestSuite.Data.Models
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="WebApiTestSuite.Data.Models.TestStepParent" />
    public class LoadTest : TestStepParent
    {
        /// <summary>
        /// The description
        /// </summary>
        private string description;

        /// <summary>
        /// The number of concurrent threads
        /// </summary>
        private int numberOfConcurrentThreads;

        /// <summary>
        /// The last execution
        /// </summary>
        private LoadTestExecution lastExecution = null;

        /// <summary>
        /// The time among requests
        /// </summary>
        private int timeAmongRequests;

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [XmlAttribute]
        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                if (this.description != value)
                {
                    this.description = value;
                    this.RaisePropertyChangedEvent(() => this.Description);
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of concurrent threads.
        /// </summary>
        /// <value>
        /// The number of concurrent threads.
        /// </value>
        public int NumberOfConcurrentThreads
        {
            get
            {
                return this.numberOfConcurrentThreads;
            }
            set
            {
                if (this.numberOfConcurrentThreads != value)
                {
                    this.numberOfConcurrentThreads = value;
                    this.RaisePropertyChangedEvent(() => this.NumberOfConcurrentThreads);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is last execution present.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is last execution present; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool IsLastExecutionPresent
        {
            get
            {
                return this.LastExecution != null;
            }
        }

        /// <summary>
        /// Gets or sets the last execution.
        /// </summary>
        /// <value>
        /// The last execution.
        /// </value>
        [XmlIgnore]
        public LoadTestExecution LastExecution
        {
            get
            {
                return this.lastExecution;
            }

            set
            {
                if (this.lastExecution != value)
                {
                    this.lastExecution = value;
                    this.RaisePropertyChangedEvent(() => this.LastExecution);
                    this.RaisePropertyChangedEvent(() => this.IsLastExecutionPresent);
                    //this.RaisePropertyChangedEvent(() => this.Group);
                }
            }
        }

        /// <summary>
        /// Gets or sets the time among requests.
        /// </summary>
        /// <value>
        /// The time among requests.
        /// </value>
        public int TimeAmongRequests
        {
            get
            {
                return this.timeAmongRequests;
            }

            set
            {
                if (this.timeAmongRequests != value)
                {
                    this.timeAmongRequests = value;
                    this.RaisePropertyChangedEvent(() => this.TimeAmongRequests);
                }
            }
        }
    }
}