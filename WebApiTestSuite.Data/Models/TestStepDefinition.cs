﻿using System.Xml.Serialization;

namespace WebApiTestSuite.Data.Models
{
    /// <summary>
    ///
    /// </summary>
    public class TestStepDefinition
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [XmlAttribute]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the type of the URL.
        /// </summary>
        /// <value>
        /// The type of the URL.
        /// </value>
        [XmlAttribute]
        public HttpVerb HttpVerb { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the request header.
        /// </summary>
        /// <value>
        /// The request header.
        /// </value>
        public string RequestHeader { get; set; }

        /// <summary>
        /// Gets or sets the request body.
        /// </summary>
        /// <value>
        /// The request body.
        /// </value>
        public string RequestBody { get; set; }

        /// <summary>
        /// Gets or sets the expected result.
        /// </summary>
        /// <value>
        /// The expected result.
        /// </value>
        public string ExpectedResult { get; set; }
    }
}