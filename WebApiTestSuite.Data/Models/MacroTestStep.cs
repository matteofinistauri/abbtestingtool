﻿using Altran.WPF.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WebApiTestSuite.Data.Models
{
    public class MacroTestStep : TestStep
    {
        /// <summary>
        /// The linked macro
        /// </summary>
        private TestStepDefinition linkedMacro;

        /// <summary>
        /// Gets or sets the linked macro.
        /// </summary>
        /// <value>
        /// The linked macro.
        /// </value>
        [XmlIgnore]
        public TestStepDefinition LinkedMacro
        {
            get
            {
                return linkedMacro;
            }
            set
            {
                if (linkedMacro != value)
                {
                    this.linkedMacro = value;
                    this.RaisePropertyChangedEvent(() => this.LinkedMacro);
                }
            }
        }

        /// <summary>
        /// Gets the linked macro description.
        /// </summary>
        /// <value>
        /// The linked macro description.
        /// </value>
        public string LinkedMacroDescription { get; set; }
    }
}