﻿using Altran.WPF.MVVM;

namespace WebApiTestSuite.Data.Models
{
    public class Variable : ModelBase
    {
        /// <summary>
        /// The name
        /// </summary>
        private string name;

        /// <summary>
        /// The value
        /// </summary>
        private string value;

        public Variable()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Variable" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public Variable(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.RaisePropertyChangedEvent(() => this.Name);
                }
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value
        {
            get
            {
                return value;
            }

            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    this.RaisePropertyChangedEvent(() => this.Value);
                }
            }
        }
    }
}