﻿using Altran.WPF.MVVM;
using System.Xml.Serialization;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.Data
{
    /// <summary>
    ///
    /// </summary>
    [XmlInclude(typeof(MacroStep))]
    [XmlInclude(typeof(CustomStep))]
    public abstract class TestStep : ModelBase
    {
        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        [XmlIgnore]
        public TestStepParent Parent { get; set; }

        /// <summary>
        /// Gets the step description.
        /// </summary>
        /// <value>
        /// The step description.
        /// </value>
        [XmlIgnore]
        public abstract string StepDescription { get; }
    }

    /// <summary>
    ///
    /// </summary>
    public enum HttpVerb
    {
        GET,
        POST,
        DELETE,
        PUT
    }

    /// <summary>
    ///
    /// </summary>
    public enum TestStepType
    {
        Custom,
        Macro
    }
}