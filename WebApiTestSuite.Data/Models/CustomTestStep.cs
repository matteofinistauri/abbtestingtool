﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTestSuite.Data.Models
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="WebApiTestSuite.Data.Models.TestStepDefinition" />
    public class CustomTestStep : TestStep
    {
        /// <summary>
        /// The linked macro
        /// </summary>
        private TestStepDefinition definition;

        /// <summary>
        /// Gets or sets the linked macro.
        /// </summary>
        /// <value>
        /// The linked macro.
        /// </value>
        public TestStepDefinition Definition
        {
            get
            {
                return definition;
            }
            set
            {
                if (definition != value)
                {
                    this.definition = value;
                    this.RaisePropertyChangedEvent(() => this.Definition);
                }
            }
        }
    }
}