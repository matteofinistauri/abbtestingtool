﻿using Altran.WPF.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.Data
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.MVVM.ModelBase" />
    /// <seealso cref="System.Collections.Generic.IEnumerable{WebApiTestSuite.Data.TestCase}" />
    [XmlRoot("TestPlan")]
    public class TestPlan : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestPlan"/> class.
        /// </summary>
        public TestPlan()
        {
            this.TestCases = new ObservableCollection<TestCase>();
            this.Macros = new ObservableCollection<TestStepDefinition>();
            this.Variables = new ObservableCollection<Variable>();
            this.LoadTests = new ObservableCollection<LoadTest>();
        }

        /// <summary>
        /// Gets or sets the name of the plan.
        /// </summary>
        /// <value>
        /// The name of the plan.
        /// </value>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the plan.
        /// </summary>
        /// <value>
        /// The file name of the plan.
        /// </value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the plan date.
        /// </summary>
        /// <value>
        /// The plan date.
        /// </value>
        [XmlAttribute]
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets the test cases.
        /// </summary>
        /// <value>
        /// The test cases.
        /// </value>
        public ObservableCollection<TestCase> TestCases { get; set; }

        /// <summary>
        /// Gets or sets the macros.
        /// </summary>
        /// <value>
        /// The macros.
        /// </value>
        public ObservableCollection<TestStepDefinition> Macros { get; set; }

        /// <summary>
        /// Gets or sets the variables.
        /// </summary>
        /// <value>
        /// The variables.
        /// </value>
        public ObservableCollection<Variable> Variables { get; set; }

        /// <summary>
        /// Gets or sets the load tests.
        /// </summary>
        /// <value>
        /// The load tests.
        /// </value>
        public ObservableCollection<LoadTest> LoadTests { get; set; }
    }
}