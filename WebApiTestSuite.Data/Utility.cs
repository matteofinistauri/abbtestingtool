﻿using System;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;

namespace WebApiTestSuite.Data
{
    public static class Utility
    {
        public const string STEP_STATE_WAIT = "WAIT";
        public const string STEP_STATE_EXECUTED_OK = "OK";
        public const string STEP_STATE_EXECUTED_FAILED = "FAILED";
        public const string STEP_STATE_RUNNING = "RUNNING";
        public const string STEP_TYPE_REQ_URL = "REQ_URL";
        public const string STEP_TYPE_REQ_TOKEN = "REQ_TOKEN";

        public const string CLR_LOG = "------------------------------------------------------------------------------------------------------------";

        public static string GetPathRepository()
        {
            return ConfigurationManager.AppSettings["pathRepositoryPlan"].ToString();
        }

        public static string GetHostWebAPI()
        {
            return ConfigurationManager.AppSettings["HostWebAPI"].ToString();
        }

        public static string GetPathFileLog()
        {
            return ConfigurationManager.AppSettings["PathFileLog"].ToString();
        }

        public static string GetFileLogName()
        {
            return ConfigurationManager.AppSettings["FileLog"].ToString();
        }

        public static void LogOnFile(string fileContent)
        {
            if (!Directory.Exists(GetPathFileLog()))
                Directory.CreateDirectory(GetPathFileLog());
            if (!File.Exists(GetPathFileLog() + "/" + GetFileLogName()))
                File.Create(GetPathFileLog() + "/" + GetFileLogName());

            using (StreamWriter w = File.AppendText(GetPathFileLog() + "/" + GetFileLogName()))
            {
                w.WriteLine(fileContent);
            }
        }

        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }
    }
}