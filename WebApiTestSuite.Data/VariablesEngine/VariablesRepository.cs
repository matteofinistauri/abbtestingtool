﻿using Altran.WPF.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.Data.VariablesEngine
{
    /// <summary>
    ///
    /// </summary>
    public class VariablesRepository : ModelBase
    {
        /// <summary>
        /// The variables dictionary
        /// </summary>
        private Dictionary<string, Variable> variablesDictionary = new Dictionary<string, Variable>();

        /// <summary>
        /// The dispatcher
        /// </summary>
        private Dispatcher dispatcher;

        /// <summary>
        /// Initializes a new instance of the <see cref="VariablesRepository"/> class.
        /// </summary>
        /// <param name="dispatcher">The dispatcher.</param>
        public VariablesRepository(Dispatcher dispatcher)
        {
            this.Variables = new ObservableCollection<Variable>();
            this.dispatcher = dispatcher;
        }

        /// <summary>
        /// Adds the global variable.
        /// </summary>
        /// <param name="variable">The variable.</param>
        public void AddGlobalVariables(params Variable[] variables)
        {
            foreach (var variable in variables)
            {
                AddVariable(variable);
            }
        }

        /// <summary>
        /// Adds the variable.
        /// </summary>
        /// <param name="variable">The variable.</param>
        private void AddVariable(Variable variable)
        {
            this.variablesDictionary.Add(variable.Name, variable);
            dispatcher.BeginInvoke(new Action(() => this.Variables.Add(variable)));
        }

        /// <summary>
        /// Adds the global variable.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public void AddGlobalVariable(string name, string value)
        {
            AddVariable(new Variable(name, value));
        }

        /// <summary>
        /// Replaces the variables.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Variable ' + variable + ' not defined.</exception>
        public string ReplaceVariables(string input)
        {
            List<string> currentVariables = new List<string>();
            Regex regex = new Regex("{{(?<variable>[a-zA-Z0-9]+)}}");
            foreach (Match match in regex.Matches(input))
            {
                foreach (string groupName in regex.GetGroupNames())
                {
                    if (groupName != "0")
                    {
                        currentVariables.Add(match.Groups[groupName].Value);
                    }
                }
            }

            foreach (var variableName in currentVariables)
            {
                if (!variablesDictionary.ContainsKey(variableName))
                {
                    throw new Exception("Variable '" + variableName + "' not defined.");
                }

                var inputString = "{{" + variableName + "}}";
                input = input.Replace(inputString, variablesDictionary[variableName].Value);
            }

            return input;
        }

        /// <summary>
        /// Stores the variables values.
        /// </summary>
        /// <param name="patternToMatch">The pattern to match.</param>
        /// <param name="input">The input.</param>
        public void StoreVariablesValues(string patternToMatch, string input)
        {
            patternToMatch = CreateRegExFromVariables(patternToMatch);
            Regex regex = new Regex(patternToMatch);
            foreach (Match match in regex.Matches(input))
            {
                foreach (string variableName in regex.GetGroupNames())
                {
                    if (variableName != "0")
                    {
                        var value = match.Groups[variableName].Value;
                        if (this.variablesDictionary.ContainsKey(variableName) && value != this.variablesDictionary[variableName].Value)
                        {
                            throw new Exception("Variable '" + variableName + "' is already defined and its value is different from the previous one.");
                        }
                        else if (!this.variablesDictionary.ContainsKey(variableName))
                        {
                            this.AddVariable(new Variable(variableName, value));
                        }
                    }
                }
            }

            this.RaisePropertyChangedEvent(() => this.HasVariables);
        }

        /// <summary>
        /// Gets a value indicating whether this instance has variables.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has variables; otherwise, <c>false</c>.
        /// </value>
        public bool HasVariables
        {
            get
            {
                return this.Variables != null && this.Variables.Count != 0;
            }
        }

        /// <summary>
        /// Gets the variables.
        /// </summary>
        /// <value>
        /// The variables.
        /// </value>
        public ObservableCollection<Variable> Variables { get; private set; }

        /// <summary>
        /// Creates the reg ex from variables.
        /// </summary>
        /// <param name="patternToMatch">The pattern to match.</param>
        /// <returns></returns>
        public static string CreateRegExFromVariables(string patternToMatch)
        {
            patternToMatch = patternToMatch.Replace("{{", "(?<");
            patternToMatch = patternToMatch.Replace("}}", @">.+)");
            return patternToMatch;
        }
    }
}