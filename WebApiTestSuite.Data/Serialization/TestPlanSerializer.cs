﻿using System.IO;
using System.Linq;
using System.Xml.Serialization;
using WebApiTestSuite.Data.Models;

namespace WebApiTestSuite.Data.Serialization
{
    /// <summary>
    ///
    /// </summary>
    public static class TestPlanSerializer
    {
        /// <summary>
        /// Serializes the specified test plan.
        /// </summary>
        /// <param name="testPlan">The test plan.</param>
        /// <param name="file">The file.</param>
        public static void Serialize(TestPlan testPlan, string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TestPlan));
            using (TextWriter writer = new StreamWriter(filename, false))
            {
                serializer.Serialize(writer, testPlan);
            }
        }

        /// <summary>
        /// Serializes the specified test plan.
        /// </summary>
        /// <param name="testPlan">The test plan.</param>
        /// <returns></returns>
        public static string Serialize(TestPlan testPlan)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TestPlan));
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, testPlan);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Deserializes the specified filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns></returns>
        public static TestPlan Deserialize(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TestPlan));
            using (FileStream myFileStream = new FileStream(filename, FileMode.Open))
            {
                var testPlan = (TestPlan)serializer.Deserialize(myFileStream);
                foreach (var testCase in testPlan.TestCases)
                {
                    testCase.Parent = testPlan;
                    RestoreStepsLink(testPlan, testCase);
                }

                foreach (var loadTest in testPlan.LoadTests)
                {
                    loadTest.Parent = testPlan;
                    foreach (var testStep in loadTest.Steps)
                    {
                        testStep.Parent = loadTest;
                        RestoreStepsLink(testPlan, loadTest);
                    }
                }

                return testPlan;
            }
        }

        /// <summary>
        /// Restores the steps link.
        /// </summary>
        /// <param name="testPlan">The test plan.</param>
        /// <param name="testStepParent">The test step parent.</param>
        private static void RestoreStepsLink(TestPlan testPlan, TestStepParent testStepParent)
        {
            foreach (var testStep in testStepParent.Steps)
            {
                testStep.Parent = testStepParent;
                var macroStep = testStep as MacroStep;
                if (macroStep != null)
                {
                    if (macroStep.MacroDescription != null)
                    {
                        macroStep.Macro = testPlan.Macros.First(x => x.Description == macroStep.MacroDescription);
                    }
                    else
                    {
                        macroStep.Macro = testPlan.Macros.FirstOrDefault();
                    }
                }
            }
        }
    }
}