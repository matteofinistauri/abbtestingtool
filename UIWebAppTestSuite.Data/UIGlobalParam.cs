﻿using System;

namespace UIWebAppTestSuite.Data
{
    public class UIGlobalParam
    {
        private String urlSite;

        public String UrlSite
        {
            set { urlSite = value; }
            get { return urlSite; }
        }

        private String user;

        public String User
        {
            set { user = value; }
            get { return user; }
        }

        private String password;

        public String Password
        {
            set { password = value; }
            get { return password; }
        }

        private String userName;

        public String UserName
        {
            set { userName = value; }
            get { return userName; }
        }
    }
}