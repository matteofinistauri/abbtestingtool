﻿using System.IO;
using System.Xml.Serialization;

namespace UIWebAppTestSuite.Data
{
    public class TestSerialize
    {
        public static T Deserialize<T>(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (FileStream myFileStream = new FileStream(filename, FileMode.Open))
            {
                return (T)serializer.Deserialize(myFileStream);
            }
        }
    }
}