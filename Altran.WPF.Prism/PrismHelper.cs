﻿using Altran.WPF.MVVM;
using Microsoft.Practices.ServiceLocation;
using Prism.Regions;
using System;
using System.Linq;
using System.Windows.Controls;

namespace Altran.WPF.Prism
{
    /// <summary>
    ///
    /// </summary>
    public static class PrismHelper
    {
        /// <summary>
        /// The region manager
        /// </summary>
        private static IRegionManager regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();

        #region NavigateToView

        /// <summary>
        /// Navigates to view.
        /// </summary>
        /// <typeparam name="VM">The type of the M.</typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="region">The region.</param>
        /// <param name="viewModelParam">The view model param.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        public static void NavigateToView<VM, V>(string region, object viewModelParam, string viewModelUnityKey = null)
            where VM : ViewModelBase
            where V : UserControl
        {
            NavigateToView<VM, V>(region, new object[] { viewModelParam });
        }

        /// <summary>
        /// Navigates to view.
        /// </summary>
        /// <typeparam name="VM">The type of the M.</typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="region">The region.</param>
        /// <param name="viewModelParams">The view model params.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        public static void NavigateToView<VM, V>(string region, object[] viewModelParams = null, string viewModelUnityKey = null)
            where VM : ViewModelBase
            where V : UserControl
        {
            NavigateToView(region, typeof(V), typeof(VM), viewModelParams, viewModelUnityKey);
        }

        /// <summary>
        /// Navigates to view.
        /// </summary>
        /// <param name="region">The region.</param>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="viewModelParams">The view model params.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        public static void NavigateToView(string region, Type viewType, Type viewModelType, object[] viewModelParams = null, string viewModelUnityKey = null)
        {
            Navigate(region, viewType, viewModelType, viewModelParams, viewModelUnityKey);
        }

        /// <summary>
        /// Navigates the specified region.
        /// </summary>
        /// <param name="region">The region.</param>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="viewModelParameters">The view model parameters.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        private static void Navigate(string region, Type viewType, Type viewModelType, object[] viewModelParameters, string viewModelUnityKey = null)
        {
            NavigateToView(region, CreateView(viewType, viewModelType, viewModelUnityKey, viewModelParameters));
        }

        /// <summary>
        /// Navigates to view.
        /// </summary>
        /// <param name="region">The region.</param>
        /// <param name="userControl">The user control.</param>
        public static void NavigateToView(string region, UserControl userControl)
        {
            PrismHelper.RemoveAllViewsFromRegion(region);
            PrismHelper.RegisterViewWithRegion(region, () => userControl);
        }

        #endregion NavigateToView

        /// <summary>
        /// Removes all views from region.
        /// </summary>
        /// <param name="regionName">Name of the region.</param>
        public static void RemoveAllViewsFromRegion(string regionName)
        {
            try
            {
                var views = from v in regionManager.Regions[regionName].Views select v;
                foreach (var view in views)
                {
                    regionManager.Regions[regionName].Remove(view);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Registers the view with region.
        /// </summary>
        /// <param name="regionName">Name of the region.</param>
        /// <param name="viewType">Type of the view.</param>
        public static void RegisterViewWithRegion(string regionName, Type viewType)
        {
            regionManager.RegisterViewWithRegion(regionName, viewType);
        }

        /// <summary>
        /// Registers the view with region.
        /// </summary>
        /// <typeparam name="T">The view type.</typeparam>
        /// <param name="regionName">Name of the region.</param>
        public static void RegisterViewWithRegion<T>(string regionName)
            where T : UserControl
        {
            regionManager.RegisterViewWithRegion(regionName, typeof(T));
        }

        /// <summary>
        /// Associate a view with a region, using a delegate to resolve a concrete instance of the view.
        /// When the region get's displayed, this delegate will be called and the result will be added to the
        /// views collection of the region.
        /// </summary>
        /// <param name="regionName">Name of the region.</param>
        /// <param name="getContentDelegate">The delegate to determine the concrete instance of the view.</param>
        public static void RegisterViewWithRegion(string regionName, Func<object> getContentDelegate)
        {
            var result = getContentDelegate();
            if (result != null)
            {
                regionManager.RegisterViewWithRegion(regionName, () => result);
            }
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        /// <returns></returns>
        public static UserControl CreateView(Type viewType, Type viewModelType, string viewModelUnityKey = null)
        {
            var view = UnityHelper.RegisterAndResolveView(viewType);
            var viewModel = viewModelUnityKey == null ? Activator.CreateInstance(viewModelType) : UnityHelper.RegisterAndResolveViewModel(viewModelType, viewModelUnityKey);
            view.DataContext = viewModel;
            return view;
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <typeparam name="VM">The type of the view model.</typeparam>
        /// <typeparam name="V">The type of the view.</typeparam>
        /// <param name="viewModelParams">The view model params.</param>
        /// <returns>
        /// The created view.
        /// </returns>
        public static V CreateView<VM, V>(params object[] viewModelParams)
            where VM : ViewModelBase
            where V : UserControl
        {
            return (V)CreateView(typeof(V), typeof(VM), null, viewModelParams);
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        /// <param name="viewModelParams">The view model params.</param>
        /// <returns></returns>
        public static UserControl CreateView(Type viewType, Type viewModelType, string viewModelUnityKey = null, params object[] viewModelParams)
        {
            var view = UnityHelper.RegisterAndResolveView(viewType);
            var viewModel = Activator.CreateInstance(viewModelType, viewModelParams);
            view.DataContext = viewModel;
            return view;
        }
    }
}