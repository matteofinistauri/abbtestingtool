﻿using Prism.Modularity;

namespace Altran.WPF.Prism
{
    /// <summary>
    ///
    /// </summary>
    public abstract class ModuleBase : IModule
    {
        /// <summary>
        /// Modules a module.
        /// </summary>
        public ModuleBase()
        {
        }

        /// <summary>
        /// Notifies the module that it has be initialized.
        /// </summary>
        public abstract void Initialize();
    }
}