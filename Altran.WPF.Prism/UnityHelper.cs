﻿using Altran.WPF.MVVM;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Diagnostics.Contracts;
using System.Windows.Controls;

namespace Altran.WPF.Prism
{
    /// <summary>
    /// An helper class for Unity IoC framework.
    /// </summary>
    public static class UnityHelper
    {
        #region Fields

        /// <summary>
        /// The Unity container.
        /// </summary>
        private static IUnityContainer container = ServiceLocator.Current.GetInstance<IUnityContainer>();

        #endregion Fields

        #region Resolve And Register Type Methods

        /// <summary>
        /// Resolves an instance of the specified class through the container.
        /// </summary>
        /// <typeparam name="T">The class of the instance to be resolved-registered.</typeparam>
        /// <param name="overrides">The overrides.</param>
        /// <returns>
        /// The registered-resolved instance.
        /// </returns>
        public static T RegisterAndResolveType<T>(params ResolverOverride[] overrides)
        {
            return (T)RegisterAndResolveType(typeof(T), overrides);
        }

        /// <summary>
        /// Resolves an instance of the specified class through the container using the specified name.
        /// </summary>
        /// <typeparam name="T">The class of the instance to be resolved-registered.</typeparam>
        /// <param name="name">The name to use to resolve-register the instance.</param>
        /// <param name="overrides">The overrides.</param>
        /// <returns>
        /// The registered-resolved instance.
        /// </returns>
        public static T RegisterAndResolveType<T>(string name, params ResolverOverride[] overrides)
        {
            return (T)RegisterAndResolveType(typeof(T), name, overrides);
        }

        /// <summary>
        /// Resolves an instance of the specified class through the container.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="overrides">The overrides.</param>
        /// <returns>
        /// The registered-resolved instance.
        /// </returns>
        public static object RegisterAndResolveType(Type type, params ResolverOverride[] overrides)
        {
            var container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            object instance = container.Resolve(type, overrides);
            // Registration for the new instances...
            container.RegisterInstance(type, instance);
            return instance;
        }

        /// <summary>
        /// Resolves an instance of the specified class through the container using the specified name.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="name">The name to use to resolve-register the instance.</param>
        /// <param name="overrides">The overrides.</param>
        /// <returns>
        /// The registered-resolved instance.
        /// </returns>
        public static object RegisterAndResolveType(Type type, string name, params ResolverOverride[] overrides)
        {
            var container = ServiceLocator.Current.GetInstance<IUnityContainer>();
            object instance = container.Resolve(type, name, overrides);
            // Registration for the new instances...
            container.RegisterInstance(type, name, instance);
            return instance;
        }

        /// <summary>
        /// Resolves an instance of an object using the specified name.
        /// </summary>
        /// <param name="name">The name to use to resolve-register the instance.</param>
        /// <param name="overrides">The overrides.</param>
        /// <returns>
        /// The registered-resolved instance.
        /// </returns>
        public static object RegisterAndResolveType(string name, params ResolverOverride[] overrides)
        {
            return RegisterAndResolveType(typeof(object), name, overrides);
        }

        #endregion Resolve And Register Type Methods

        #region Resolve And Register ViewModels

        /// <summary>
        /// Resolves a view model with the specified name and of the specified view model type.
        /// </summary>
        /// <param name="viewModelType">The type of the view model.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static ViewModelBase ResolveViewModel(Type viewModelType, string name)
        {
            Contract.Assert(typeof(ViewModelBase).IsAssignableFrom(viewModelType));
            return (ViewModelBase)container.Resolve(viewModelType, name);
        }

        /// <summary>
        /// Resolves a view model with the specified name.
        /// </summary>
        /// <typeparam name="T">The type of the view model.</typeparam>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static T ResolveViewModel<T>(string name)
            where T : ViewModelBase
        {
            return (T)container.Resolve<T>(name);
        }

        /// <summary>
        /// Registers the view model.
        /// </summary>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="name">The name.</param>
        /// <param name="viewModelParameters">The view model parameters.</param>
        private static void RegisterViewModel(Type viewModelType, string name, params object[] viewModelParameters)
        {
            RegisterViewModel(viewModelType, name, Activator.CreateInstance(viewModelType, viewModelParameters));
        }

        /// <summary>
        /// Registers the view model.
        /// </summary>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="name">The name.</param>
        /// <param name="viewModel">The view model.</param>
        public static void RegisterViewModel(Type viewModelType, string name, object viewModel)
        {
            Contract.Assert(typeof(ViewModelBase).IsAssignableFrom(viewModelType));
            if (!IsViewModelRegistered(viewModelType, name))
            {
                container.RegisterInstance(viewModelType, name, viewModel);
            }
        }

        /// <summary>
        /// Registers the view model.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <param name="viewModelParameters">The view model parameters.</param>
        private static void RegisterViewModel<T>(string name, params object[] viewModelParameters)
            where T : ViewModelBase
        {
            RegisterViewModel<T>(name, (T)Activator.CreateInstance(typeof(T), viewModelParameters));
        }

        /// <summary>
        /// Registers the view model.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <param name="viewModel">The view model.</param>
        public static void RegisterViewModel<T>(string name, T viewModel)
            where T : ViewModelBase
        {
            if (!IsViewModelRegistered<T>(name))
            {
                container.RegisterInstance<T>(name, viewModel);
            }
        }

        /// <summary>
        /// Resolves the and register view model.
        /// </summary>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="name">The name.</param>
        /// <param name="viewModelParameters">The view model parameters.</param>
        /// <returns></returns>
        public static ViewModelBase RegisterAndResolveViewModel(Type viewModelType, string name, params object[] viewModelParameters)
        {
            RegisterViewModel(viewModelType, name, viewModelParameters);
            return ResolveViewModel(viewModelType, name);
        }

        /// <summary>
        /// Resolves the and register view model.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <param name="viewModelParameters">The view model parameters.</param>
        /// <returns></returns>
        public static T RegisterAndResolveViewModel<T>(string name, params object[] viewModelParameters)
            where T : ViewModelBase
        {
            RegisterViewModel<T>(name, viewModelParameters);
            return ResolveViewModel<T>(name);
        }

        /// <summary>
        /// Determines whether a view model type has been registered with the specified name.
        /// </summary>
        /// <param name="viewModelType">The type of the view model.</param>
        /// <param name="name">The registration name.</param>
        /// <returns>
        ///   <c>true</c> if a view model type has been registered with the specified name; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsViewModelRegistered(Type viewModelType, string name)
        {
            Contract.Assert(typeof(ViewModelBase).IsAssignableFrom(viewModelType));
            return container.IsRegistered(viewModelType, name);
        }

        /// <summary>
        /// Determines whether a view model type has been registered with the specified name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <returns>
        ///   <c>true</c> if a view model type has been registered with the specified name; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsViewModelRegistered<T>(string name)
            where T : ViewModelBase
        {
            return container.IsRegistered<T>(name);
        }

        #endregion Resolve And Register ViewModels

        #region Resolve And Register Views

        /// <summary>
        /// Resolves the and register view.
        /// </summary>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewParameters">The view parameters.</param>
        /// <returns></returns>
        public static UserControl RegisterAndResolveView(Type viewType, params object[] viewParameters)
        {
            Contract.Assert(typeof(UserControl).IsAssignableFrom(viewType));
            RegisterView(viewType, viewParameters);
            return ResolveView(viewType);
        }

        /// <summary>
        /// Resolves the and register view.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewParameters">The view parameters.</param>
        /// <returns></returns>
        public static T RegisterAndResolveView<T>(params object[] viewParameters)
            where T : UserControl
        {
            RegisterView<T>(viewParameters);
            return ResolveView<T>();
        }

        /// <summary>
        /// Resolves a view of the specified type.
        /// </summary>
        /// <param name="viewType">The type of the view.</param>
        /// <returns>
        /// The resolved view.
        /// </returns>
        public static UserControl ResolveView(Type viewType)
        {
            Contract.Assert(typeof(UserControl).IsAssignableFrom(viewType));
            return (UserControl)container.Resolve(typeof(object), viewType.Name);
        }

        /// <summary>
        /// Resolves a view of the specified type.
        /// </summary>
        /// <typeparam name="T">The type of the view.</typeparam>
        /// <returns>
        /// The resolved view.
        /// </returns>
        public static T ResolveView<T>()
            where T : UserControl
        {
            return (T)container.Resolve(typeof(object), typeof(T).Name);
        }

        /// <summary>
        /// Registers a view of the specified type.
        /// </summary>
        /// <param name="viewType">The type of the view.</param>
        /// <param name="viewParameters">The view parameters.</param>
        public static void RegisterView(Type viewType, params object[] viewParameters)
        {
            Contract.Assert(typeof(UserControl).IsAssignableFrom(viewType));
            if (!IsViewRegistered(viewType))
            {
                var view = Activator.CreateInstance(viewType, viewParameters);
                container.RegisterInstance(typeof(object), viewType.Name, view);
            }
        }

        /// <summary>
        /// Registers the view.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewParameters">The view parameters.</param>
        public static void RegisterView<T>(params object[] viewParameters)
            where T : UserControl
        {
            if (!IsViewRegistered<T>())
            {
                var view = Activator.CreateInstance(typeof(T), viewParameters);
                container.RegisterInstance(typeof(object), typeof(T).Name, view);
            }
        }

        /// <summary>
        /// Determines whether a view is registered for the specified view type.
        /// </summary>
        /// <param name="viewType">Type of the view.</param>
        /// <returns>
        ///   <c>true</c> if a view is registered for the specified view type; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsViewRegistered(Type viewType)
        {
            Contract.Assert(typeof(UserControl).IsAssignableFrom(viewType));
            return container.IsRegistered(typeof(object), viewType.Name);
        }

        /// <summary>
        /// Determines whether [is view registered].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>
        ///   <c>true</c> if [is view registered]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsViewRegistered<T>()
            where T : UserControl
        {
            return container.IsRegistered<object>(typeof(T).Name);
        }

        #endregion Resolve And Register Views

        /// <summary>
        /// Registers the object.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="name">The name.</param>
        public static void RegisterObject(object obj, string name)
        {
            container.RegisterInstance(obj.GetType(), name, obj);
        }
    }
}