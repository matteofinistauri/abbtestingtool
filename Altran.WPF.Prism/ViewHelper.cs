﻿using Altran.WPF.MVVM;
using System;
using System.Windows.Controls;

namespace Altran.WPF.Prism
{
    /// <summary>
    /// An helper class for managing views.
    /// </summary>
    public static class ViewHelper
    {
        #region CreateView

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <typeparam name="VM">The type of the view model.</typeparam>
        /// <typeparam name="V">The type of the view</typeparam>
        /// <returns>
        /// The created view.
        /// </returns>
        public static V CreateView<VM, V>()
            where VM : ViewModelBase
            where V : UserControl
        {
            return (V)CreateView(typeof(V), typeof(VM));
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        /// <returns></returns>
        public static UserControl CreateView(Type viewType, Type viewModelType, string viewModelUnityKey = null)
        {
            var view = UnityHelper.RegisterAndResolveView(viewType);
            var viewModel = viewModelUnityKey == null ? Activator.CreateInstance(viewModelType) : UnityHelper.RegisterAndResolveViewModel(viewModelType, viewModelUnityKey);
            view.DataContext = viewModel;
            return view;
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <typeparam name="VM">The type of the view model.</typeparam>
        /// <typeparam name="V">The type of the view.</typeparam>
        /// <param name="viewModelParams">The view model params.</param>
        /// <returns>
        /// The created view.
        /// </returns>
        public static V CreateView<VM, V>(params object[] viewModelParams)
            where VM : ViewModelBase
            where V : UserControl
        {
            return (V)CreateView(typeof(V), typeof(VM), null, viewModelParams);
        }

        public static V CreateView<VM, V>(Action<VM> viewModelConfigurationMethod, params object[] viewModelParams)
            where VM : ViewModelBase
            where V : UserControl
        {
            var view = (V)CreateView(typeof(V), typeof(VM), null, viewModelParams);
            var viewModel = (VM)view.DataContext;
            viewModelConfigurationMethod(viewModel);
            return view;
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        /// <param name="viewModelParams">The view model params.</param>
        /// <returns></returns>
        public static UserControl CreateView(Type viewType, Type viewModelType, string viewModelUnityKey = null, params object[] viewModelParams)
        {
            var view = UnityHelper.RegisterAndResolveView(viewType);
            var viewModel = Activator.CreateInstance(viewModelType, viewModelParams);
            view.DataContext = viewModel;
            return view;
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        /// <param name="viewModelConfigurationMethod">The view model configuration method.</param>
        /// <param name="viewModelUnityKey">The view model unity key.</param>
        /// <param name="viewModelParams">The view model parameters.</param>
        /// <returns></returns>
        public static UserControl CreateView(Type viewType, Type viewModelType, Action viewModelConfigurationMethod, string viewModelUnityKey = null, params object[] viewModelParams)
        {
            var view = UnityHelper.RegisterAndResolveView(viewType);
            var viewModel = Activator.CreateInstance(viewModelType, viewModelParams);
            view.DataContext = viewModel;
            return view;
        }

        #endregion CreateView

        //#region NavigateToView

        ///// <summary>
        ///// Navigates to view.
        ///// </summary>
        ///// <typeparam name="VM">The type of the M.</typeparam>
        ///// <typeparam name="V"></typeparam>
        ///// <param name="region">The region.</param>
        ///// <param name="viewModelParam">The view model param.</param>
        ///// <param name="viewModelUnityKey">The view model unity key.</param>
        //public static void NavigateToView<VM, V>(string region, object viewModelParam, string viewModelUnityKey = null)
        //    where VM : MVVMBase
        //    where V : UserControl
        //{
        //    NavigateToView<VM, V>(region, new object[] { viewModelParam });
        //}

        ///// <summary>
        ///// Navigates to view.
        ///// </summary>
        ///// <typeparam name="VM">The type of the M.</typeparam>
        ///// <typeparam name="V"></typeparam>
        ///// <param name="region">The region.</param>
        ///// <param name="viewModelParams">The view model params.</param>
        ///// <param name="viewModelUnityKey">The view model unity key.</param>
        //public static void NavigateToView<VM, V>(string region, object[] viewModelParams = null, string viewModelUnityKey = null)
        //    where VM : MVVMBase
        //    where V : UserControl
        //{
        //    NavigateToView(region, typeof(V), typeof(VM), viewModelParams, viewModelUnityKey);
        //}

        ///// <summary>
        ///// Navigates to view.
        ///// </summary>
        ///// <param name="region">The region.</param>
        ///// <param name="viewType">Type of the view.</param>
        ///// <param name="viewModelType">Type of the view model.</param>
        ///// <param name="viewModelParams">The view model params.</param>
        ///// <param name="viewModelUnityKey">The view model unity key.</param>
        //public static void NavigateToView(string region, Type viewType, Type viewModelType, object[] viewModelParams = null, string viewModelUnityKey = null)
        //{
        //    Navigate(region, viewType, viewModelType, viewModelParams, viewModelUnityKey);
        //}

        ///// <summary>
        ///// Navigates the specified region.
        ///// </summary>
        ///// <param name="region">The region.</param>
        ///// <param name="viewType">Type of the view.</param>
        ///// <param name="viewModelType">Type of the view model.</param>
        ///// <param name="viewModelParameters">The view model parameters.</param>
        ///// <param name="viewModelUnityKey">The view model unity key.</param>
        //private static void Navigate(string region, Type viewType, Type viewModelType, object[] viewModelParameters, string viewModelUnityKey = null)
        //{
        //    NavigateToView(region, ViewHelper.CreateView(viewType, viewModelType, viewModelUnityKey, viewModelParameters));
        //}

        ///// <summary>
        ///// Navigates to view.
        ///// </summary>
        ///// <param name="region">The region.</param>
        ///// <param name="userControl">The user control.</param>
        //public static void NavigateToView(string region, UserControl userControl)
        //{
        //    PrismHelper.RemoveAllViewsFromRegion(region);
        //    PrismHelper.RegisterViewWithRegion(region, () => userControl);
        //}

        //#endregion NavigateToView
    }
}