﻿using Altran.WPF.Windows;
using Microsoft.Practices.Unity;
using System;
using System.Windows.Media.Imaging;
using WebApiTestSuite.ViewModels;

namespace WebApiTestSuite
{
    /// <summary>
    ///
    /// </summary>
    /// <seealso cref="Altran.WPF.Windows.FolderBootstrapper{Altran.WPF.Windows.ShellWithMenuWindow}" />
    public class WebApiTestSuiteBootstrapper : FolderBootstrapper<ShellWithMenuWindow>
    {
        /// <summary>
        /// The module s_ folder
        /// </summary>
        private const string MODULES_FOLDER = "Modules";

        /// <summary>
        /// Initializes a new instance of the <see cref="WebApiTestSuiteBootstrapper" /> class.
        /// </summary>
        public WebApiTestSuiteBootstrapper()
            : base(MODULES_FOLDER)
        {
        }

        /// <summary>
        /// Configures the shell.
        /// </summary>
        /// <param name="shell">The shell.</param>
        protected override void ConfigureShell(ShellWithMenuWindow shell)
        {
            shell.DataContext = Container.Resolve<WebApiTestSuiteShellViewModel>();
            shell.Icon = new BitmapImage(new Uri("pack://application:,,,/WebApiTestSuite.Common;component/Icons/TestSuite.png"));
        }
    }
}