﻿using Altran.WPF.Windows.Events;
using Altran.WPF.Windows.ViewModels;
using Microsoft.Win32;
using Prism.Events;
using System.IO;
using System.Windows;
using WebApiTestSuite.Data;
using WebApiTestSuite.Data.Serialization;

namespace WebApiTestSuite.ViewModels
{
    public class WebApiTestSuiteShellViewModel : ShellWithMenuWindowViewModel
    {
        /// <summary>
        /// The current file
        /// </summary>
        private string currentFile;

        /// <summary>
        /// The current test plan
        /// </summary>
        private TestPlan currentTestPlan = new TestPlan();

        /// <summary>
        /// The event aggregator
        /// </summary>
        private IEventAggregator eventAggregator;

        /// <summary>
        /// The serialized test plan
        /// </summary>
        private string serializedTestPlan = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebApiTestSuiteShellViewModel"/> class.
        /// </summary>
        /// <param name="eventAggregator">The event aggregator.</param>
        public WebApiTestSuiteShellViewModel(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
            eventAggregator.GetEvent<GetOpenObjectEvent<TestPlan>>().Subscribe(this.GetOpenObject, ThreadOption.PublisherThread, true);
        }

        /// <summary>
        /// Gets the open object.
        /// </summary>
        /// <param name="obj">The object.</param>
        private void GetOpenObject(ObjectContainer<TestPlan> obj)
        {
            obj.Object = currentTestPlan;
        }

        /// <summary>
        /// The current file
        /// </summary>
        private string CurrentFile
        {
            get
            {
                return currentFile;
            }
            set
            {
                if (this.currentFile != value)
                {
                    this.currentFile = value;
                    this.RaisePropertyChangedEvent(() => this.CurrentFile);
                    this.RaisePropertyChangedEvent(() => this.WindowTitle);
                }
            }
        }

        /// <summary>
        /// Gets the filter.
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
        private string Filter
        {
            get
            {
                return "Web API Test Suite files (*.xml)|*.xml";
            }
        }

        /// <summary>
        /// Gets the window title.
        /// </summary>
        /// <value>
        /// The window title.
        /// </value>
        public override string WindowTitle
        {
            get
            {
                string result = "Web API Test Suite";
                if (currentFile != null)
                {
                    FileInfo f = new FileInfo(this.currentFile);
                    result += " [" + f.Name + "]";
                }

                return result;
            }
        }

        /// <summary>
        /// News this instance.
        /// </summary>
        protected override void New()
        {
            this.CurrentFile = null;
            this.currentTestPlan = new TestPlan();
            this.serializedTestPlan = TestPlanSerializer.Serialize(this.currentTestPlan);
            eventAggregator.GetEvent<OpenObjectEvent<TestPlan>>().Publish(this.currentTestPlan);
        }

        /// <summary>
        /// Opens this instance.
        /// </summary>
        protected override void Open()
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = this.Filter;
            var result = openDialog.ShowDialog();
            if (result != null && (bool)result)
            {
                VerifyIfUpdated();
                this.CurrentFile = openDialog.FileName;
                this.currentTestPlan = TestPlanSerializer.Deserialize(this.CurrentFile);
                this.serializedTestPlan = TestPlanSerializer.Serialize(this.currentTestPlan);
                eventAggregator.GetEvent<OpenObjectEvent<TestPlan>>().Publish(this.currentTestPlan);
            }
        }

        /// <summary>
        /// Verifies if updated.
        /// </summary>
        private void VerifyIfUpdated()
        {
            var serialized = TestPlanSerializer.Serialize(this.currentTestPlan);
            if (serializedTestPlan != serialized)
            {
                var result = MessageBox.Show("Do you want to save the current file?", "File updated", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    Save();
                }
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        protected override bool Save()
        {
            bool ok = true;
            if (currentFile == null)
            {
                ok = SaveAs();
            }

            if (ok)
            {
                this.serializedTestPlan = TestPlanSerializer.Serialize(this.currentTestPlan);
                TestPlanSerializer.Serialize(this.currentTestPlan, this.CurrentFile);
            }

            return ok;
        }

        /// <summary>
        /// Saves as.
        /// </summary>
        protected override bool SaveAs()
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = this.Filter;
            var result = saveDialog.ShowDialog();
            if (result != null && (bool)result)
            {
                this.CurrentFile = saveDialog.FileName;
                return Save();
            }

            return false;
        }

        /// <summary>
        /// Exits this instance.
        /// </summary>
        protected override void Exit()
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Forms the closing.
        /// </summary>
        protected override void FormClosing()
        {
            VerifyIfUpdated();
        }
    }
}