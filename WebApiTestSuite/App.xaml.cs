﻿using System.Windows;

namespace WebApiTestSuite
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            WebApiTestSuiteBootstrapper bs = new WebApiTestSuiteBootstrapper();
            bs.Run();
        }
    }
}